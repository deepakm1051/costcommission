package com.costcommission.util;

import com.costcommission.exception.CostPlusException;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtil {


    public synchronized static void createFolder(String baseFolder){
        try {
            File folder = new File( baseFolder);
            if (!folder.exists()) {
                folder.mkdirs();
                System.out.println("Folder created in :" + baseFolder);
            }
        } catch (Exception ex) {
            System.out.println("Exception in creating folder : " + baseFolder + ex);
        }
    }

    public synchronized static void createFile(String filePath, String fileName) {
        try {
            File myObj = new File(filePath + "/" +fileName);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred while creating file. "+filePath+"/"+fileName);
            e.printStackTrace();
        }
    }

    public synchronized static void saveFiles(InputStream inputStream, String uploadedFileLocation){
        OutputStream out = null;
        try {
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (Exception e) {
            System.out.println("Exception in saving file : " + uploadedFileLocation + e.getMessage());
        }finally{
            try{
                if(inputStream !=null) {
                    inputStream.close();
                }
                if(out !=null) {
                    out.flush();
                    out.close();
                }

            }catch(Exception ex){
                System.out.println("Exception in closing stream: " + ex.getMessage());
                return;
            }
        }
    }

    public synchronized static boolean isFileAvailable(String filePath){
        File file = new File(filePath);
        try{
            if(file.exists() && !file.isDirectory()) {
                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
            System.out.println("Exception in finding file : " + ex);
            return false;
        }
    }

    public synchronized static boolean delete(String filePath, String fileName){
        File file = new File(filePath + "/" + fileName);
        try{
            if(file.exists() && !file.isDirectory()) {
                if(file.delete())
                return true;
            }
            return false;
        }catch(Exception ex){
            System.out.println("Exception in finding file : " + ex);
            return false;
        }
    }


    public static void developmentUpload(ByteArrayOutputStream baos, String costPlusPath, String fileName) {
        FileUtil.createFolder(costPlusPath);
        FileUtil.createFile(costPlusPath, fileName);
        FileUtil.saveFiles(new ByteArrayInputStream(baos.toByteArray()), costPlusPath + "/" + fileName);
    }

    public static void saveEmailTempForDevelopment(String tempPath, String fileName, String sourceFilePath, Long userId, String errorMessage) throws CostPlusException, FileNotFoundException {
        InputStream inputStream = null;
        File tempFile = new File(sourceFilePath+"/"+fileName);
        boolean exists = tempFile.exists();
        if(exists){
            inputStream = new FileInputStream(sourceFilePath + "/"+fileName);
            fileName = userId+"#"+fileName;
            FileUtil.createFolder(tempPath);
            FileUtil.createFile(tempPath, fileName);
            FileUtil.saveFiles(inputStream, tempPath + "/" + fileName);
        }else {
            throw new CostPlusException(errorMessage);
        }
    }

    public static ByteArrayOutputStream zipFileWithoutSaveLocal(Map<String,InputStream> inputStreamMap) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = null;
        try {
            zos=new ZipOutputStream(baos);
            for (Map.Entry<String,InputStream> entry : inputStreamMap.entrySet()){
                ZipEntry zipEntry = new ZipEntry(entry.getKey());
                zos.putNextEntry(zipEntry);
                byte[] bytes = IOUtils.toByteArray(entry.getValue());
                ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(bytes);

                byte[] buffer = new byte[1024];
                int len;
                while ((len = arrayInputStream.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
            }
            zos.closeEntry();
            return baos;
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }finally {
            if (zos != null) {
                zos.close();
            }
            baos.close();
        }

    }
}
