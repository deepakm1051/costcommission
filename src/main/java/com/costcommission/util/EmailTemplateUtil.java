package com.costcommission.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

@Component
public class EmailTemplateUtil {

    private final TemplateEngine templateEngine;

    @Autowired
    public EmailTemplateUtil(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String getEmailTemplateData(String templateName, Map<String, Object> itemdata) {
        Context context = new Context();
        if (itemdata != null) {
            for (Map.Entry<String, Object> data : itemdata.entrySet()) {
                context.setVariable(data.getKey(), data.getValue());
            }
        }
        return templateEngine.process(templateName, context);
    }

}
