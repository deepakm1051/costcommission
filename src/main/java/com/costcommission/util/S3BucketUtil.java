package com.costcommission.util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.costcommission.exception.CostPlusException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class S3BucketUtil {

    public static AmazonS3 s3Authentication() {
        return AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_1).build();
    }

    public static void upload(String fileName, InputStream inputStream, String bucketName, String bucketPath) {
        AmazonS3 s3 = s3Authentication();
        try {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(inputStream.available());
            s3.putObject(new PutObjectRequest(bucketName, bucketPath+fileName,inputStream,objectMetadata)
                    .withCannedAcl(CannedAccessControlList.Private));
        } catch (AmazonServiceException | IOException e) {
            e.printStackTrace();
            throw new AmazonClientException(e);
        }
    }

    public static boolean fileExist(String fileName,String bucketName, String bucketPath) {
        AmazonS3 s3client = s3Authentication();
        return s3client.doesObjectExist(bucketName,bucketPath+fileName);
    }

    public static InputStream getFileInputStream(String fileName,String bucketName,String bucketPath) throws Exception {
        try {
            AmazonS3 s3client = s3Authentication();
            S3Object object = s3client.getObject(new GetObjectRequest(bucketName, bucketPath + fileName));
            if(object !=null)
            return object.getObjectContent();
            else
                throw new CostPlusException("File Not Found");
        } catch (AmazonS3Exception | CostPlusException e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}
