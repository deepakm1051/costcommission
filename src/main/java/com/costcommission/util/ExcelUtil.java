package com.costcommission.util;

import com.costcommission.costenum.MonthEnum;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ExcelUtil {

    public static void border(int firstRow, int lastRow, int firstColumn, int lastColumn, Sheet sheet) {
        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRow, lastRow, firstColumn, lastColumn);
        RegionUtil.setBorderBottom(BorderStyle.MEDIUM, cellRangeAddress, sheet);
        RegionUtil.setBorderTop(BorderStyle.MEDIUM, cellRangeAddress, sheet);
        RegionUtil.setBorderLeft(BorderStyle.MEDIUM, cellRangeAddress, sheet);
        RegionUtil.setBorderRight(BorderStyle.MEDIUM, cellRangeAddress, sheet);
    }

    public static void borderThin(int firstRow, int lastRow, int firstColumn, int lastColumn, Sheet sheet) {
        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRow, lastRow, firstColumn, lastColumn);
        RegionUtil.setBorderBottom(BorderStyle.THIN, cellRangeAddress, sheet);
        RegionUtil.setBorderTop(BorderStyle.THIN, cellRangeAddress, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN, cellRangeAddress, sheet);
        RegionUtil.setBorderRight(BorderStyle.THIN, cellRangeAddress, sheet);
    }

    public static void borderBottom(int firstRow, int lastRow, int firstColumn, int lastColumn, Sheet sheet) {
        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRow, lastRow, firstColumn, lastColumn);
        RegionUtil.setBorderBottom(BorderStyle.MEDIUM, cellRangeAddress, sheet);
    }

    public static void setExcelCellValue(Row row, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
    }

    public static void setCellValueWithStyle(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT);
        cell.setCellStyle(style);
    }

    public static void setCellValueAsNumeric(Row row, CellStyle style, int column, String value, DataFormat dataFormat) {
        Cell cell = row.createCell(column);
        cell.setCellFormula("VALUE(" + value + ")");
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        cell.setCellStyle(style);
    }
    public static void setCellValueAsNumericAlignBottomRight(Row row, CellStyle style, int column, String value, DataFormat dataFormat) {
        Cell cell = row.createCell(column);
        cell.setCellFormula("VALUE(" + value + ")");
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        style.setVerticalAlignment(VerticalAlignment.BOTTOM);
        style.setAlignment(HorizontalAlignment.RIGHT);
        cell.setCellStyle(style);
    }

    public static void setCellValueAsNumericGetCell(Row row, CellStyle style, int column, String value, XSSFDataFormat dataFormat) {
        Cell cell = row.getCell(column);
        cell.setCellFormula("VALUE(" + value + ")");
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        cell.setCellStyle(style);
    }

    public static void setCellValueAsNumericRedColor(Row row, CellStyle style, XSSFFont font, int column, String value, XSSFDataFormat dataFormat) {
        Cell cell = row.createCell(column);
        cell.setCellFormula("VALUE(" + value + ")");
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        font.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);
        cell.setCellStyle(style);
    }

    public static String getCellRawValue(XSSFWorkbook wbInput, Row row, int column) {
        String value = "0.0";
        Cell cell = row.getCell(column);
        if (cell.getCellType() == CellType.FORMULA) {
            switch (cell.getCachedFormulaResultType()) {
                case NUMERIC:
                    value = String.valueOf(cell.getNumericCellValue());
                    break;
                case STRING:
                    value = cell.getStringCellValue();
                    break;
            }
        }
        return value;
    }

    public static String getColumnLetter(String subCategory, Map<String, List<String>> subcategoryCostColLetterMap, boolean isStart) {
        List<String> columnLetterList = subcategoryCostColLetterMap.get(subCategory);
        if (isStart) {
            return columnLetterList.get(0);
        } else {
            if (columnLetterList.size() > 1) {
                return columnLetterList.get(columnLetterList.size() - 1);
            } else {
                return columnLetterList.get(0);
            }
        }
    }

    public static int getExcelColumnIndex(Map<String, List<String>> subcategoryCostColLetterMap, boolean isStart) {
        String startLetterIndex = getColumnLetter("Front Office", subcategoryCostColLetterMap, true);
        String endLetterIndex = getColumnLetter("Front Office", subcategoryCostColLetterMap, false);
        if (isStart) {
            return CellReference.convertColStringToIndex(startLetterIndex);
        } else {
            return CellReference.convertColStringToIndex(endLetterIndex);
        }
    }

    public static String getExcelCellValue(Row row, int column) {
        Cell cell = row.getCell(column);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public static String getExcelCellValueAsNumber(Row row, int column,Workbook workbook) {
        DataFormat fmt = workbook.createDataFormat();
        CellStyle textStyle = workbook.createCellStyle();
        textStyle.setDataFormat(fmt.getFormat("#,##0.##"));
        Cell cell = row.getCell(column);
        cell.setCellStyle(textStyle);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public static String getColumnLetterByColIndex(int colIndex, Row row) {
        String subTotalColLetter;
        Cell cell = row.getCell(colIndex);
        subTotalColLetter = CellReference.convertNumToColString(cell.getColumnIndex());
        return subTotalColLetter;
    }

    public static void formula(Row row, int cellIndex, String formula, Sheet sheet) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellFormula(formula);
    }

    public static void formulaWithFormat(Row row, int cellIndex, CellStyle style, String formula, Sheet sheet, XSSFDataFormat dataFormat) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellFormula(formula);
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        style.setAlignment(HorizontalAlignment.CENTER);
        cell.setCellStyle(style);
    }

    public static void formulaWithFormatBlueColor(Row row, int cellIndex, CellStyle style, String formula, Sheet sheet, XSSFDataFormat dataFormat) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellFormula(formula);
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        cell.setCellStyle(style);
    }

    public static void formulaWithFormatGrayColor(Row row, int cellIndex, CellStyle style, String formula, Sheet sheet, XSSFDataFormat dataFormat) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellFormula(formula);
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cell.setCellStyle(style);
    }

    public static void formulaWithFormatRedColor(Row row, int cellIndex, XSSFFont font, String formula, XSSFWorkbook workbook, XSSFDataFormat dataFormat) {
        CellStyle style = workbook.createCellStyle();
        Cell cell = row.createCell(cellIndex);
        cell.setCellFormula(formula);
        font.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        cell.setCellStyle(style);
    }

    public static void formulaWithFormatBoldWrap(Row row, int cellValue, String formula, XSSFFont font, CellStyle style, XSSFDataFormat dataFormat) {
        Cell cell = row.createCell(cellValue);
        cell.setCellFormula(formula);
        font.setBold(true);
        style.setFont(font);
        style.setWrapText(false);
        style.setDataFormat(dataFormat.getFormat("#,##0"));
        style.setVerticalAlignment(VerticalAlignment.BOTTOM);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cell.setCellStyle(style);
    }

    public static void setCellGrayColor(Row row, int cellValue, XSSFFont font, CellStyle style) {
        Cell cell = row.createCell(cellValue);
        font.setBold(true);
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.BOTTOM);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cell.setCellStyle(style);
    }

    public static void percentageFormula(Row row, int cellValue, CellStyle style, String formula, Sheet sheet, DataFormat format) {
        Cell cell = row.createCell(cellValue);
        /*short percentDataFormat = format.getFormat("0.00%");
        CellUtil.setCellStyleProperty(cell, CellUtil.DATA_FORMAT, percentDataFormat);*/
        cell.setCellFormula(formula);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setDataFormat(format.getFormat("0.00%"));
        cell.setCellStyle(style);
    }

    public void createCell(Row row, int columnCount, String value, Sheet sheet) {
        Cell cell = row.createCell(columnCount);
        cell.setCellValue(value);
    }

    public void createSlNoCell(Row row, int columnCount, int value, Sheet sheet) {
        Cell cell = row.createCell(columnCount);
        cell.setCellValue(value);
    }

    public void createHeaderCell(Row row, int columnCount, String value, Sheet sheet, SXSSFWorkbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        style.setWrapText(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        Cell cell = row.createCell(columnCount);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    public void createHeaderCellBlueFontColor(Row row, int columnCount, String value, Sheet sheet, SXSSFWorkbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        font.setColor(HSSFColor.HSSFColorPredefined.BLUE.getIndex());
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        Cell cell = row.createCell(columnCount);
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    public static void setCellValueWrapFillBlueColor(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        cell.setCellStyle(style);
    }
    public static void setCellValueWrapFillBlueColorLeftAlign(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        cell.setCellStyle(style);
    }
    public static void setCellStyleFillBlueColorCentre(Row row, CellStyle style, XSSFFont font, int column) {
        Cell cell = row.getCell(column);
        font.setBold(true);
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        cell.setCellStyle(style);
    }
    public static void setCellStyleFillBlueColorLeft(Row row, CellStyle style, XSSFFont font, int column) {
        Cell cell = row.getCell(column);
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        cell.setCellStyle(style);
    }


    public static void setCellValueWrapFillGrayColor(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cell.setCellStyle(style);
    }

    public static void setCellValueAndWrap(Row row, CellStyle style, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        style.setWrapText(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        cell.setCellStyle(style);
    }

    public static void setCellValueWrapBold(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setWrapText(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        cell.setCellStyle(style);
    }

    public static void setCellValueBoldCenter(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        cell.setCellStyle(style);
    }

    public static void setCellValueBoldCenterFontSize(Row row, CellStyle style, XSSFFont font, int column, String value, int size) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        font.setFontHeight(size);
        style.setFont(font);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setAlignment(HorizontalAlignment.CENTER);
        cell.setCellStyle(style);
    }

    public static void setCellValueBoldAlignLeft(Row row, CellStyle style, XSSFFont font, int column, String value) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.LEFT);
        cell.setCellStyle(style);
    }

    public static Date lastDayOfMonth(String month, Integer year) throws ParseException {
        int monthOrdinal = MonthEnum.valueOf(month).ordinal() + 1;
        String monthValue = String.valueOf(monthOrdinal).length() == 1 ? "0" + monthOrdinal : "" + monthOrdinal;
        String date = "01/" + monthValue + "/" + year;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedDate = dateFormat.parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(convertedDate);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static List<String> getMonthList(String toMonth) {
        MonthEnum[] monthsEnum = MonthEnum.values();
        List<String> months = new ArrayList<>();
        for (MonthEnum monthEnum : monthsEnum) {
            months.add(monthEnum.toString());
            if (monthEnum.toString().equalsIgnoreCase(toMonth)) {
                break;
            }
        }
        return months;
    }

    public static List<String> getMonthListExceptCurrent(String toMonth) {
        MonthEnum[] monthsEnum = MonthEnum.values();
        List<String> months = new ArrayList<>();
        for (MonthEnum monthEnum : monthsEnum) {
            if (!monthEnum.toString().equalsIgnoreCase(toMonth)) {
                months.add(monthEnum.toString());
            } else {
                break;
            }
        }
        return months;
    }

    public static String convert(String price) {
        String formatted = "";
        if (price.length() > 1) {
            formatted = price.substring(0, 1);
            price = price.substring(1);
        }

        while (price.length() > 3) {
            formatted += "," + price.substring(0, 2);
            price = price.substring(2);
        }
        return formatted + "," + price;
    }

    public static String doubleToCommaSeparatedValue(String price) {
        int length = 0;
        StringBuffer priceBuf = new StringBuffer(price);
        long comparePrice = Long.parseLong(price);
        length = price.length();
        if (comparePrice > 999) {
            for (int i = length; i > 0; i = i - 2) {
                if (i == length) {
                    i = i - 3;
                    priceBuf.insert(i, ",");

                } else {
                    priceBuf.insert(i, ",");

                }
            }
        }
        return priceBuf.toString();
    }

}
