package com.costcommission.jpaspecification;

import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.Cluster;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class AgencySpecification implements Specification<Agency> {

    SpecificationDTO criteria;

    public AgencySpecification() {

    }

    public AgencySpecification(final SpecificationDTO specificationDTO) {
        super();
        this.criteria = specificationDTO;
    }

    @Override
    public Predicate toPredicate
            (Root<Agency> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria.getKey().equalsIgnoreCase("clusterName")) {
            criteria.setKey("name");
            Join<Agency, Cluster> joinParent = root.join("cluster");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue());
            }
        }
        else if (criteria.getKey().equalsIgnoreCase("markup") || criteria.getKey().equalsIgnoreCase("vatPercentage")) {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
        }
        else if (criteria.getKey().equalsIgnoreCase("headCountFlag") || criteria.getKey().equalsIgnoreCase("isVatGst")
                || criteria.getKey().equalsIgnoreCase("vatDaLine")|| criteria.getKey().equalsIgnoreCase("vatAccrual")
                || criteria.getKey().equalsIgnoreCase("vatAllCarrier")) {
            boolean value = false;
            if(criteria.getValue().equalsIgnoreCase("yes")) {
                    value = true;
            }
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()),  value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()),  value );
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value );
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            }
        }
        else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue());
            }
        }
        return null;
    }

}
