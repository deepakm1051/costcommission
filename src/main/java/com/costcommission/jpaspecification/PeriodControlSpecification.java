package com.costcommission.jpaspecification;

import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.PeriodControl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;

@Component
public class PeriodControlSpecification implements Specification<PeriodControl> {

    SpecificationDTO criteria;

    public PeriodControlSpecification() {

    }

    public PeriodControlSpecification(final SpecificationDTO specificationDTO) {
        super();
        this.criteria = specificationDTO;
    }

    @Override
    public Predicate toPredicate
            (Root<PeriodControl> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria.getKey().equalsIgnoreCase("agencyName")) {
            criteria.setKey("name");
            Join<PeriodControl, Agency> joinParent = root.join("agency");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        if (criteria.getKey().equalsIgnoreCase("year")) {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
        } else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        return null;
    }

}