package com.costcommission.jpaspecification;

import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Cluster;
import com.costcommission.entity.Region;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class ClusterSpecification implements Specification<Cluster> {

    SpecificationDTO criteria;

    public ClusterSpecification() {

    }

    public ClusterSpecification(final SpecificationDTO specificationDTO) {
        super();
        this.criteria = specificationDTO;
    }

    @Override
    public Predicate toPredicate
            (Root<Cluster> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria.getKey().equalsIgnoreCase("regionName")) {
            criteria.setKey("name");
            Join<Cluster, Region> joinParent = root.join("region");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        } else {

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        return null;
    }

}
