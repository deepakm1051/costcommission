package com.costcommission.jpaspecification;

import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.AdiJournal;
import com.costcommission.entity.Agency;
import com.costcommission.entity.CostPlusTemplateProcess;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ADIJournalSpecification implements Specification<AdiJournal> {

    SpecificationDTO criteria;
    String journalType;
    Long agencyId;

    public ADIJournalSpecification() {

    }

    public ADIJournalSpecification(final SpecificationDTO specificationDTO, String journalType, Long agencyId) {
        super();
        this.criteria = specificationDTO;
        this.journalType = journalType;
        this.agencyId = agencyId;
    }

    @Override
    public Predicate toPredicate
            (Root<AdiJournal> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Join<AdiJournal, CostPlusTemplateProcess> joinParent = root.join("costPlusTemplate");
        Join<CostPlusTemplateProcess, Agency> joinParentAgency = joinParent.join("agency");
        Predicate onAgency = builder.equal(joinParentAgency.<String>get("id"), agencyId);
        Predicate onType = builder.equal(root.<String>get("journalType"), journalType);
        Predicate onFilter = null;
        if (criteria.getOperation().isEmpty()) {
            return builder.and(onAgency, onType);
        } else if (criteria.getKey().equalsIgnoreCase("agencyName")) {
            criteria.setKey("name");

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            return builder.and(onAgency, onType, onFilter);
        } else if (criteria.getKey().equalsIgnoreCase("createdTimeStamp")) {
            if (criteria.getValue().length() == 11) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                Date date = new Date();
                try {
                    date = simpleDateFormat.parse(criteria.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                criteria.setValue(sqlDate.toString());
            }

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
            return builder.and(onAgency, onType, onFilter);
        } else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinParentAgency.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            return builder.and(onAgency, onType, onFilter);
        }
    }
}