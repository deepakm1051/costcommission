package com.costcommission.jpaspecification;

import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.TBVolumeTransaction;
import com.costcommission.entity.User;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AuditTrialSpecification implements Specification<TBVolumeTransaction> {

    SpecificationDTO criteria;

    public AuditTrialSpecification() {

    }

    public AuditTrialSpecification(final SpecificationDTO specificationDTO) {
        super();
        this.criteria = specificationDTO;
    }

    @Override
    public Predicate toPredicate
            (Root<TBVolumeTransaction> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria.getKey().equalsIgnoreCase("agencyName")) {
            criteria.setKey("name");
            Join<TBVolumeTransaction, Agency> joinParent = root.join("agency");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        else if (criteria.getKey().equalsIgnoreCase("displayCreatedBy")) {
            criteria.setKey("fullName");
            Join<TBVolumeTransaction, User> joinParent = root.join("user");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        } else if (criteria.getKey().equalsIgnoreCase("createdTimeStamp")) {
            if (criteria.getValue().length() == 11) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                Date date = new Date();
                try {
                    date = simpleDateFormat.parse(criteria.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                criteria.setValue(sqlDate.toString());
            }
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
        } else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        return null;
    }

}