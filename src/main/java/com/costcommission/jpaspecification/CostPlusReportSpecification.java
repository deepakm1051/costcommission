package com.costcommission.jpaspecification;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class CostPlusReportSpecification implements Specification<CostPlusTemplateProcess> {

    SpecificationDTO criteria;
    Long agencyId;
    Integer year;
    String type;

    public CostPlusReportSpecification() {

    }

    public CostPlusReportSpecification(final SpecificationDTO specificationDTO, final Long agencyId, final Integer year, final String type) {
        super();
        this.criteria = specificationDTO;
        this.agencyId = agencyId;
        this.year = year;
        this.type = type;
    }

    @Override
    public Predicate toPredicate
            (Root<CostPlusTemplateProcess> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        Join<CostPlusTemplateProcess, Agency> joinParent = root.join("agency");
        Predicate onStartConfirm = builder.greaterThanOrEqualTo(root.<String>get("status"), String.valueOf(CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal()));
        Predicate onYear = builder.equal(root.<String>get("year"), year);
        Predicate onAgency = builder.equal(joinParent.<String>get("id"), agencyId);

        /*CriteriaBuilder.In<Long> inClause = builder.in(joinParent.<Long>get("id"));
        for (Long title : agencyId) {
            inClause.value(title);
        }*/
        Predicate onFilter = null;
        if (criteria.getOperation().isEmpty() && type.equalsIgnoreCase("Report")) {
            return builder.and(onAgency, onYear, onStartConfirm);
        } else if (criteria.getOperation().isEmpty() && !type.equalsIgnoreCase("Report")) {
            return builder.and(onAgency, onYear);
        } else if (criteria.getKey().equalsIgnoreCase("preparer") || criteria.getKey().equalsIgnoreCase("reviewer") || criteria.getKey().equalsIgnoreCase("approver")) {
            String joinCol = "";
            if (criteria.getKey().equalsIgnoreCase("preparer")) {
                joinCol = "preparer";
            } else if (criteria.getKey().equalsIgnoreCase("reviewer")) {
                joinCol = "reviewer";
            } else if (criteria.getKey().equalsIgnoreCase("approver")) {
                joinCol = "approver";
            }
            criteria.setKey("fullName");
            Join<CostPlusTemplateProcess, User> joinUser = root.join(joinCol);
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            if (type.equalsIgnoreCase("Report"))
                return builder.and(onAgency, onYear, onStartConfirm, onFilter);
            else
                return builder.and(onAgency, onYear, onFilter);
        }
        else if (criteria.getKey().equalsIgnoreCase("createdTimeStamp")) {
            if (criteria.getValue().length() == 11) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                Date date = new Date();
                try {
                    date = simpleDateFormat.parse(criteria.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                criteria.setValue(sqlDate.toString());
            }

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
            if (type.equalsIgnoreCase("Report"))
                return builder.and(onAgency, onYear, onStartConfirm, onFilter);
            else
                return builder.and(onAgency, onYear, onFilter);
        } else {
            if (criteria.getKey().equalsIgnoreCase("agencyName"))
                criteria.setKey("name");

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue());
            }
            if (type.equalsIgnoreCase("Report"))
                return builder.and(onAgency, onYear, onStartConfirm, onFilter);
            else
                return builder.and(onAgency, onYear, onFilter);
        }

    }
}