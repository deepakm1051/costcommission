package com.costcommission.jpaspecification;


import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.COA;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class COASpecification implements Specification<COA> {

    SpecificationDTO criteria;

    public COASpecification() {

    }

    public COASpecification(final SpecificationDTO specificationDTO) {
        super();
        this.criteria = specificationDTO;
    }

    @Override
    public Predicate toPredicate
            (Root<COA> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (criteria.getKey().equalsIgnoreCase("active") ) {
            boolean value = false;
            if(criteria.getValue().equalsIgnoreCase("Active")) {
                value = true;
            }
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()),  value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notEqual(
                        root.<Integer>get(criteria.getKey()),  value );
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value );
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            }
        }else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                return builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                return builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                return builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
        }
        return null;
    }

}
