package com.costcommission.jpaspecification;


import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CostCenterSpecification implements Specification<CostCenter> {

    SpecificationDTO criteria;
    String type;

    public CostCenterSpecification() {

    }

    public CostCenterSpecification(final SpecificationDTO specificationDTO, String type) {
        super();
        this.criteria = specificationDTO;
        this.type = type;
    }

    @Override
    public Predicate toPredicate
            (Root<CostCenter> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate onType = builder.equal(root.<String>get("type"), type);
        Predicate onFilter = null;
        if (criteria.getOperation().isEmpty()) {
            return builder.and(onType);
        }
        else if(criteria.getKey().equalsIgnoreCase("subCategory")){
            criteria.setKey("name");
            Join<CostCenter, SubCategory> joinParent = root.join("subCategory");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter=builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter=builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter=builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter=builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter=builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter=builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue());
            }
            return builder.and(onType, onFilter);
        }
        else if(criteria.getKey().equalsIgnoreCase("category")){
            criteria.setKey("name");
            Join<CostCenter, SubCategory> joinParent = root.join("subCategory");
            Join<SubCategory, Category> joinSubCategory = joinParent.join("category");
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter=builder.equal(
                        joinSubCategory.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter=builder.notEqual(
                        joinSubCategory.<String>get(criteria.getKey()), criteria.getValue());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter=builder.like(
                        joinSubCategory.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter=builder.notLike(
                        joinSubCategory.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter=builder.like(
                        joinSubCategory.<String>get(criteria.getKey()), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter=builder.like(
                        joinSubCategory.<String>get(criteria.getKey()), "%" + criteria.getValue());
            }
            return builder.and(onType, onFilter);
        }
       else if (criteria.getKey().equalsIgnoreCase("active")) {
            boolean value = false;
            if (criteria.getValue().equalsIgnoreCase("Active")) {
                value = true;
            }
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notEqual(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.equal(
                        root.<Integer>get(criteria.getKey()), value);
            }
            return builder.and(onType, onFilter);
        } else {
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            return builder.and(onType, onFilter);
        }
    }
}
