package com.costcommission.jpaspecification;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.entity.User;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ARInvoiceSpecification implements Specification<CostPlusTemplateProcess> {

    SpecificationDTO criteria;
    Long agencyId;

    public ARInvoiceSpecification() {

    }

    public ARInvoiceSpecification(final SpecificationDTO specificationDTO, Long agencyId) {
        super();
        this.criteria = specificationDTO;
        this.agencyId = agencyId;
    }

    @Override
    public Predicate toPredicate
            (Root<CostPlusTemplateProcess> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Join<CostPlusTemplateProcess, Agency> joinParent = root.join("agency");
        Predicate onClosed = builder.equal(root.<String>get("status"), String.valueOf(CostPlusStatus.AR_INVOICE_REQUEST.ordinal()));
        Predicate onAgency = builder.equal(joinParent.<String>get("id"), agencyId);
        Predicate onFilter = null;
        if (criteria.getOperation().isEmpty()) {
            return builder.and(onAgency, onClosed);
        } else if (criteria.getKey().equalsIgnoreCase("preparer")) {
            String joinCol = "preparer";
            criteria.setKey("fullName");
            Join<CostPlusTemplateProcess, User> joinUser = root.join(joinCol);
            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinUser.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            return builder.and(onAgency, onClosed, onFilter);
        } else if (criteria.getKey().equalsIgnoreCase("arInvoiceDate")) {
            if (criteria.getValue().length() == 11) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
                Date date = new Date();
                try {
                    date = simpleDateFormat.parse(criteria.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                criteria.setValue(sqlDate.toString());
            }
             if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter= builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter= builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter= builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter= builder.notLike(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter= builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), criteria.getValue() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter= builder.like(
                        root.<String>get(criteria.getKey()).as(String.class), "%" + criteria.getValue());
            }
            return builder.and(onAgency, onClosed, onFilter);
        } else {
            if (criteria.getKey().equalsIgnoreCase("agencyName"))
                criteria.setKey("name");

            if (criteria.getOperation().equalsIgnoreCase("EQUAL_TO")) {
                onFilter = builder.equal(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_EQUAL_TO")) {
                onFilter = builder.notEqual(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString());
            } else if (criteria.getOperation().equalsIgnoreCase("CONTAINS")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("NOT_CONTAINS")) {
                onFilter = builder.notLike(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("STARTS_WITH")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), criteria.getValue().toString() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("ENDS_WITH")) {
                onFilter = builder.like(
                        joinParent.<String>get(criteria.getKey()), "%" + criteria.getValue().toString());
            }
            return builder.and(onAgency, onClosed, onFilter);
        }
    }

}