package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CostPlusWorkflowDTO extends BaseDTO {

    private Long costPlusTemplateId;
    private Long userId;
    private String action;
    private String userName;
    private String comments;
    private String createdTimeStamp;


}
