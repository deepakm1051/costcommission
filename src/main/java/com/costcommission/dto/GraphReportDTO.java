package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GraphReportDTO {
    private String region;
    private Long noOfUploaded;
    private Long noOfNotUploaded;
    private Long noOfReviewed;
    private Long noOfRejected;
    private Long noOfReleased;
    private Long noOfApproved;
}
