package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CostPlusRemunerationDTO extends BaseDTO {

    private Long agencyId;
    private Double amount;
    private Double ytdAmount;
    private Double ytdAmountCurrentMonth;
    private Double vatAmount;
    private Double totalAmount;
    private Integer year;
    private String month;
    private Long costCenterId;
    private String costCenterCode;
    private String carrierCode;
    private String carrierName;
    private Long CostPlusTemplateProcessId;

}
