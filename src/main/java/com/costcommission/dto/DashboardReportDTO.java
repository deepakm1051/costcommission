package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DashboardReportDTO {

    private List<RegionWiseReportDTO> tableData;
    private List<GraphReportDTO> graphData;


}
