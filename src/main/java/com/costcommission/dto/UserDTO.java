package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO extends BaseDTO {

    private String fullName;
    private String loginId;
    private String password="1234";
    private String emailId;
    private String location;
    private boolean active;
    private Long designationId;
    private String designationName;
    private Long userAgency;
    private String userMonth;
    private Integer userYear;
    private Long userCurrentSessionId;
    private List<RoleAgencyDTO> roleAgencies;
    private List<RoleAgencyDisplayDTO> roleAgencyDisplay;
    private List<RoleAgencyIdDTO> roleAgencyIds;

}
