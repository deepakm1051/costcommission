package com.costcommission.dto;

import com.costcommission.entity.AgencyVatCarriers;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AgencyDTO extends BaseDTO {

    private String code;
    private String name;
    private Long safEntityNumber;
    private String legalEntityName;
    private String oceanType;
    private Integer markup;
    private boolean headCountFlag;
    private String fcCcy;
    private String location;
    private String port;
    private String ledgerName;
    private String company;
    private String site;
    private String debitAccount;
    private String creditAccount;
    private String natureCode;
    private boolean vatGstFlag;
    private Integer vatPercentage;
    private String vatAccount;
    private Long clusterId;
    private String clusterName;
    private String arTempRecipient;
    private String managerTempRecipient;
    private String agencyTempRecipient;
    private boolean active;
    private boolean vatDaLine;
    private boolean vatAccrual;
    private boolean vatAllCarrier;
    private List<Long> costCenterIds;
    private List<String> displayCarriers;

}
