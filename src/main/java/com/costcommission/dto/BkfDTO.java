package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BkfDTO {

    private String costCenterCode;
    private String costCenterName;
    private String carrierName;

}
