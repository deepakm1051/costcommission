package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CostPlusDTO extends BaseDTO {

    private Long agencyId;
    private String month;
    private Integer year;
    private String preparer;
    private String reviewer;
    private String approver;
    private int status;
    private String displayStatus;
    private String agencyName;
    private String createdTimeStamp;
    private String approvedTimeStamp;
    private String fileName;
    private List<CostPlusWorkflowDTO> costPlusWorkflows;
    private List<String> reviewers;
    private List<String> approvers;


}
