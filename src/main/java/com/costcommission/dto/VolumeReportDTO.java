package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VolumeReportDTO extends BaseDTO {

    private Long year;
    private String month;
    private String mtdOrYtd;
    private Double countOfBOLNumber;
    private Double sumOfTues;
    private String type;
    private Long costCenterId;
    private String carrierName;

}
