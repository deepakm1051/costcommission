package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgencyWiseReportDTO {
    private Long agencyId;
    private String agencyName;
    private boolean isUploaded;
    private boolean isNotUploaded;
    private boolean isReviewed;
    private boolean isRejected;
    private boolean isReleased;
    private boolean isApproved;
}
