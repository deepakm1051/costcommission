package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeriodControlDTO extends BaseDTO {

    private Long agencyId;
    private String agencyName;
    private String month;
    private String year;
    private boolean status;

}
