package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrialBalanceR12DTO {

    private String ledgerName;
    private String periodName;
    private String amountType;
    private String journalType;
    private String company;
    private String site;
    private String bu;
    private String buDescription;
    private String account;
    private String accountDescription;
    private String nature;
    private String natureDescription;
    private String partner;
    private String partnerDescription;
    private String currencyCode;
    private String beginBalance;
    private String periodNetDebit;
    private String periodNetCredit;
    private String closingBalance;

}
