package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoleAgencyDisplayDTO {

    private String roleName;
    private List<String> agencyNames;
}
