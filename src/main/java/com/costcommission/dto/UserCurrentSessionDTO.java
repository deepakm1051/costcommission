package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCurrentSessionDTO extends BaseDTO {

    private Long userId;
    private Long agencyId;
    private String month;
    private Integer year;
}
