package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CostPlusTemplateDTO extends BaseDTO {

    private Long agencyId;
    private String month;
    private Integer year;
    private String preparer;
    private String reviewer;
    private String approver;
    private int status;
    private String displayStatus;
    private String agencyName;
    private boolean headCountApplicable;
    private String fileName;
    private String createdTimeStamp;
    private String arInvoiceDate;


}
