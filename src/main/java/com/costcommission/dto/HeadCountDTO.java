package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeadCountDTO extends BaseDTO {

    private boolean period;
    private Long agencyId;
    private String month;
    private Integer year;
    private Long allCCBEmployees;
    private Long inWhichContainerShip;

}
