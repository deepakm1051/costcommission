package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TBVolumeTransactionDTO extends BaseDTO {

    private String fileName;
    private String displayFileName;
    private String displayCreatedBy;
    private Long agencyId;
    private String agencyName;
    private String month;
    private Integer year;
    private String type;
    private String fileType;
    private String createdTimeStamp;
}
