package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdiJournalDTO  extends BaseDTO{

    private String journalFileName;
    private String journalType;
    private String agencyName;
    private String createdTimeStamp;

}
