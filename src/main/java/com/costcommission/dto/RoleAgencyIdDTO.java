package com.costcommission.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoleAgencyIdDTO {
    private Long roleId;
    private List<Long> agencyIds;

}
