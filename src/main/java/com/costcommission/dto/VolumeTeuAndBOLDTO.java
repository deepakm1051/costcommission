package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VolumeTeuAndBOLDTO {

    private String carrier;
    private String BOLNumber;
    private Double totalVolumeInTEU;
    private Double lostSlot;
}
