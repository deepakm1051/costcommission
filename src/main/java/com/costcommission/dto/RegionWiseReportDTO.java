package com.costcommission.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RegionWiseReportDTO {
    private String region;
    private List<AgencyWiseReportDTO> agencyData;
}
