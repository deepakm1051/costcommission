package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Role extends BaseEntity {

    @Column(unique = true, length = 100)
    private String name;

}
