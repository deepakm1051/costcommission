package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class AgencyVatCarriers extends BaseEntity {

    private Long costCenterId;
    @ManyToOne
    @JoinColumn(name="agency_id")
    private Agency agency;
}
