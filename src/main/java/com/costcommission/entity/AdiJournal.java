package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class AdiJournal extends BaseEntity{

    @Column(length = 200)
    private String journalFileName;
    @Column(length = 10)
    private String journalType; //Actual //Accrual

    @ManyToOne
    @JoinColumn(name="cost_plus_id")
    private CostPlusTemplateProcess costPlusTemplate;

}
