package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class TBVolumeTransaction extends BaseEntity{

    //Audit of Tb , volume and head count update.

    @Column(length = 300)
    private String fileName;
    @Column(length = 300)
    private String displayFileName;
    @Column(length = 3)
    private String month;
    private Integer year;
    @Column(length = 30)
    private String type; //Export booked //Export loaded // Import // Transhipment
    @Column(length = 20)
    private String fileType;// TRIALBALANCE // volume // headcount // COA

    @ManyToOne
    @JoinColumn(name="agency_id")
    private Agency agency;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;



}