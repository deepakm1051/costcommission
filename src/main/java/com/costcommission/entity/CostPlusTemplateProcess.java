package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
public class CostPlusTemplateProcess extends BaseEntity{

    //volume extraction manual or automatic - tbvoltrnas,actualprocess, period control
    // D+5 to  D+10

    @Column(length = 3)
    private String month;
    private Integer year;
    private int status;
    private String fileName;
    private LocalDateTime arInvoiceDate;
    private LocalDateTime approvedTimeStamp;

    @ManyToOne
    @JoinColumn(name="agency_id")
    private Agency agency;

    @ManyToOne
    @JoinColumn(name="preparer_id")
    private User preparer;

    @ManyToOne
    @JoinColumn(name="reviewer_id")
    private User reviewer;

    @ManyToOne
    @JoinColumn(name="approver_id")
    private User approver;

    @OneToMany(mappedBy = "costPlusTemplateProcess", cascade= CascadeType.ALL)
    List<CostPlusWorkflow> costPlusWorkflows;

    @OneToMany(mappedBy = "costPlusTemplate", cascade= CascadeType.ALL)
    List<CostPlusRemuneration> costPlusRemunerations;

    @OneToMany(mappedBy = "costPlusTemplate", cascade= CascadeType.ALL)
    List<AdiJournal> adiJournals;


}
