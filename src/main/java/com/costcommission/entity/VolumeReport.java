package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class  VolumeReport extends BaseEntity {

    @Column(length = 4)
    private Integer year;
    @Column(length = 3)
    private String month;
    @Column(length = 3)
    private String mtdOrYtd;
    private Double countOfBOLNumber;
    private Double sumOfTues;
    @Column(length = 30)
    private String type;
    private Long costCenterId;
    private Long agencyId;

}
