package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Agency extends BaseEntity {

    @Column(unique = true, nullable = false, length = 10)
    private String code;
    @Column(unique = true, nullable = false, length = 100)
    private String name;
    @Column(length = 50)
    private Long safEntityNumber;
    @Column(length = 100)
    private String legalEntityName;
    @Column(length = 3)
    private String oceanType;
    @Column(length = 3)
    private int markup;
    @Column(length = 3)
    private boolean headCountFlag;
    @Column(length = 4)
    private String fcCcy;
    @Column(length = 100)
    private String location;
    @Column(length = 100)
    private String port;
    @Column(length = 100)
    private String ledgerName;
    @Column(length = 100)
    private String company;
    @Column(length = 100)
    private String site;
    @Column(length = 100)
    private String debitAccount;
    @Column(length = 100)
    private String creditAccount;
    @Column(length = 50)
    private String natureCode;
    private boolean vatGstFlag;
    private Integer vatPercentage;
    @Column(length = 100)
    private String vatAccount;
    @Column(length = 300)
    private String arTempRecipient;
    @Column(length = 300)
    private String managerTempRecipient;
    @Column(length = 300)
    private String agencyTempRecipient;
    private boolean active = true;
    private boolean vatDaLine;
    private boolean vatAccrual;
    private boolean vatAllCarrier;

    @ManyToOne
    @JoinColumn(name = "cluster_id")
    private Cluster cluster;

    @OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
    List<TBVolumeTransaction> tbVolumeTransactions;

    @OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
    List<AgencyVatCarriers> vatCarriers;

    @OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
    List<PeriodControl> periodControls;

    @OneToMany(mappedBy = "agency", cascade = CascadeType.ALL)
    List<CostPlusTemplateProcess> costPlusTemplateProcesses;


}
