package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class CostPlusWorkflow  extends BaseEntity{

    @Column(length = 300)
    private String comments;
    private String action;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="cost_plus_id")
    private CostPlusTemplateProcess costPlusTemplateProcess;


}
