package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Category extends BaseEntity {

    @Column(unique = true, length = 100)
    private String name;
    private Integer categorySeq;

    @OneToMany(mappedBy = "category", cascade= CascadeType.ALL)
    List<SubCategory> subCategories;

}
