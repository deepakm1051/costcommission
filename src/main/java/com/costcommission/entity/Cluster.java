package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Cluster extends BaseEntity{

    @Column(unique = true,nullable = false,length = 3)
    private String code;
    @Column(unique = true,nullable = false,length = 100)
    private String name;
    
     private boolean active=true;

    @ManyToOne
    @JoinColumn(name="region_id")
    private Region region;

    @OneToMany(mappedBy = "cluster",cascade= CascadeType.ALL)
    List<Agency> agencies;
}
