package com.costcommission.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class User extends BaseEntity {

    @Column(length = 100)
    private String fullName;
    @Column(length = 100)
    private String loginId;
    private String password;
    @Column(length = 100)
    private String emailId;
    @Column(length = 100)
    private String location;
    
     private boolean active=true;

    @ManyToOne
    @JoinColumn(name="designation_id")
    private Designation designation;

    @OneToMany(mappedBy = "user", cascade= CascadeType.ALL)
    List<CostPlusWorkflow> costPlusWorkflows;

    @OneToMany(mappedBy = "user", cascade= CascadeType.ALL)
    List<TBVolumeTransaction> tbVolumeTransactions;

    @OneToMany(mappedBy = "preparer", cascade = CascadeType.ALL)
    List<CostPlusTemplateProcess> costPlusTemplateProcesses;

    @OneToMany(mappedBy = "reviewer", cascade = CascadeType.ALL)
    List<CostPlusTemplateProcess> templateProcesses;

    @OneToMany(mappedBy = "approver", cascade = CascadeType.ALL)
    List<CostPlusTemplateProcess> plusTemplateProcesses;

}
