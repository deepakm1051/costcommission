package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class PeriodControl extends BaseEntity{

   // private Long agencyId;
    @Column(length = 3)
    private String month;
    private Integer year;
    private boolean status;

    @ManyToOne
    @JoinColumn(name="agency_id")
    private Agency agency;

}
