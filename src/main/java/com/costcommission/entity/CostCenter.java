package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class CostCenter extends BaseEntity{

    @Column(length = 20)
    private String code;
    @Column(length = 100)
    private String name;
    private Integer sequence;
    @Column(length = 100)
    private String keyAllocation;
    @Column(length = 3)
    private String type;
    @Column(length = 50)
    private String carrierCode;
    @Column(length = 100)
    private String carrierName;
    
     private boolean active=true;

    @ManyToOne
    @JoinColumn(name="sub_category_id")
    private SubCategory subCategory;
}
