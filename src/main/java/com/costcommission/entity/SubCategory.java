package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class SubCategory extends BaseEntity {

    @Column(unique = true, length = 100)
    private String name;
    private Integer subCategorySeq;

    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

    @OneToMany(mappedBy = "subCategory",cascade= CascadeType.ALL)
    private List<CostCenter> costCenters;

}
