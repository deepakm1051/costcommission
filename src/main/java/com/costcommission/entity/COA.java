package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class COA extends BaseEntity{

    @Column(length = 50)
    private String natureCode;
    @Column(length = 100)
    private String natureDesc;
    @Column(length = 100)
    private String description;
    @Column(length = 100)
    private String safranpl;
    @Column(length = 100)
    private String safranplDetails;
    @Column(length = 50)
    private String agencyBudgetCode;
    @Column(length = 100)
    private String agencyBudgetLine;
    @Column(length = 100)
    private String mandatoryAllocPerCC;
    @Column(length = 100)
    private String unallocatedExpenses;
    @Column(length = 100)
    private String costCenterAllocation;
    @Column(length = 100)
    private String natureNotRelevant;
    @Column(length = 300)
    private String rationale;
    @Column(length = 100)
    private String costPlusMatrix;
    @Column(length = 100)
    private String descriptionEnglish;
    
     private boolean active=true;


}
