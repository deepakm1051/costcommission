package com.costcommission.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class HeadCount extends BaseEntity{

    private Long agencyId;
    @Column(length = 3)
    private String month;
    private Integer year;
    private Long allCCBEmployees;
    private Long inWhichContainerShip;

}
