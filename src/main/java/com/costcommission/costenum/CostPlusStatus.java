package com.costcommission.costenum;

public enum CostPlusStatus {

    OPEN,
    VOLUME_CONFIRMED,
    TB_BY_COST_CENTER_CONFIRMED,
    HEAD_COUNT_CONFIRMED,
    COST_PLUS_CALCULATION_START,
    SENT_FOR_APPROVAL,
    REVIEWED,
    REJECTED,
    RELEASED,
    CLOSED,
    AR_INVOICE_REQUEST;

    public static final CostPlusStatus[] values = values();
    public static CostPlusStatus get(int ordinal) { return values[ordinal]; }

}
