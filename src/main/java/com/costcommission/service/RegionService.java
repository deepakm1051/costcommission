package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.RegionDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Region;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.RegionSpecification;
import com.costcommission.repository.ClusterRepository;
import com.costcommission.repository.RegionRepository;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

@Service
public class RegionService {

    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private ClusterRepository clusterRepository;
    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private EnvConfiguration envConfiguration;

    public RegionDTO create(RegionDTO regionDTO) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        try {
            Long countCode = regionRepository.countByCode(regionDTO.getCode().trim().toLowerCase());
            Long countName = regionRepository.countByName(regionDTO.getName().trim().toLowerCase());
            if (countCode > 0) {
                throw new CostPlusException("Region Code already exists");
            } else if (countName > 0) {
                throw new CostPlusException("Region already exists");
            } else {
                Region region = modelMapper.map(regionDTO, Region.class);
                return modelMapper.map(regionRepository.save(region), RegionDTO.class);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public RegionDTO update(RegionDTO regionDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Region savedRegion = regionRepository.findByNameEqualsIgnoreCase(regionDTO.getName());
        if (savedRegion != null && !savedRegion.getId().equals(regionDTO.getId())) {
            throw new CostPlusException("Region already exists");
        } else {
            Region region = modelMapper.map(regionDTO, Region.class);
            return modelMapper.map(regionRepository.save(region), RegionDTO.class);
        }
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {
            if (!status) {
                Long count = clusterRepository.countActiveRegion(id);
                if (count > 0) {
                    throw new CostPlusException("Region is in use.");
                } else {
                    return regionRepository.enableOrDisable(status,userId,id);
                }
            } else {
                return regionRepository.enableOrDisable(status,userId,id);
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<RegionDTO> allActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Region> regions = regionRepository.allActive();
            Type listType = new TypeToken<List<RegionDTO>>() {
            }.getType();
            return mapper.map(regions, listType);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<RegionDTO> allIsActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Region> regions = regionRepository.findRegionByActive(true);
            Type listType = new TypeToken<List<RegionDTO>>() {
            }.getType();
            return mapper.map(regions, listType);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, int page, int size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        RegionSpecification spec = new RegionSpecification(specificationDTO);
        Page<Region> results = regionRepository.findAll(spec, pageable);
        Type listType = new TypeToken<List<RegionDTO>>() {
        }.getType();
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(modelMapper.map(results.getContent(), listType));
        return paginationResponseDTO;
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "RegionList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createRegionSheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createRegionSheet(SXSSFSheet sheet, SXSSFWorkbook workbook) throws Exception {
        int rowCount = 0;
        String[] header = {"Sl No", "Code", "Name"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<RegionDTO> regionDTOList = allActive();
        for (RegionDTO regionDTO : regionDTOList) {
            rowCount = rowCount + 1;
            columnCount = 0;
            Row row = sheet.createRow(rowCount);
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, regionDTO.getCode(), sheet);
            excelUtil.createCell(row, ++columnCount, regionDTO.getName(), sheet);
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}
