package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.CostPlusTemplateDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.CostPlusReportSpecification;
import com.costcommission.repository.CostPlusTemplateRepository;
import com.costcommission.repository.UserRepository;
import com.costcommission.util.S3BucketUtil;
import org.apache.commons.compress.utils.IOUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CostPlusReportService {

    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EnvConfiguration envConfiguration;

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO, Long agencyId, Integer year) {
       /* List<Tuple> agencies =userRepository.findAgencyMappedToUser(2L);
        List<Long> agencyIds = new ArrayList<>();
        for (Tuple tuple : agencies){
            agencyIds.add(Long.parseLong(tuple.get(0).toString()));
        }*/
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        CostPlusReportSpecification spec = new CostPlusReportSpecification(specificationDTO,agencyId,year,"Report");
        Page<CostPlusTemplateProcess> results = costPlusTemplateRepository.findAll(spec, pageable);
        List<CostPlusTemplateDTO> costPlusTemplateDTOS = new ArrayList<>();
        for (CostPlusTemplateProcess costPlusTemplateProcess :
                results) {
            CostPlusTemplateDTO costPlusTemplateDTO = new CostPlusTemplateDTO();
            costPlusTemplateDTO.setApprover(costPlusTemplateProcess.getApprover()!=null?costPlusTemplateProcess.getApprover().getFullName():"NA");
            costPlusTemplateDTO.setPreparer(costPlusTemplateProcess.getPreparer()!=null?costPlusTemplateProcess.getPreparer().getFullName():"NA");
            costPlusTemplateDTO.setReviewer(costPlusTemplateProcess.getReviewer()!=null?costPlusTemplateProcess.getReviewer().getFullName():"NA");
            costPlusTemplateDTO.setCreatedTimeStamp(costPlusTemplateProcess.getCreatedTimeStamp().toString());
            costPlusTemplateDTO.setId(costPlusTemplateProcess.getId());
            costPlusTemplateDTO.setAgencyId(costPlusTemplateProcess.getAgency().getId());
            costPlusTemplateDTO.setMonth(costPlusTemplateProcess.getMonth());
            costPlusTemplateDTO.setYear(costPlusTemplateProcess.getYear());
            costPlusTemplateDTO.setFileName(costPlusTemplateProcess.getFileName());
            costPlusTemplateDTO.setDisplayStatus(CostPlusStatus.get(costPlusTemplateProcess.getStatus()).name().replaceAll("_"," ").toLowerCase());
            costPlusTemplateDTOS.add(costPlusTemplateDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(costPlusTemplateDTOS);
        return paginationResponseDTO;

    }

    public ResponseEntity<ByteArrayResource> export(Long costPlusId) throws Exception {
        InputStream file = null;
        try {
            CostPlusTemplateProcess templateProcess = costPlusTemplateRepository.findCostPlusTemplateById(costPlusId);
            String filePath=envConfiguration.getAwsBucketPath().concat("/Reports/COSTPLUS/");
            String fileName = templateProcess.getFileName();
            /*boolean isExist= S3BucketUtil.fileExist(fileName,envConfiguration.getAwsBucketName(),filePath);
            if(isExist){
                file =  S3BucketUtil.getFileInputStream(fileName,envConfiguration.getAwsBucketName(),filePath);
            }else{
                throw new CostPlusException("Cost+ Remuneration template file does not exist");
            }*/
            file = new FileInputStream(filePath+fileName);
            byte[] bytes = IOUtils.toByteArray(file);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(bytes));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }finally {
            Objects.requireNonNull(file).close();
        }
    }
}
