package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.*;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.ADIJournalSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.S3BucketUtil;
import org.apache.commons.compress.utils.IOUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ADIJournalService {

    @Autowired
    private ADIJournalRepository adiJournalRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private EnvConfiguration envConfiguration;

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, String journalType, Long agencyId, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        ADIJournalSpecification spec = new ADIJournalSpecification(specificationDTO, journalType,agencyId);
        Page<AdiJournal> results = adiJournalRepository.findAll(spec, pageable);
        List<AdiJournalDTO> adiJournalDTOS = new ArrayList<>();
        for (AdiJournal adiJournal : results) {
            AdiJournalDTO adiJournalDTO = modelMapper.map(adiJournal, AdiJournalDTO.class);
            adiJournalDTO.setJournalFileName(adiJournal.getJournalFileName());
            adiJournalDTO.setCreatedTimeStamp(adiJournal.getCreatedTimeStamp().toString());
            CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findCostPlusTemplateById(adiJournal.getCostPlusTemplate().getId());
            Agency agency = agencyRepository.findAgencyById(costPlusTemplateProcess.getAgency().getId());
            adiJournalDTO.setAgencyName(agency.getName());
            adiJournalDTOS.add(adiJournalDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(adiJournalDTOS);
        return paginationResponseDTO;
    }

    public ResponseEntity<ByteArrayResource> export(Long journalId) throws Exception {
            InputStream journalFile = null;
        try {
            AdiJournal adiJournal = adiJournalRepository.findAdiJournalById(journalId);
            String filePath=envConfiguration.getAwsBucketPath().concat("/Reports/").concat("ADIJOURNAL/");
            String fileName = adiJournal.getJournalFileName();
            /*boolean isExist= S3BucketUtil.fileExist(fileName,envConfiguration.getAwsBucketName(),filePath);
            if(isExist){
                journalFile =  S3BucketUtil.getFileInputStream(fileName,envConfiguration.getAwsBucketName(),filePath);
            }else{
                throw new CostPlusException("ADI journal file does not exist");
            }*/
            journalFile = new FileInputStream(filePath+fileName);
            byte[] bytes = IOUtils.toByteArray(journalFile);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(bytes));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }finally {
            Objects.requireNonNull(journalFile).close();
        }
    }

}
