package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.*;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.UserSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayOutputStream;
import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleAgencyRepository userRoleAgencyRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private DesignationRepository designationRepository;

    @Autowired
    private AgencyRepository agencyRepository;

    @Autowired
    private EnvConfiguration envConfiguration;

    @Autowired
    private ExcelUtil excelUtil;

    public void create(UserDTO userDTO) throws Exception {
        try {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
            User user = modelMapper.map(userDTO, User.class);
            User savedUserEmailId = userRepository.findByEmailIdIgnoreCase(userDTO.getEmailId());
            User savedUserLoginId = userRepository.findByLoginIdIgnoreCase(userDTO.getLoginId());
            if (savedUserEmailId != null) {
                throw new CostPlusException("User already exists! Choose different Email Id");
            } else if (savedUserLoginId != null) {
                throw new CostPlusException("User already exists! Choose different Login Id");
            } else {
                if (CollectionUtils.isEmpty(userDTO.getRoleAgencies())) {
                    throw new Exception("Role agency cannot be empty");
                }
                Designation designation = designationRepository.findDesignationById(userDTO.getDesignationId());
                user.setDesignation(designation);
                userRepository.save(user);
                Long roleId = null, savedRoleId = null;
                for (RoleAgencyDTO roleAgencyDTO : userDTO.getRoleAgencies()) {
                    if (roleId == null || !roleId.equals(roleAgencyDTO.getRoleId())) {
                        savedRoleId = createUserRole(roleAgencyDTO, user);
                        createUserRoleAgency(roleAgencyDTO, savedRoleId, user);
                        roleId = roleAgencyDTO.getRoleId();
                    } else if (roleId.equals(roleAgencyDTO.getRoleId())) {
                        createUserRoleAgency(roleAgencyDTO, savedRoleId, user);
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public Long createUserRole(RoleAgencyDTO roleAgencyDTO, User user) {
        UserRole userRole = new UserRole();
        userRole.setRoleId(roleAgencyDTO.getRoleId());
        userRole.setUserId(user.getId());
        userRole.setCreatedBy(user.getCreatedBy());
        userRoleRepository.save(userRole);
        return userRole.getId();
    }


    public void createUserRoleAgency(RoleAgencyDTO roleAgencyDTO, Long roleId, User user) {
        UserRoleAgency userRoleAgency = new UserRoleAgency();
        userRoleAgency.setUserRoleId(roleId);
        userRoleAgency.setAgencyId(roleAgencyDTO.getAgencyId());
        userRoleAgency.setCreatedBy(user.getCreatedBy());
        userRoleAgencyRepository.save(userRoleAgency);
    }

    public void update(UserDTO userDTO) throws Exception {
        try {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
            User user = modelMapper.map(userDTO, User.class);
            if (CollectionUtils.isEmpty(userDTO.getRoleAgencies())) {
                throw new Exception("Role agency cannot be empty");
            }
            Designation designation = designationRepository.findDesignationById(userDTO.getDesignationId());
            user.setDesignation(designation);
            userRepository.save(user);
            List<Long> activeUserRoleIds = new ArrayList<>();
            List<Long> activeRoleAgencyIds = new ArrayList<>();
            List<Long> savedRoleIds = new ArrayList<>();
            List<UserRole> userRoles = userRoleRepository.findByUserId(userDTO.getId());
            for (UserRole userRole : userRoles) {
                savedRoleIds.add(userRole.getId());
            }
            for (RoleAgencyDTO roleAgencyDTO : userDTO.getRoleAgencies()) {
                if (roleAgencyDTO.getUserRoleId() == null && roleAgencyDTO.getUserRoleAgencyId() != null) {
                    activeUserRoleIds.add(0L);
                    activeRoleAgencyIds.add(0L);
                } else {
                    activeUserRoleIds.add(roleAgencyDTO.getUserRoleId());
                    if (roleAgencyDTO.getUserRoleAgencyId() != null)
                        activeRoleAgencyIds.add(roleAgencyDTO.getUserRoleAgencyId());
                    else
                        activeRoleAgencyIds.add(0L);
                }
            }
            deleteUserRole(activeUserRoleIds, user.getId());
            deleteUserRoleAgency(activeRoleAgencyIds, savedRoleIds);
            Long roleId = null, savedRoleId = null;
            for (RoleAgencyDTO roleAgencyDTO : userDTO.getRoleAgencies()) {
                if (roleAgencyDTO.getUserRoleAgencyId() != null && roleAgencyDTO.getUserRoleId() != null) {
                    updateUserRole(roleAgencyDTO, user);
                    updateUserRoleAgency(roleAgencyDTO,user);
                } else if (roleAgencyDTO.getUserRoleAgencyId() == null && roleAgencyDTO.getUserRoleId() != null) {
                    createUserRoleAgency(roleAgencyDTO, roleAgencyDTO.getUserRoleId(),user);
                } else {
                    if (roleId == null || !roleId.equals(roleAgencyDTO.getRoleId())) {
                        savedRoleId = createUserRole(roleAgencyDTO, user);
                        createUserRoleAgency(roleAgencyDTO, savedRoleId, user);
                        roleId = roleAgencyDTO.getRoleId();
                    } else if (roleId.equals(roleAgencyDTO.getRoleId())) {
                        createUserRoleAgency(roleAgencyDTO, savedRoleId, user);
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void updateUserRole(RoleAgencyDTO roleAgencyDTO, User user) {
        UserRole userRole = new UserRole();
        userRole.setId(roleAgencyDTO.getUserRoleId());
        userRole.setRoleId(roleAgencyDTO.getRoleId());
        userRole.setUserId(user.getId());
        userRole.setUpdatedBy(user.getUpdatedBy());
        userRoleRepository.save(userRole);
    }

    public void updateUserRoleAgency(RoleAgencyDTO roleAgencyDTO, User user) {
        UserRoleAgency userRoleAgency = new UserRoleAgency();
        userRoleAgency.setId(roleAgencyDTO.getUserRoleAgencyId());
        userRoleAgency.setUserRoleId(roleAgencyDTO.getUserRoleId());
        userRoleAgency.setAgencyId(roleAgencyDTO.getAgencyId());
        userRoleAgency.setUpdatedBy(user.getUpdatedBy());
        userRoleAgencyRepository.save(userRoleAgency);
    }

    public void deleteUserRole(List<Long> activeUserRoleIds, Long userId) {
        userRoleRepository.disableById(activeUserRoleIds, userId);
    }

    public void deleteUserRoleAgency(List<Long> activeRoleAgencyIds, List<Long> savedRoleIds) {
        userRoleAgencyRepository.disableById(activeRoleAgencyIds, savedRoleIds);
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {
            if (!status) {
                List<Long> savedRoleIds = new ArrayList<>();
                List<UserRole> userRoles = userRoleRepository.findByUserId(id);
                for (UserRole userRole : userRoles) {
                    savedRoleIds.add(userRole.getId());
                }
                userRoleRepository.disableByUserId(id);
                userRoleAgencyRepository.disableByRoleId(savedRoleIds);
            }
            return userRepository.enableOrDisable(status, userId,id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, int page, int size, boolean
            asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        List<UserDTO> userDTOS = new ArrayList<>();
        UserSpecification spec = new UserSpecification(specificationDTO);
        Page<User> users = userRepository.findAll(spec, pageable);
        for (User user : users) {
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            userDTO.setDesignationId(user.getDesignation().getId());
            userDTO.setDesignationName(user.getDesignation().getName());
            List<UserRole> userRoles = userRoleRepository.findByUserId(user.getId());
            List<RoleAgencyDTO> roleAgencyDTOS = new ArrayList<>();
            List<RoleAgencyDisplayDTO> roleAgencyDisplayDTOS = new ArrayList<>();
            List<RoleAgencyIdDTO> roleAgencyIdDTOS = new ArrayList<>();
            String roleName = "", agencyName = "";
            Long roleId = null, agencyId = null;
            List<String> agencies = null;
            List<Long> agencyIds = null;
            Map<String, List<String>> roleAgencyMap = new HashMap<>();
            Map<Long, List<Long>> roleAgencyIdMap = new HashMap<>();
            for (UserRole userRole : userRoles) {
                Role role = roleRepository.findRoleById(userRole.getRoleId());
                List<UserRoleAgency> userRoleAgencies = userRoleAgencyRepository.findByUserRoleId(userRole.getId());
                for (UserRoleAgency userRoleAgency : userRoleAgencies) {
                    Agency agency = agencyRepository.findAgencyById(userRoleAgency.getAgencyId());
                    RoleAgencyDTO roleAgencyDTO = new RoleAgencyDTO();
                    roleAgencyDTO.setRoleId(role.getId());
                    roleAgencyDTO.setRoleName(role.getName());
                    roleAgencyDTO.setAgencyId(agency.getId());
                    roleAgencyDTO.setAgencyName(agency.getName());
                    roleAgencyDTO.setUserRoleId(userRole.getId());
                    roleAgencyDTO.setUserRoleAgencyId(userRoleAgency.getId());
                    roleAgencyDTOS.add(roleAgencyDTO);

                    if (!roleName.equals(role.getName())) {
                        roleName = role.getName();
                        roleId = role.getId();
                        agencies = new ArrayList<>();
                        agencyIds = new ArrayList<>();
                    }
                    if (!agencyName.equals(agency.getName())) {
                        agencyName = agency.getName();
                        agencyId = agency.getId();
                        assert agencies != null;
                        agencies.add(agencyName);
                        agencyIds.add(agencyId);
                    }
                    if (roleAgencyMap.containsKey(roleName)) {
                        roleAgencyMap.replace(roleName, agencies);
                        roleAgencyIdMap.replace(roleId, agencyIds);
                    } else {
                        roleAgencyMap.put(roleName, agencies);
                        roleAgencyIdMap.put(roleId, agencyIds);
                    }
                }
            }
            for (Map.Entry<String, List<String>> entry :
                    roleAgencyMap.entrySet()) {
                RoleAgencyDisplayDTO roleAgencyDisplayDTO = new RoleAgencyDisplayDTO();
                roleAgencyDisplayDTO.setRoleName(entry.getKey());
                roleAgencyDisplayDTO.setAgencyNames(entry.getValue());
                roleAgencyDisplayDTOS.add(roleAgencyDisplayDTO);
            }
            for (Map.Entry<Long, List<Long>> entry :
                    roleAgencyIdMap.entrySet()) {
                RoleAgencyIdDTO roleAgencyIdDTO = new RoleAgencyIdDTO();
                roleAgencyIdDTO.setRoleId(entry.getKey());
                roleAgencyIdDTO.setAgencyIds(entry.getValue());
                roleAgencyIdDTOS.add(roleAgencyIdDTO);
            }
            userDTO.setRoleAgencies(roleAgencyDTOS);
            userDTO.setRoleAgencyDisplay(roleAgencyDisplayDTOS);
            userDTO.setRoleAgencyIds(roleAgencyIdDTOS);
            userDTOS.add(userDTO);
        }
        paginationResponseDTO.setTotalSize(users.getTotalElements());
        paginationResponseDTO.setData(userDTOS);
        return paginationResponseDTO;
    }

    public List<UserDTO> allActive() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        List<UserDTO> userDTOS = new ArrayList<>();
        List<User> users = userRepository.findAll();
        for (User user : users) {
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            userDTO.setDesignationId(user.getDesignation().getId());
            userDTO.setDesignationName(user.getDesignation().getName());
            List<UserRole> userRoles = userRoleRepository.findByUserId(user.getId());
            List<RoleAgencyDTO> roleAgencyDTOS = new ArrayList<>();
            List<RoleAgencyDisplayDTO> roleAgencyDisplayDTOS = new ArrayList<>();
            String roleName = "", agencyName = "";
            List<String> agencies = null;
            Map<String, List<String>> roleAgencyMap = new HashMap<>();
            for (UserRole userRole : userRoles) {
                Role role = roleRepository.findRoleById(userRole.getRoleId());
                List<UserRoleAgency> userRoleAgencies = userRoleAgencyRepository.findByUserRoleId(userRole.getId());
                for (UserRoleAgency userRoleAgency : userRoleAgencies) {
                    Agency agency = agencyRepository.findAgencyById(userRoleAgency.getAgencyId());
                    RoleAgencyDTO roleAgencyDTO = new RoleAgencyDTO();
                    roleAgencyDTO.setRoleId(role.getId());
                    roleAgencyDTO.setRoleName(role.getName());
                    roleAgencyDTO.setAgencyId(agency.getId());
                    roleAgencyDTO.setAgencyName(agency.getName());
                    roleAgencyDTO.setUserRoleId(userRole.getId());
                    roleAgencyDTO.setUserRoleAgencyId(userRoleAgency.getId());
                    roleAgencyDTOS.add(roleAgencyDTO);

                    if (!roleName.equals(role.getName())) {
                        roleName = role.getName();
                        agencies = new ArrayList<>();
                    }
                    if (!agencyName.equals(agency.getName())) {
                        agencyName = agency.getName();
                        agencies.add(agencyName);
                    }
                    if (roleAgencyMap.containsKey(roleName)) {
                        roleAgencyMap.replace(roleName, agencies);
                    } else {
                        roleAgencyMap.put(roleName, agencies);
                    }
                }
            }
            for (Map.Entry<String, List<String>> entry :
                    roleAgencyMap.entrySet()) {
                RoleAgencyDisplayDTO roleAgencyDisplayDTO = new RoleAgencyDisplayDTO();
                roleAgencyDisplayDTO.setRoleName(entry.getKey());
                roleAgencyDisplayDTO.setAgencyNames(entry.getValue());
                roleAgencyDisplayDTOS.add(roleAgencyDisplayDTO);
            }
            userDTO.setRoleAgencies(roleAgencyDTOS);
            userDTO.setRoleAgencyDisplay(roleAgencyDisplayDTOS);
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "UserList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createUserSheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createUserSheet(SXSSFSheet sheet, SXSSFWorkbook workbook) {
        int rowCount = 0, userRow = 0;
        String[] header = {"Sl No", "Full Name", "Email Id", "Login Id", "Designation", "Location", "User Status"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<UserDTO> userDTOS = allActive();
        for (UserDTO userDTO : userDTOS) {
            rowCount = rowCount + 1;
            userRow = sheet.getLastRowNum();
            columnCount = 0;
            Row row = sheet.createRow(++userRow);
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.getFullName(), sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.getEmailId(), sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.getLoginId(), sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.getDesignationName(), sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.getLocation(), sheet);
            excelUtil.createCell(row, ++columnCount, userDTO.isActive() ? "Active" : "In-Active", sheet);

            for (RoleAgencyDisplayDTO roleAgencyDisplayDTO : userDTO.getRoleAgencyDisplay()) {
                columnCount = 1;
                int roleRow = sheet.getLastRowNum();
                Row roleAgencyRow = sheet.createRow(++roleRow);
                String agencyCommaSeparated = String.join(",", roleAgencyDisplayDTO.getAgencyNames());
                excelUtil.createHeaderCell(roleAgencyRow, columnCount, "Role", sheet, workbook);
                excelUtil.createCell(roleAgencyRow, ++columnCount, roleAgencyDisplayDTO.getRoleName(), sheet);
                excelUtil.createHeaderCell(roleAgencyRow, ++columnCount, "Agencies", sheet, workbook);
                excelUtil.createCell(roleAgencyRow, ++columnCount, agencyCommaSeparated, sheet);
            }
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}