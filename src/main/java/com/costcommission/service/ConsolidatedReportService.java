package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.costenum.MonthEnum;
import com.costcommission.dto.*;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.repository.*;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.ExcelUtil;
import com.costcommission.util.FileUtil;
import com.costcommission.util.S3BucketUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.*;
import java.util.*;

@Service
public class ConsolidatedReportService {

    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private CostPlusRemunerationRepository costPlusRemunerationRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private EmailTemplateUtil emailTemplateUtil;
    @Autowired
    private AgencyVatCarriersRepository agencyVatCarriersRepository;

    public ResponseEntity<ByteArrayResource> export(String month, Integer year, String userId) throws Exception {
        SXSSFWorkbook workbook = null;
        InputStream inputStream = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/").concat("CONSOLIDATED REPORT/");
            String fileName = "Template accruals cost+ agency fees for " + month + " " + year.toString() + ".xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            int monthOrdinal = MonthEnum.valueOf(month).ordinal() + 1;
            String monthYear = String.valueOf(monthOrdinal).length() == 1 ? "0" + monthOrdinal + "" + year : monthOrdinal + "" + year;
            SXSSFSheet sheet = workbook.createSheet(monthYear);
            createConsolidatedReportSheet(sheet, workbook, month, year);
            workbook.write(arrayOutputStream);
            inputStream = new ByteArrayInputStream(arrayOutputStream.toByteArray());
            FileUtil.developmentUpload(arrayOutputStream,filePath,fileName);
            //S3BucketUtil.upload(fileName, inputStream, envConfiguration.getAwsBucketName(), filePath);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(arrayOutputStream).close();
            Objects.requireNonNull(inputStream).close();
            Objects.requireNonNull(workbook).close();
        }
    }

    private void createConsolidatedReportSheet(SXSSFSheet sheet, SXSSFWorkbook workbook, String month, Integer year) {
        List<Integer> costStatus = new ArrayList<>();
        costStatus.add(CostPlusStatus.CLOSED.ordinal());
        costStatus.add(CostPlusStatus.AR_INVOICE_REQUEST.ordinal());
        Map<Long, Agency> agencyDTOMap = new HashMap<>();
        Map<Long, CostPlusRemuneration> costPlusRemunerationMap = new HashMap<>();
        Set<Tuple> agencyIds = costPlusRemunerationRepository.findAgencyByMonthYear(month, year);
        for (Tuple tuple : agencyIds) {
            Agency agency = agencyRepository.findAgencyById(Long.valueOf(tuple.get(0).toString()));
            List<CostPlusRemuneration> costPlusRemunerationByAgency = costPlusRemunerationRepository.findAgencyIdMonthYear(Long.valueOf(tuple.get(0).toString()), month, year, costStatus);
            if (costPlusRemunerationByAgency.size() > 0) {
                agencyDTOMap.put(agency.getId(), agency);
                for (CostPlusRemuneration costPlusRemuneration : costPlusRemunerationByAgency) {
                    costPlusRemunerationMap.put(costPlusRemuneration.getCostCenterId(), costPlusRemuneration);
                }
            }
        }
        List<CostCenter> costCenters = costCenterRepository.findAllCostCenterByCarrierCodeNotEmpty();
        Map<String, List<Long>> carrierMap = new HashMap<>();
        String carrierCode = "";
        List<Long> costCentreIDs = null;
        for (CostCenter costCenter : costCenters) {
            if (!carrierCode.equalsIgnoreCase(costCenter.getCarrierCode())) {
                costCentreIDs = new ArrayList<>();
                costCentreIDs.add(costCenter.getId());
                carrierMap.put(costCenter.getCarrierCode(), costCentreIDs);
                carrierCode = costCenter.getCarrierCode();
            } else {
                assert costCentreIDs != null;
                costCentreIDs.add(costCenter.getId());
                if (carrierMap.containsKey(costCenter.getCarrierCode()))
                    carrierMap.replace(costCenter.getCarrierCode(), costCentreIDs);
            }
        }
        createHeaderRow(sheet, workbook, month, year, carrierMap);
        createDataRows(sheet, workbook, agencyDTOMap, costPlusRemunerationMap, carrierMap);
        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
    }

    private void createDataRows(SXSSFSheet sheet, SXSSFWorkbook workbook, Map<Long, Agency> agencyDTOMap, Map<Long, CostPlusRemuneration> costPlusRemunerationMap, Map<String, List<Long>> carrierMap) {
        sheet.trackAllColumnsForAutoSizing();
        CellStyle style = workbook.createCellStyle();
        int columnCount = 0, rowCount = sheet.getLastRowNum();
        if (agencyDTOMap.size() > 0) {
            for (Map.Entry<Long, Agency> agencyEntry : agencyDTOMap.entrySet()) {
                List<Long> mappedVatCarries = new ArrayList<>();
                List<AgencyVatCarriers> savedAgencyVatCarriers = agencyVatCarriersRepository.findByAgencyIdAndDeleted(agencyEntry.getKey(), false);
                if (savedAgencyVatCarriers.size() > 0) {
                    for (AgencyVatCarriers vatCarriers : savedAgencyVatCarriers) {
                        mappedVatCarries.add(vatCarriers.getCostCenterId());
                    }
                }
                rowCount = rowCount + 1;
                columnCount = 0;
                Row row = sheet.createRow(rowCount);
                excelUtil.createCell(row, 0, agencyEntry.getValue().getName(), sheet);
                sheet.autoSizeColumn(columnCount);
                excelUtil.createCell(row, 1, agencyEntry.getValue().getSafEntityNumber().toString(), sheet);
                excelUtil.createCell(row, 2, agencyEntry.getValue().getFcCcy(), sheet);
                columnCount = 3;
                for (Map.Entry<String, List<Long>> entry : carrierMap.entrySet()) {
                    String feeAmount = "0";
                    for (Long costCentreId : entry.getValue()) {
                        if (costPlusRemunerationMap.containsKey(costCentreId) && agencyEntry.getKey().equals(costPlusRemunerationMap.get(costCentreId).getAgencyId())) {
                            feeAmount = costPlusRemunerationMap.get(costCentreId).getAmount().toString();
                            ExcelUtil.setCellValueAsNumeric(row, style, columnCount++, feeAmount, workbook.createDataFormat());
                        }
                    }
                    if (feeAmount.equals("0") || entry.getValue().size() == 0) {
                        ExcelUtil.setCellValueAsNumeric(row, style, columnCount++, feeAmount, workbook.createDataFormat());
                    }

                }
                if (agencyEntry.getValue().isVatGstFlag()) {
                    rowCount = rowCount + 1;
                    columnCount = 0;
                    Row vatRow = sheet.createRow(rowCount);
                    excelUtil.createCell(vatRow, 0, agencyEntry.getValue().getName() + "(VAT- "+agencyEntry.getValue().getVatPercentage()+"%)", sheet);
                    sheet.autoSizeColumn(columnCount);
                    excelUtil.createCell(vatRow, 1, agencyEntry.getValue().getSafEntityNumber().toString(), sheet);
                    excelUtil.createCell(vatRow, 2, agencyEntry.getValue().getFcCcy(), sheet);
                    columnCount = 3;
                    for (Map.Entry<String, List<Long>> entry : carrierMap.entrySet()) {
                        String vatGstAmount = "0";
                        for (Long costCentreId : entry.getValue()) {
                            if(agencyEntry.getValue().isVatAllCarrier() || mappedVatCarries.contains(costCentreId)) {
                                if (costPlusRemunerationMap.containsKey(costCentreId) && agencyEntry.getKey().equals(costPlusRemunerationMap.get(costCentreId).getAgencyId())) {
                                    double vatAmount = (agencyEntry.getValue().getVatPercentage() / 100.0f) * costPlusRemunerationMap.get(costCentreId).getAmount();
                                    vatGstAmount = String.valueOf(vatAmount);
                                    ExcelUtil.setCellValueAsNumeric(vatRow, style, columnCount++, vatGstAmount, workbook.createDataFormat());
                                }
                            }
                        }
                        if (vatGstAmount.equals("0")) {
                            ExcelUtil.setCellValueAsNumeric(vatRow, style, columnCount++, vatGstAmount, workbook.createDataFormat());
                        }
                    }
                }
            }
        } else {
            rowCount = rowCount + 1;
            Row row = sheet.createRow(rowCount);
            excelUtil.createCell(row, 0, "No agency template is approved for selected month and year.", sheet);
        }
    }

    private void createHeaderRow(SXSSFSheet sheet, SXSSFWorkbook workbook, String month, Integer year, Map<String, List<Long>> carrierMap) {
        CellStyle style = workbook.createCellStyle();
        String[] header = {"Entity Name", "SAFRAN Entity Number", "Currency"};
        int rowHeaderCount = 0;
        Row headingRowIndex = sheet.createRow(rowHeaderCount);
        excelUtil.createHeaderCellBlueFontColor(headingRowIndex, 0, month, sheet, workbook);
        excelUtil.createHeaderCellBlueFontColor(headingRowIndex, 1, year.toString(), sheet, workbook);
        excelUtil.createHeaderCell(headingRowIndex, 3, "ACCRUED COST+ REMUNERATION", sheet, workbook);
        sheet.trackAllColumnsForAutoSizing();
        rowHeaderCount++;
        Row rowIndex = sheet.createRow(rowHeaderCount);
        int commentCol = header.length + carrierMap.size();
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
            sheet.autoSizeColumn(i);
        }
        int i = header.length;
        for (Map.Entry<String, List<Long>> entry : carrierMap.entrySet()) {
            excelUtil.createHeaderCell(rowIndex, i, "AGENCY FEE " + entry.getKey() + " in agent currency ", sheet, workbook);
            sheet.autoSizeColumn(i);
            i++;
        }
        excelUtil.createHeaderCell(rowIndex, commentCol, "Comments", sheet, workbook);
        for (int j = 0; j < header.length; j++) {
            sheet.autoSizeColumn(j);
        }
        sheet.autoSizeColumn(commentCol);
        int carrierLength = carrierMap.size() + header.length;
        Cell cell = rowIndex.getCell(carrierLength - 1);
        String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
        sheet.addMergedRegion(CellRangeAddress.valueOf("D1:" + columnLetter + "1"));
    }

    public void sendConsolidatedReportMail(UserCurrentSessionDTO userCurrentSessionDTO) throws Exception {
        String fileName = "Template accruals cost+ agency fees for " + userCurrentSessionDTO.getMonth() + " " + userCurrentSessionDTO.getYear()+ ".xlsx";
        String s3FilePath = envConfiguration.getAwsBucketPath() + "/Reports/CONSOLIDATED REPORT/";
        String tempPath = envConfiguration.getEmailFileTempPath() + "/CONSOLIDATED REPORT/";
        try {
            String subject = "Monthly Consolidated details for " + userCurrentSessionDTO.getMonth() + " " + userCurrentSessionDTO.getYear();
            List<Integer> costStatus = new ArrayList<>();
            costStatus.add(CostPlusStatus.CLOSED.ordinal());
            costStatus.add(CostPlusStatus.AR_INVOICE_REQUEST.ordinal());
            Set<Long> set = new HashSet<>();
            Set<Tuple> agencyIds = costPlusRemunerationRepository.findAgencyByMonthYear(userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
            for (Tuple tuple : agencyIds) {
                List<CostPlusRemuneration> costPlusRemunerationByAgency = costPlusRemunerationRepository.findAgencyIdMonthYear(Long.valueOf(tuple.get(0).toString()), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear(), costStatus);
                if (costPlusRemunerationByAgency.size() > 0) {
                    set.add(Long.valueOf(tuple.get(0).toString()));
                }
            }
            List<Long> agencyIdList = new ArrayList<>(set);
            if (agencyIdList.size() > 0) {
                List<Tuple> managerEmailIds = agencyRepository.findMangerEmailIds(agencyIdList);
                List<String> emailIdList = new ArrayList<>();
                for (Tuple tuple : managerEmailIds) {
                    emailIdList.addAll(Arrays.asList(mailService.getRecipientEmailIds(tuple)));
                }
                String[] toList = emailIdList.stream().toArray(String[]::new);

                FileUtil.saveEmailTempForDevelopment(tempPath,fileName,s3FilePath,userCurrentSessionDTO.getUserId(),"Consolidated Report is not generated");
                //mailService.saveS3FileInTempPath(fileName, s3FilePath, tempPath, userCurrentSessionDTO.getUserId(),"Consolidated Report is not generated.");

                Map<String, Object> itemdata = new HashMap<>();
                itemdata.put("mangerName", "All");
                String process = emailTemplateUtil.getEmailTemplateData("manager-template", itemdata);
                mailService.sendMail(toList, null,subject, process, Collections.singletonList(userCurrentSessionDTO.getUserId()+"#"+fileName), "consolidated");
            } else {
                throw new CostPlusException("No agency template is approved for selected month and year. So, Mail notification is not allowed.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            FileUtil.delete(tempPath, userCurrentSessionDTO.getUserId()+"#"+fileName);
        }
    }


}
