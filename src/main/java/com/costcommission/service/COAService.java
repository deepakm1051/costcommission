package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.CoaDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.entity.COA;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.entity.TBVolumeTransaction;
import com.costcommission.entity.User;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.COASpecification;
import com.costcommission.repository.COARepository;
import com.costcommission.repository.TBVolumeTransactionRepository;
import com.costcommission.repository.UserRepository;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.*;

@Service
public class COAService {

    @Autowired
    private COARepository coaRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private TBVolumeTransactionRepository tBvolumeTransactionRepository;
    @Autowired
    private CostPlusTemplateService costPlusTemplateService;

    public List<CoaDTO> allActive() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Type listType = new TypeToken<List<CoaDTO>>() {
        }.getType();
        return modelMapper.map(coaRepository.findAll(), listType);
    }

    public CoaDTO create(CoaDTO coaDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Long count = coaRepository.count(coaDTO.getNatureCode().trim().toLowerCase());
        if (count > 0) {
            throw new CostPlusException("Chart Of Accounts already exists");
        } else {
            COA coa = modelMapper.map(coaDTO, COA.class);
            return modelMapper.map(coaRepository.save(coa), CoaDTO.class);
        }
    }

    public CoaDTO update(CoaDTO coaDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        COA savedCoa = coaRepository.findByNatureCode(coaDTO.getNatureCode());
        if (savedCoa != null && !savedCoa.getId().equals(coaDTO.getId())) {
            throw new CostPlusException("Chart Of Accounts already exists");
        } else {
            COA coa = modelMapper.map(coaDTO, COA.class);
            return modelMapper.map(coaRepository.save(coa), CoaDTO.class);
        }
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {
            if (!status) {
                List<CostPlusTemplateProcess> templateProcesses = costPlusTemplateService.getActiveCostPlusTemplate();
                if (templateProcesses.size() > 0) {
                    throw new CostPlusException("Cost+ remuneration template is in generated/approval stage. COA inactivate is not allowed.");
                }
            }
            return coaRepository.enableOrDisable(status, userId, id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void upload(MultipartFile file, UserCurrentSessionDTO currentSessionDTO) throws Exception {
        try {
            List<COA> coas = getCOA(file);
            List<COA> finalCoas = new ArrayList<>();
            Map<String, COA> existingCoaMap = new HashMap<>();
            List<COA> existingCOA = coaRepository.findAll();
            User user = userRepository.findUserById(currentSessionDTO.getUserId());
            for (COA coa : existingCOA) {
                existingCoaMap.put(coa.getNatureCode(), coa);
            }
            for (COA coa : coas) {
                coa.setUpdatedBy(user.getId());
                if (existingCoaMap.containsKey(coa.getNatureCode())) {
                    coa.setId(existingCoaMap.get(coa.getNatureCode()).getId());
                }
                coa.setActive(true);
                finalCoas.add(coa);
            }
            coaRepository.saveAll(finalCoas);

            TBVolumeTransaction tbVolumeTransaction = new TBVolumeTransaction();
            tbVolumeTransaction.setFileType("COA");
            tbVolumeTransaction.setType("COA");
            tbVolumeTransaction.setFileName(file.getOriginalFilename());
            tbVolumeTransaction.setDisplayFileName(file.getOriginalFilename());
            tbVolumeTransaction.setCreatedBy(currentSessionDTO.getUserId());
            tbVolumeTransaction.setUser(user);
            tBvolumeTransactionRepository.save(tbVolumeTransaction);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    public List<COA> getCOA(MultipartFile file) throws Exception {
        List<COA> coas = new ArrayList<>();
        try {
            InputStream coaFile = file.getInputStream();
            XSSFWorkbook workbookinput = new XSSFWorkbook(coaFile);
            String name = workbookinput.getSheetName(0);
            Sheet sheet = workbookinput.getSheet(name);
            Row row = sheet.getRow(1);
            if (row != null) {
                Integer lastRowIndex = sheet.getLastRowNum();
                for (int i = 1; i <= lastRowIndex; i++) {
                    Row rowItr = sheet.getRow(i);
                    if (rowItr != null) {
                        Integer totalNoOfCols = rowItr.getLastCellNum() - 0;
                        COA coa = new COA();
                        for (int j = 0; j < totalNoOfCols; j++) {
                            switch (j) {
                                case 0:
                                    String natureCode = getCellValue(rowItr, j);
                                    if (natureCode == null || natureCode.trim().equals("")) {
                                        throw new Exception("Nature code should not be empty");
                                    }
                                    coa.setNatureCode(natureCode);
                                    break;
                                case 1:
                                    coa.setNatureDesc(getCellValue(rowItr, j));
                                    break;
                                case 2:
                                    String safrenPL = getCellValue(rowItr, j);
                                    if (safrenPL == null || safrenPL.trim().equals("")) {
                                        throw new Exception("SAFRAN P&L should not be empty");
                                    }
                                    coa.setSafranpl(safrenPL);
                                    break;
                                case 3:
                                    coa.setDescriptionEnglish(getCellValue(rowItr, j));
                                    break;
                                case 4:
                                    coa.setSafranplDetails(getCellValue(rowItr, j));
                                    break;
                                case 5:
                                    coa.setDescription(getCellValue(rowItr, j));
                                    break;
                                case 6:
                                    coa.setAgencyBudgetCode(getCellValue(rowItr, j));
                                    break;
                                case 7:
                                    coa.setAgencyBudgetLine(getCellValue(rowItr, j));
                                    break;
                                case 8:
                                    coa.setMandatoryAllocPerCC(getCellValue(rowItr, j));
                                    break;
                                case 9:
                                    coa.setUnallocatedExpenses(getCellValue(rowItr, j));
                                    break;
                                case 10:
                                    coa.setCostCenterAllocation(getCellValue(rowItr, j));
                                    break;
                                case 11:
                                    coa.setNatureNotRelevant(getCellValue(rowItr, j));
                                    break;
                                case 12:
                                    coa.setRationale(getCellValue(rowItr, j));
                                    break;
                                case 13:
                                    String costPlusMatrix = getCellValue(rowItr, j);
                                    if (costPlusMatrix == null || costPlusMatrix.trim().equals("")) {
                                        throw new Exception("Cost plus Matrix should not be empty");
                                    }
                                    coa.setCostPlusMatrix(costPlusMatrix);
                                    break;
                                case 14:
                                    boolean isActive = false;
                                    String coaStatus = getCellValue(rowItr, j);
                                    if (coaStatus.equalsIgnoreCase("yes"))
                                        isActive = true;
                                    coa.setActive(isActive);
                                    break;
                            }
                        }
                        coas.add(coa);
                    } else {
                        throw new Exception("Some Rows are empty. Please Check and upload again");
                    }
                }
            } else {
                throw new Exception("Invalid File! Download the template provided, fill it and upload.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return coas;
    }

    public String getCellValue(Row row, int column) {
        Cell cell = row.getCell(column);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        COASpecification spec = new COASpecification(specificationDTO);
        Page<COA> coa = coaRepository.findAll(spec, pageable);
        Type listType = new TypeToken<List<CoaDTO>>() {
        }.getType();

        paginationResponseDTO.setTotalSize(coa.getTotalElements());
        paginationResponseDTO.setData(modelMapper.map(coa.getContent(), listType));
        return paginationResponseDTO;
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "ChartOfAccountList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createCOASheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createCOASheet(SXSSFSheet sheet, SXSSFWorkbook workbook) throws Exception {
        try {
            int rowCount = 0;
            String[] header = {"Sl No", "Nature Code", "Nature Description", "Description", "SAFRAN P&L", "SAFRAN P&L Detail", "Agency Budget Code", "Agency Budget Line", "Mandatory allocation per cost center"
                    , "Unallocated Expenses :Mandatory for C-CGEN Cost center", "Cost Center Allocation if feasible & meaningful otherwise C-CGEN", "Nature not relevant for cost center", "Rationale / Comment"
                    , "Cost plus Matrix", "Description English", "Chart Of Accounts Status"};
            Row rowIndex = sheet.createRow(rowCount);
            for (int i = 0; i < header.length; i++) {
                excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
            }
            List<CoaDTO> coaDTOS = allActive();
            for (CoaDTO coaDTO : coaDTOS) {
                rowCount = rowCount + 1;
                Row row = sheet.createRow(rowCount);
                excelUtil.createSlNoCell(row, 0, rowCount, sheet);
                excelUtil.createCell(row, 1, coaDTO.getNatureCode(), sheet);
                excelUtil.createCell(row, 2, coaDTO.getNatureDesc(), sheet);
                excelUtil.createCell(row, 3, coaDTO.getDescription(), sheet);
                excelUtil.createCell(row, 4, coaDTO.getSafranpl(), sheet);
                excelUtil.createCell(row, 5, coaDTO.getSafranplDetails(), sheet);
                excelUtil.createCell(row, 6, coaDTO.getAgencyBudgetCode(), sheet);
                excelUtil.createCell(row, 7, coaDTO.getAgencyBudgetLine(), sheet);
                excelUtil.createCell(row, 8, coaDTO.getMandatoryAllocPerCC(), sheet);
                excelUtil.createCell(row, 9, coaDTO.getUnallocatedExpenses(), sheet);
                excelUtil.createCell(row, 10, coaDTO.getCostCenterAllocation(), sheet);
                excelUtil.createCell(row, 11, coaDTO.getNatureNotRelevant(), sheet);
                excelUtil.createCell(row, 12, coaDTO.getRationale(), sheet);
                excelUtil.createCell(row, 13, coaDTO.getCostPlusMatrix(), sheet);
                excelUtil.createCell(row, 14, coaDTO.getDescriptionEnglish(), sheet);
                excelUtil.createCell(row, 15, coaDTO.isActive() ? "Active" : "In-Active", sheet);
            }
            sheet.setDefaultColumnWidth(20);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}
