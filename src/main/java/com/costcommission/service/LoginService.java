package com.costcommission.service;

import com.costcommission.dto.RoleAgencyDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.dto.UserDTO;
import com.costcommission.entity.*;
import com.costcommission.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCurrentSessionRepository userCurrentSessionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleAgencyRepository userRoleAgencyRepository;

    @Autowired
    private AgencyRepository agencyRepository;

    public UserDTO validateUser(String loginId, String password) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        User user =null;
        if(password==null || password.isEmpty()){
            user = userRepository.findUserByLoginIdIgnoreCase(loginId);
        }else {
             user = userRepository.findUserByLoginIdAndPassword(loginId, password);
        }
        if (user == null)
            return null;
        else if(!user.isActive()){
            return modelMapper.map(user, UserDTO.class);
        } else{
            List<RoleAgencyDTO> roleAgencyDTOS = new ArrayList<>();
            List<UserRole> userRoles = userRoleRepository.findByUserId(user.getId());
            for (UserRole userRole : userRoles) {
                Role role = roleRepository.findRoleById(userRole.getRoleId());
                List<UserRoleAgency> userRoleAgencies = userRoleAgencyRepository.findByUserRoleId(userRole.getId());
                if(userRoleAgencies.size()>0) {
                    for (UserRoleAgency userRoleAgency : userRoleAgencies) {
                        Agency agency = agencyRepository.findAgencyById(userRoleAgency.getAgencyId());
                        RoleAgencyDTO roleAgencyDTO = new RoleAgencyDTO();
                        roleAgencyDTO.setRoleId(role.getId());
                        roleAgencyDTO.setRoleName(role.getName());
                        roleAgencyDTO.setAgencyId(agency.getId());
                        roleAgencyDTO.setAgencyName(agency.getName());
                        roleAgencyDTO.setUserRoleId(userRole.getId());
                        roleAgencyDTO.setUserRoleAgencyId(userRoleAgency.getId());
                        roleAgencyDTOS.add(roleAgencyDTO);
                    }
                }
            }
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            UserCurrentSession userCurrentSession = userCurrentSessionRepository.findByUserId(user.getId());
            if (userCurrentSession != null) {
                userDTO.setUserAgency(userCurrentSession.getAgencyId());
                userDTO.setUserMonth(userCurrentSession.getMonth());
                userDTO.setUserYear(userCurrentSession.getYear());
                userDTO.setUserCurrentSessionId(userCurrentSession.getId());
            }
            userDTO.setRoleAgencies(roleAgencyDTOS);
            return userDTO;
        }
    }

    public UserCurrentSessionDTO createOrUpdate(UserCurrentSessionDTO currentSessionDTO) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        UserCurrentSessionDTO userCurrentSessionDTO = null;
        try {
            UserCurrentSession currentSession = modelMapper.map(currentSessionDTO, UserCurrentSession.class);
            userCurrentSessionDTO = modelMapper.map(userCurrentSessionRepository.save(currentSession), UserCurrentSessionDTO.class);
            return userCurrentSessionDTO;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
