package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.PeriodControlDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.PeriodControl;
import com.costcommission.jpaspecification.PeriodControlSpecification;
import com.costcommission.repository.AgencyRepository;
import com.costcommission.repository.PeriodControlRepository;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PeriodControlService {

    @Autowired
    private PeriodControlRepository periodControlRepository;

    @Autowired
    private AgencyRepository agencyRepository;

    @Autowired
    private ExcelUtil excelUtil;

    @Autowired
    private EnvConfiguration envConfiguration;

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        PeriodControlSpecification spec = new PeriodControlSpecification(specificationDTO);
        Page<PeriodControl> results = periodControlRepository.findAll(spec, pageable);
        List<PeriodControlDTO> periodControlDTOS = new ArrayList<>();
        for (PeriodControl periodControl : results) {
            PeriodControlDTO periodControlDTO = modelMapper.map(periodControl, PeriodControlDTO.class);
            Agency agency = agencyRepository.findAgencyById(periodControlDTO.getAgencyId());
            periodControlDTO.setAgencyId(agency.getId());
            periodControlDTO.setAgencyName(agency.getName());
            periodControlDTOS.add(periodControlDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(periodControlDTOS);
        return paginationResponseDTO;
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "PeriodControlList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createPeriodControlSheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createPeriodControlSheet(SXSSFSheet sheet, SXSSFWorkbook workbook) {
        int rowCount = 0;
        String[] header = {"Sl No", "Agency", "Month", "Year", "Open/Close"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<PeriodControl> periodControls = periodControlRepository.findAll();
        for (PeriodControl periodControl : periodControls) {
            rowCount = rowCount + 1;
            columnCount = 0;
            Row row = sheet.createRow(rowCount);
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, periodControl.getAgency().getName(), sheet);
            excelUtil.createCell(row, ++columnCount, periodControl.getMonth(), sheet);
            excelUtil.createCell(row, ++columnCount, String.valueOf(periodControl.getYear()), sheet);
            excelUtil.createCell(row, ++columnCount, periodControl.isStatus() ? "Open" : "Close", sheet);
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}
