package com.costcommission.service;

import com.costcommission.costenum.MonthEnum;
import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.CostPlusRemunerationDTO;
import com.costcommission.dto.TrialBalanceR11DTO;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.repository.*;
import com.costcommission.util.ExcelUtil;
import com.costcommission.util.FileUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CostPlusTemplateR11Service {

    @Autowired
    private CostCenterService costCenterService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private COARepository coaRepository;
    @Autowired
    private VolumeReportRepository volumeReportRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostPlusRemunerationRepository costPlusRemunerationRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private ADIJournalRepository adiJournalRepository;
    @Autowired
    private AgencyVatCarriersRepository agencyVatCarriersRepository;
    @Autowired
    private TBVolumeTransactionRepository tbVolumeTransactionRepository;

    public void generate(CostPlusTemplateProcess templateProcess, Agency agency, User user) throws Exception {
        XSSFWorkbook workbook = null;
        InputStream file = null, templateInputStream = null;
        ByteArrayOutputStream baos = null;
        try {
            String templateFilePath = envConfiguration.getAwsBucketPath().concat("/Templates/");
            /*boolean isExist = S3BucketUtil.fileExist("CostPlusStandardWithOutHeadCountR11.xlsx", envConfiguration.getAwsBucketName(), templateFilePath);
            if (isExist) {
                file = S3BucketUtil.getFileInputStream("CostPlusStandardWithOutHeadCountR11.xlsx", envConfiguration.getAwsBucketName(), templateFilePath);
            } else {
                throw new CostPlusException("Cost+ template file does not exist");
            }*/
            file = new FileInputStream(templateFilePath + "CostPlusStandardWithOutHeadCountR11.xlsx");
            String fileName = agency.getCode() +
                    "-Template " +
                    agency.getOceanType() +
                    " Cost+ remuneration calculation " +
                    "(FY - " + templateProcess.getYear() + ") " + templateProcess.getMonth() + " - " + templateProcess.getYear() + ".xlsx";
            workbook = new XSSFWorkbook(file);
            Map<Long, String> costCenterRemunerationMap = new HashMap<>();
            Map<String, List<String>> subCategoryCostCenterColumnMap = new HashMap<>();
            Map<Integer, Long> columnCostCenterIdMap = new HashMap<>();
            Map<Long, CostPlusRemunerationDTO> finalAmountMap = new HashMap<>();
            List<String> totalLegalColAndAccSchSum = new ArrayList<>();
            List<Long> mappedVatCarries = new ArrayList<>();

            updatePeriodInValidationSheet(workbook, templateProcess);
            generateMapping(workbook);
            generatePAndLSheetAndTBSheet(workbook, templateProcess, agency, subCategoryCostCenterColumnMap, columnCostCenterIdMap, totalLegalColAndAccSchSum);
            generateAccountingScheme(workbook, templateProcess, agency, costCenterRemunerationMap, subCategoryCostCenterColumnMap, columnCostCenterIdMap, finalAmountMap, user, totalLegalColAndAccSchSum, mappedVatCarries);
            createCoherenceChecking(workbook, totalLegalColAndAccSchSum, finalAmountMap);
            generateADIJournal(agency, templateProcess, finalAmountMap, user, mappedVatCarries);
            updateCostPlusFileName(templateProcess.getId(), fileName);
            String costPlusPath = envConfiguration.getAwsBucketPath().concat("/Reports/COSTPLUS/");
            baos = new ByteArrayOutputStream();
            workbook.write(baos);
            templateInputStream = new ByteArrayInputStream(baos.toByteArray());
            FileUtil.developmentUpload(baos, costPlusPath, fileName);
            //S3BucketUtil.upload(fileName, templateInputStream, envConfiguration.getAwsBucketName(), costPlusPath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            if (file != null) {
                file.close();
            }
            if (baos != null) {
                baos.close();
            }
            if (templateInputStream != null) {
                templateInputStream.close();
            }
        }
    }

    private void updateCostPlusFileName(Long id, String fileName) {
        costPlusTemplateRepository.updateCostPlusFileName(fileName, id);
    }

    private void updatePeriodInValidationSheet(XSSFWorkbook workbook, CostPlusTemplateProcess templateProcess) {
        String validationSheetName = workbook.getSheetName(0);
        Sheet validationSheet = workbook.getSheet(validationSheetName);
        Cell cell = validationSheet.getRow(15).getCell(5);
        String monthYear = templateProcess.getMonth() + " - " + templateProcess.getYear();
        cell.setCellValue(monthYear);
    }

    public void generateMapping(XSSFWorkbook workbook) throws Exception {
        try {
            String mappingSheetName = workbook.getSheetName(4);
            CellStyle style = workbook.createCellStyle();
            Sheet mappingSheet = workbook.getSheet(mappingSheetName);
            List<COA> coas = coaRepository.findAllByActive(true);
            if (coas.size() == 0)
                throw new CostPlusException("Chart of accounts are not updated. Please contact admin.");
            createMappingSheet(coas, mappingSheet, style);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void createMappingSheet(List<COA> coas, Sheet sheet, CellStyle style) throws Exception {
        try {
            for (int startRow = 1, i = 0; i < coas.size(); startRow++, i++) {
                Row row = sheet.createRow(startRow);
                ExcelUtil.setExcelCellValue(row, 0, coas.get(i).getNatureCode());
                ExcelUtil.setExcelCellValue(row, 1, coas.get(i).getNatureDesc());
                ExcelUtil.setExcelCellValue(row, 2, coas.get(i).getSafranpl());
                ExcelUtil.setExcelCellValue(row, 3, coas.get(i).getDescriptionEnglish());
                ExcelUtil.setExcelCellValue(row, 4, coas.get(i).getSafranplDetails());
                ExcelUtil.setExcelCellValue(row, 5, coas.get(i).getDescription());
                ExcelUtil.setExcelCellValue(row, 6, coas.get(i).getAgencyBudgetCode());
                ExcelUtil.setExcelCellValue(row, 7, coas.get(i).getAgencyBudgetLine());
                ExcelUtil.setExcelCellValue(row, 8, coas.get(i).getMandatoryAllocPerCC());
                ExcelUtil.setExcelCellValue(row, 9, coas.get(i).getUnallocatedExpenses());
                ExcelUtil.setExcelCellValue(row, 10, coas.get(i).getCostCenterAllocation());
                ExcelUtil.setExcelCellValue(row, 11, coas.get(i).getNatureNotRelevant());
                ExcelUtil.setExcelCellValue(row, 12, coas.get(i).getRationale());
                ExcelUtil.setExcelCellValue(row, 13, coas.get(i).getCostPlusMatrix());
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void generatePAndLSheetAndTBSheet(XSSFWorkbook workbook, CostPlusTemplateProcess templateProcess, Agency agency, Map<String, List<String>> subCategoryCostCenterColumnMap, Map<Integer, Long> columnCostCenterIdMap, List<String> totalLegalCol) throws Exception {
        String name = workbook.getSheetName(1);
        Sheet pAndLSheet = workbook.getSheet(name);
        Map<String, String> costCenterColumnMap = new HashMap<>();
        List<Category> categories = categoryRepository.findAll();
        CellStyle style = workbook.createCellStyle();
        CellStyle redStyle = workbook.createCellStyle();
        CellStyle boldStyle = workbook.createCellStyle();
        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont boldFont = workbook.createFont();
        int categoryStartRowIndex = 3;
        int subcategoryStartRowIndex = 5;
        int costCenterNameStartRowIndex = 6;
        int costCenterCodeStartRowIndex = 7;
        int categoryStartColumnIndex = 3;
        int subcategoryStartColumnIndex = 3;
        int costCenterStartColumnIndex = 3;
        String subTotalColLetter = "";
        List<String> costScopeCostCenterColLetter = new ArrayList<>();
        for (Category category : categories) {
            int subCatCount = 0;
            for (SubCategory subCategory : category.getSubCategories()) {
                int costCenterCount = 0;
                List<String> costCenterColumnLetter = new ArrayList<>();
                for (CostCenter costCenter : subCategory.getCostCenters()) {
                    if (costCenter.getType().equalsIgnoreCase("R11") && costCenter.isActive()) {
                        Row costCenterNameRow = pAndLSheet.getRow(costCenterNameStartRowIndex);
                        ExcelUtil.setCellValueAndWrap(costCenterNameRow, style, costCenterStartColumnIndex, costCenter.getName());

                        Row costCenterCodeRow = pAndLSheet.getRow(costCenterCodeStartRowIndex);
                        ExcelUtil.setCellValueWrapBold(costCenterCodeRow, boldStyle, boldFont, costCenterStartColumnIndex, costCenter.getCode());
                        String columnLetter = ExcelUtil.getColumnLetterByColIndex(costCenterStartColumnIndex, costCenterCodeRow);
                        costCenterColumnLetter.add(columnLetter);
                        costCenterColumnMap.put(costCenter.getCode(), columnLetter);
                        columnCostCenterIdMap.put(costCenterStartColumnIndex, costCenter.getId());
                        if (category.getName().startsWith("Cost"))
                            costScopeCostCenterColLetter.add(columnLetter);

                        applyFormulaForCostCenterAccounting(pAndLSheet, style, costCenterStartColumnIndex, columnLetter, workbook, category.getName());

                        costCenterStartColumnIndex = costCenterStartColumnIndex + 1;
                        costCenterCount = costCenterCount + 1;
                        subCatCount = subCatCount + 1;
                    }
                }
                Row subCategoryRow = pAndLSheet.getRow(subcategoryStartRowIndex);
                if (category.getName().startsWith("Cost"))
                    ExcelUtil.setCellValueWrapFillBlueColor(subCategoryRow, headerStyle, boldFont, subcategoryStartColumnIndex, subCategory.getName());
                else
                    ExcelUtil.setCellValueWrapFillGrayColor(subCategoryRow, headerStyle, boldFont, subcategoryStartColumnIndex, subCategory.getName());
                subCategoryCostCenterColumnMap.put(subCategory.getName(), costCenterColumnLetter);
                int startCol = subcategoryStartColumnIndex;
                int endCol = startCol + (costCenterCount != 0 ? (costCenterCount - 1) : costCenterCount);
                ExcelUtil.border(subcategoryStartRowIndex, subcategoryStartRowIndex, startCol, endCol, pAndLSheet);
                String startColLetter = ExcelUtil.getColumnLetterByColIndex(startCol, subCategoryRow);
                String endColLetter = ExcelUtil.getColumnLetterByColIndex(endCol, subCategoryRow);
                int subCatRow = subcategoryStartRowIndex + 1;
                if (!startColLetter.equals(endColLetter))
                    pAndLSheet.addMergedRegion(CellRangeAddress.valueOf("" + startColLetter + subCatRow + ":" + endColLetter + subCatRow));
                if (costCenterCount == 1)
                    pAndLSheet.autoSizeColumn(subcategoryStartColumnIndex);

                subcategoryStartColumnIndex = costCenterStartColumnIndex;

            }

            if (category.getName().startsWith("Cost")) {
                costCenterStartColumnIndex = costCenterStartColumnIndex + 1;
                Row subCategoryRow = pAndLSheet.getRow(subcategoryStartRowIndex);
                ExcelUtil.setCellValueWrapFillBlueColor(subCategoryRow, headerStyle, boldFont, costCenterStartColumnIndex, "SUB-TOTAL AGENCY ACTIVITY");
                pAndLSheet.autoSizeColumn(costCenterStartColumnIndex);
                ExcelUtil.border(subcategoryStartRowIndex, subcategoryStartRowIndex, costCenterStartColumnIndex, costCenterStartColumnIndex, pAndLSheet);
                Row subCategoryCodeRow = pAndLSheet.getRow(costCenterCodeStartRowIndex);
                ExcelUtil.setExcelCellValue(subCategoryCodeRow, costCenterStartColumnIndex, "FY");
                applyFormulaForSubTotalAgency(pAndLSheet, costCenterStartColumnIndex, costScopeCostCenterColLetter, style, workbook);
                subTotalColLetter = ExcelUtil.getColumnLetterByColIndex(costCenterStartColumnIndex, subCategoryRow);
                costCenterStartColumnIndex = costCenterStartColumnIndex + 1;
            }

            Row categoryRow = pAndLSheet.getRow(categoryStartRowIndex);
            int startCol = categoryStartColumnIndex;
            int endCol = startCol + (subCatCount != 0 ? (subCatCount - 1) : subCatCount);
            if (category.getName().startsWith("Cost"))
                ExcelUtil.setCellValueWrapFillBlueColor(categoryRow, headerStyle, boldFont, categoryStartColumnIndex, category.getName());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(categoryRow, headerStyle, boldFont, categoryStartColumnIndex, category.getName());

            ExcelUtil.border(categoryStartRowIndex, categoryStartRowIndex, startCol, endCol, pAndLSheet);
            String startColLetter = ExcelUtil.getColumnLetterByColIndex(startCol, categoryRow);
            String endColLetter = ExcelUtil.getColumnLetterByColIndex(endCol, categoryRow);
            int catRow = categoryStartRowIndex + 1;
            if (!startColLetter.equals(endColLetter))
                pAndLSheet.addMergedRegion(CellRangeAddress.valueOf("" + startColLetter + catRow + ":" + endColLetter + catRow));

            categoryStartColumnIndex = subcategoryStartColumnIndex = costCenterStartColumnIndex = costCenterStartColumnIndex + 1;
        }

        Row subCategoryRow = pAndLSheet.getRow(subcategoryStartRowIndex);
        ExcelUtil.setCellValueWrapFillBlueColor(subCategoryRow, headerStyle, boldFont, costCenterStartColumnIndex, "TOTAL LEGAL ENTITY");
        pAndLSheet.autoSizeColumn(costCenterStartColumnIndex);
        ExcelUtil.border(subcategoryStartRowIndex, subcategoryStartRowIndex, costCenterStartColumnIndex, costCenterStartColumnIndex, pAndLSheet);
        totalLegalCol.add(ExcelUtil.getColumnLetterByColIndex(costCenterStartColumnIndex, subCategoryRow));

        String lastCostCenterColLetter = ExcelUtil.getColumnLetterByColIndex(costCenterStartColumnIndex - 2, subCategoryRow);
        applyFormulaForTotalLegalEntity(pAndLSheet, costCenterStartColumnIndex, subTotalColLetter, lastCostCenterColLetter, style, workbook);
        totalLegalCol.add(String.valueOf(costCenterStartColumnIndex));

        generateTB(workbook, templateProcess, lastCostCenterColLetter);
        generateAllocationKeys(workbook, templateProcess, redStyle, subCategoryCostCenterColumnMap, columnCostCenterIdMap, costCenterColumnMap, costScopeCostCenterColLetter, subTotalColLetter);
        applyFormulaForAllocationOfCostPerCarrier(pAndLSheet, style, workbook, subCategoryCostCenterColumnMap, costCenterColumnMap, costScopeCostCenterColLetter, subTotalColLetter);
        applyFormulaForRemunerationCalculation(workbook, pAndLSheet, style, workbook, subCategoryCostCenterColumnMap, costCenterColumnMap, agency, subTotalColLetter, costScopeCostCenterColLetter);
        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();

    }

    public void generateTB(XSSFWorkbook workbook, CostPlusTemplateProcess templateProcess, String lastCostCenterColLetter) throws Exception {
        String tbSheetName = workbook.getSheetName(2);
        CellStyle style = workbook.createCellStyle();
        Sheet tbSheet = workbook.getSheet(tbSheetName);
        List<TrialBalanceR11DTO> trialBalanceR11DTOS = getTbSheet(templateProcess);
        createTbSheet(trialBalanceR11DTOS, tbSheet, style, workbook, lastCostCenterColLetter);
    }

    public List<TrialBalanceR11DTO> getTbSheet(CostPlusTemplateProcess templateProcess) throws Exception {
        InputStream r11File = null;
        List<TrialBalanceR11DTO> trialBalanceR11DTOS = new ArrayList<>();
        TBVolumeTransaction tbVolumeTransaction = tbVolumeTransactionRepository.findByAgencyIdAndMonthAndYearAndDeletedAndFileType(templateProcess.getAgency().getId(), templateProcess.getMonth(), templateProcess.getYear(), false, "TRIALBALANCE");
        try {
            String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/TRIALBALANCE/");
            /*boolean isExist = S3BucketUtil.fileExist(tbVolumeTransaction.getFileName(), envConfiguration.getAwsBucketName(), filePath);
            if (isExist) {
                r11File = S3BucketUtil.getFileInputStream(tbVolumeTransaction.getFileName(), envConfiguration.getAwsBucketName(), filePath);
            } else {
                throw new CostPlusException("Trial balance file does not exist");
            }*/
            r11File = new FileInputStream(filePath + tbVolumeTransaction.getFileName());
            XSSFWorkbook workbook = new XSSFWorkbook(r11File);
            String name = workbook.getSheetName(0);
            Sheet sheet = workbook.getSheet(name);


            int lastRowIndex = sheet.getLastRowNum();
            for (int i = 19; i <= lastRowIndex; i++) {
                Row rowItr = sheet.getRow(i);
                if (rowItr == null)
                    break;
                int totalNoOfCols = rowItr.getLastCellNum();
                TrialBalanceR11DTO trialBalanceR11DTO = new TrialBalanceR11DTO();
                for (int j = 0; j < totalNoOfCols; j++) {
                    switch (j) {
                        case 0:
                            trialBalanceR11DTO.setCompany(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 1:
                            trialBalanceR11DTO.setSite(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 2:
                            trialBalanceR11DTO.setAccount(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 3:
                            trialBalanceR11DTO.setAccountDescription(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 4:
                            trialBalanceR11DTO.setNature(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 5:
                            trialBalanceR11DTO.setNatureDescription(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 6:
                            trialBalanceR11DTO.setCostCenter(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 7:
                            trialBalanceR11DTO.setCostCenterDescription(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 8:
                            trialBalanceR11DTO.setPartner(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 9:
                            trialBalanceR11DTO.setPartnerDescription(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 10:
                            trialBalanceR11DTO.setCurrencyCode(ExcelUtil.getExcelCellValue(rowItr, j));
                            break;
                        case 11:
                            trialBalanceR11DTO.setBeginBalance(ExcelUtil.getExcelCellValueAsNumber(rowItr, j, workbook).replace(",", ""));
                            break;
                        case 12:
                            trialBalanceR11DTO.setPeriodNetDebit(ExcelUtil.getExcelCellValueAsNumber(rowItr, j, workbook).replace(",", ""));
                            break;
                        case 13:
                            trialBalanceR11DTO.setPeriodNetCredit(ExcelUtil.getExcelCellValueAsNumber(rowItr, j, workbook).replace(",", ""));
                            break;
                        case 14:
                            trialBalanceR11DTO.setClosingBalance(ExcelUtil.getExcelCellValueAsNumber(rowItr, j, workbook).replace(",", ""));
                            break;
                    }
                }
                trialBalanceR11DTOS.add(trialBalanceR11DTO);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(r11File).close();
        }
        return trialBalanceR11DTOS;
    }

    public void createTbSheet(List<TrialBalanceR11DTO> trialBalanceR11DTOS, Sheet sheet, CellStyle style, XSSFWorkbook workbook, String lastCostCenterColLetter) throws Exception {
        try {
            for (int i = 0, startRow = 2, j = 3; startRow < trialBalanceR11DTOS.size() + 2; i++, startRow++, j++) {
                Row row = sheet.createRow(startRow);
                ExcelUtil.formula(row, 0, "VLOOKUP(J" + j + ",mapping!A:C,3,0)", sheet);
                ExcelUtil.formula(row, 1, "VLOOKUP(J" + j + ",mapping!A:N,14,0)", sheet);
                ExcelUtil.formula(row, 2, "IF(LEFT(L" + j + ",3)=\"CTR\",\"CTRT\",IFERROR(HLOOKUP(L" + j + ",'Template cost center P&L'!$C$8:$" + lastCostCenterColLetter + "$8,1,FALSE),\"old cost center\"))", sheet);
                ExcelUtil.formula(row, 3, "B" + j + "&C" + j, sheet);
                ExcelUtil.setExcelCellValue(row, 5, trialBalanceR11DTOS.get(i).getCompany());
                ExcelUtil.setExcelCellValue(row, 6, trialBalanceR11DTOS.get(i).getSite());
                ExcelUtil.setExcelCellValue(row, 7, trialBalanceR11DTOS.get(i).getAccount());
                ExcelUtil.setExcelCellValue(row, 8, trialBalanceR11DTOS.get(i).getAccountDescription());
                ExcelUtil.setExcelCellValue(row, 9, trialBalanceR11DTOS.get(i).getNature());
                ExcelUtil.setExcelCellValue(row, 10, trialBalanceR11DTOS.get(i).getNatureDescription());
                ExcelUtil.setExcelCellValue(row, 11, trialBalanceR11DTOS.get(i).getCostCenter());
                ExcelUtil.setExcelCellValue(row, 12, trialBalanceR11DTOS.get(i).getCostCenterDescription());
                ExcelUtil.setExcelCellValue(row, 13, trialBalanceR11DTOS.get(i).getPartner());
                ExcelUtil.setExcelCellValue(row, 14, trialBalanceR11DTOS.get(i).getPartnerDescription());
                ExcelUtil.setExcelCellValue(row, 15, trialBalanceR11DTOS.get(i).getCurrencyCode());
                ExcelUtil.setCellValueAsNumeric(row, style, 16, trialBalanceR11DTOS.get(i).getBeginBalance(), workbook.createDataFormat());
                ExcelUtil.setCellValueAsNumeric(row, style, 17, trialBalanceR11DTOS.get(i).getPeriodNetDebit(), workbook.createDataFormat());
                ExcelUtil.setCellValueAsNumeric(row, style, 18, trialBalanceR11DTOS.get(i).getPeriodNetCredit(), workbook.createDataFormat());
                ExcelUtil.setCellValueAsNumeric(row, style, 19, trialBalanceR11DTOS.get(i).getClosingBalance(), workbook.createDataFormat());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    private void createCoherenceChecking(XSSFWorkbook workbook, List<String> totalLegalCol, Map<Long, CostPlusRemunerationDTO> finalAmountMap) {
        String name = workbook.getSheetName(1);
        Sheet pAndLSheet = workbook.getSheet(name);
        CellStyle style = workbook.createCellStyle();
        CellStyle boldStyle = workbook.createCellStyle();
        CellStyle boldCenterStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        int costCenterStartColumnIndex = Integer.parseInt(totalLegalCol.get(1)) + 1;
        Row coherenceHeadRow = pAndLSheet.getRow(28);
        Row coherenceMessageRow = pAndLSheet.getRow(29);
        Row tbVsCostRow = pAndLSheet.getRow(30);
        Row monthlyVariationRow = pAndLSheet.getRow(32);
        int coherenceIndex = costCenterStartColumnIndex + 1;
        int coherenceFormulaIndex = costCenterStartColumnIndex + 3;

        ExcelUtil.setCellValueBoldCenterFontSize(coherenceHeadRow, boldCenterStyle, font, coherenceIndex, "Coherence checking", 14);
        ExcelUtil.setCellValueAndWrap(coherenceMessageRow, style, coherenceIndex, "Before cost+ remuneration of the month is booked, the gap between the TB and the cost+ template must be equal to the monthly variation of the cost+ remuneration.");

        ExcelUtil.setCellValueBoldAlignLeft(tbVsCostRow, boldStyle, font, coherenceIndex, ". TB vs cost+ template");
        ExcelUtil.formulaWithFormat(tbVsCostRow, coherenceFormulaIndex, style, "ROUND(" + totalLegalCol.get(0) + "28-SUM('TB cost center + cost+ section'!T:T),0)", pAndLSheet, workbook.createDataFormat());
        ExcelUtil.border(tbVsCostRow.getRowNum(), tbVsCostRow.getRowNum(), coherenceFormulaIndex, coherenceFormulaIndex, pAndLSheet);

        ExcelUtil.setCellValueBoldAlignLeft(monthlyVariationRow, boldStyle, font, coherenceIndex, ". Cost+ remuneration monthly variation");
        ExcelUtil.formulaWithFormat(monthlyVariationRow, coherenceFormulaIndex, style, "SUM('accounting scheme'!G6:G" + totalLegalCol.get(2) + ")", pAndLSheet, workbook.createDataFormat());
        ExcelUtil.border(monthlyVariationRow.getRowNum(), monthlyVariationRow.getRowNum(), coherenceFormulaIndex, coherenceFormulaIndex, pAndLSheet);

        String coherenceStartColLetter = ExcelUtil.getColumnLetterByColIndex(coherenceIndex, coherenceHeadRow);
        String coherenceEndColLetter = ExcelUtil.getColumnLetterByColIndex(coherenceFormulaIndex, tbVsCostRow);
        pAndLSheet.addMergedRegion(CellRangeAddress.valueOf("" + coherenceStartColLetter + "29:" + coherenceEndColLetter + "29"));
        pAndLSheet.addMergedRegion(CellRangeAddress.valueOf("" + coherenceStartColLetter + "30:" + coherenceEndColLetter + "30"));

        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
        pAndLSheet.autoSizeColumn(coherenceIndex, true);
        pAndLSheet.autoSizeColumn(coherenceFormulaIndex, true);
    }

    private void applyFormulaForSubTotalAgency(Sheet sheet, int columnIndex, List<String> costCenterColumnLetter, CellStyle style, XSSFWorkbook workbook) {
        CellStyle row19Style = workbook.createCellStyle();
        String startLetter = costCenterColumnLetter.get(0);
        String endLetter = costCenterColumnLetter.get(costCenterColumnLetter.size() - 1);
        Row row9 = sheet.getRow(8);
        ExcelUtil.formulaWithFormat(row9, columnIndex, style, "SUM(" + startLetter + "9:" + endLetter + "9)", sheet, workbook.createDataFormat());
        Row row10 = sheet.getRow(9);
        ExcelUtil.formulaWithFormat(row10, columnIndex, style, "SUM(" + startLetter + "10:" + endLetter + "10)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row9.getRowNum(), row10.getRowNum(), columnIndex, columnIndex, sheet);
        Row row11 = sheet.getRow(10);
        ExcelUtil.formulaWithFormat(row11, columnIndex, style, "SUM(" + startLetter + "11:" + endLetter + "11)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row11.getRowNum(), row11.getRowNum(), columnIndex, columnIndex, sheet);
        Row row13 = sheet.getRow(12);
        ExcelUtil.formulaWithFormat(row13, columnIndex, style, "SUM(" + startLetter + "13:" + endLetter + "13)", sheet, workbook.createDataFormat());
        Row row14 = sheet.getRow(13);
        ExcelUtil.formulaWithFormat(row14, columnIndex, style, "SUM(" + startLetter + "14:" + endLetter + "14)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row13.getRowNum(), row14.getRowNum(), columnIndex, columnIndex, sheet);
        Row row15 = sheet.getRow(14);
        ExcelUtil.formulaWithFormat(row15, columnIndex, style, "SUM(" + startLetter + "15:" + endLetter + "15)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row15.getRowNum(), row15.getRowNum(), columnIndex, columnIndex, sheet);
        Row row17 = sheet.getRow(16);
        ExcelUtil.formulaWithFormat(row17, columnIndex, style, "SUM(" + startLetter + "17:" + endLetter + "17)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row17.getRowNum(), row17.getRowNum(), columnIndex, columnIndex, sheet);
        Row row19 = sheet.getRow(18);
        ExcelUtil.formulaWithFormatBlueColor(row19, columnIndex, row19Style, "SUM(" + startLetter + "19:" + endLetter + "19)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row19.getRowNum(), row19.getRowNum(), columnIndex, columnIndex, sheet);
        Row row22 = sheet.getRow(21);
        ExcelUtil.formulaWithFormat(row22, columnIndex, style, "SUM(" + startLetter + "22:" + endLetter + "22)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row22.getRowNum(), row22.getRowNum(), columnIndex, columnIndex, sheet);
        Row row24 = sheet.getRow(23);
        ExcelUtil.formulaWithFormat(row24, columnIndex, style, "SUM(" + startLetter + "24:" + endLetter + "24)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row24.getRowNum(), row24.getRowNum(), columnIndex, columnIndex, sheet);
        Row row26 = sheet.getRow(25);
        ExcelUtil.formulaWithFormat(row26, columnIndex, style, "SUM(" + startLetter + "26:" + endLetter + "26)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row26.getRowNum(), row26.getRowNum(), columnIndex, columnIndex, sheet);
        Row row28 = sheet.getRow(27);
        ExcelUtil.formulaWithFormat(row28, columnIndex, style, "SUM(" + startLetter + "28:" + endLetter + "28)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row28.getRowNum(), row28.getRowNum(), columnIndex, columnIndex, sheet);
    }

    private void applyFormulaForTotalLegalEntity(Sheet sheet, int columnIndex, String subTotalColLetter, String lastCostCenterColLetter, CellStyle style, XSSFWorkbook workbook) {
        CellStyle row19Style = workbook.createCellStyle();
        Row row9 = sheet.getRow(8);
        ExcelUtil.formulaWithFormat(row9, columnIndex, style, "SUM(" + subTotalColLetter + "9:" + lastCostCenterColLetter + "9)", sheet, workbook.createDataFormat());
        Row row10 = sheet.getRow(9);
        ExcelUtil.formulaWithFormat(row10, columnIndex, style, "SUM(" + subTotalColLetter + "10:" + lastCostCenterColLetter + "10)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row9.getRowNum(), row10.getRowNum(), columnIndex, columnIndex, sheet);
        Row row11 = sheet.getRow(10);
        ExcelUtil.formulaWithFormat(row11, columnIndex, style, "SUM(" + subTotalColLetter + "11:" + lastCostCenterColLetter + "11)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row11.getRowNum(), row11.getRowNum(), columnIndex, columnIndex, sheet);
        Row row13 = sheet.getRow(12);
        ExcelUtil.formulaWithFormat(row13, columnIndex, style, "SUM(" + subTotalColLetter + "13:" + lastCostCenterColLetter + "13)", sheet, workbook.createDataFormat());
        Row row14 = sheet.getRow(13);
        ExcelUtil.formulaWithFormat(row14, columnIndex, style, "SUM(" + subTotalColLetter + "14:" + lastCostCenterColLetter + "14)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row13.getRowNum(), row14.getRowNum(), columnIndex, columnIndex, sheet);
        Row row15 = sheet.getRow(14);
        ExcelUtil.formulaWithFormat(row15, columnIndex, style, "SUM(" + subTotalColLetter + "15:" + lastCostCenterColLetter + "15)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row15.getRowNum(), row15.getRowNum(), columnIndex, columnIndex, sheet);
        Row row17 = sheet.getRow(16);
        ExcelUtil.formulaWithFormat(row17, columnIndex, style, "SUM(" + subTotalColLetter + "17:" + lastCostCenterColLetter + "17)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row17.getRowNum(), row17.getRowNum(), columnIndex, columnIndex, sheet);
        Row row19 = sheet.getRow(18);
        ExcelUtil.formulaWithFormatBlueColor(row19, columnIndex, row19Style, "SUM(" + subTotalColLetter + "19:" + lastCostCenterColLetter + "19)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row19.getRowNum(), row19.getRowNum(), columnIndex, columnIndex, sheet);
        Row row22 = sheet.getRow(21);
        ExcelUtil.formulaWithFormat(row22, columnIndex, style, "SUM(" + subTotalColLetter + "22:" + lastCostCenterColLetter + "22)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row22.getRowNum(), row22.getRowNum(), columnIndex, columnIndex, sheet);
        Row row24 = sheet.getRow(23);
        ExcelUtil.formulaWithFormat(row24, columnIndex, style, "SUM(" + subTotalColLetter + "24:" + lastCostCenterColLetter + "24)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row24.getRowNum(), row24.getRowNum(), columnIndex, columnIndex, sheet);
        Row row26 = sheet.getRow(25);
        ExcelUtil.formulaWithFormat(row26, columnIndex, style, "SUM(" + subTotalColLetter + "26:" + lastCostCenterColLetter + "26)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row26.getRowNum(), row26.getRowNum(), columnIndex, columnIndex, sheet);
        Row row28 = sheet.getRow(27);
        ExcelUtil.formulaWithFormat(row28, columnIndex, style, "SUM(" + subTotalColLetter + "28:" + lastCostCenterColLetter + "28)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row28.getRowNum(), row28.getRowNum(), columnIndex, columnIndex, sheet);
    }

    public void applyFormulaForCostCenterAccounting(Sheet sheet, CellStyle style, int columnIndex, String columnLetter, XSSFWorkbook workbook, String catName) {
        XSSFFont redFont = workbook.createFont();
        CellStyle blueStyle = workbook.createCellStyle();
        CellStyle grayStyle = workbook.createCellStyle();

        Row row9 = sheet.getRow(8);
        ExcelUtil.formulaWithFormat(row9, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B9&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        Row row10 = sheet.getRow(9);
        ExcelUtil.formulaWithFormat(row10, columnIndex, style, columnLetter + "74", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row9.getRowNum(), row10.getRowNum(), columnIndex, columnIndex, sheet);
        Row row11 = sheet.getRow(10);
        ExcelUtil.formulaWithFormat(row11, columnIndex, style, "SUM(" + columnLetter + "9:" + columnLetter + "10)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row11.getRowNum(), row11.getRowNum(), columnIndex, columnIndex, sheet);
        Row row13 = sheet.getRow(12);
        if (catName.startsWith("Cost"))
            ExcelUtil.formulaWithFormatRedColor(row13, columnIndex, redFont, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B13&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", workbook, workbook.createDataFormat());
        else
            ExcelUtil.formulaWithFormat(row13, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B13&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        Row row14 = sheet.getRow(13);
        ExcelUtil.formulaWithFormat(row14, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B14&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row13.getRowNum(), row14.getRowNum(), columnIndex, columnIndex, sheet);
        Row row15 = sheet.getRow(14);
        ExcelUtil.formulaWithFormat(row15, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B15&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row15.getRowNum(), row15.getRowNum(), columnIndex, columnIndex, sheet);
        Row row17 = sheet.getRow(16);
        ExcelUtil.formulaWithFormat(row17, columnIndex, style, "SUM(" + columnLetter + "13:" + columnLetter + "15)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row17.getRowNum(), row17.getRowNum(), columnIndex, columnIndex, sheet);
        Row row19 = sheet.getRow(18);
        if (catName.startsWith("Cost"))
            ExcelUtil.formulaWithFormatBlueColor(row19, columnIndex, blueStyle, "SUM(" + columnLetter + "17," + columnLetter + "11)", sheet, workbook.createDataFormat());
        else
            ExcelUtil.formulaWithFormatGrayColor(row19, columnIndex, grayStyle, "SUM(" + columnLetter + "17," + columnLetter + "11)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row19.getRowNum(), row19.getRowNum(), columnIndex, columnIndex, sheet);
        Row row22 = sheet.getRow(21);
        ExcelUtil.formulaWithFormat(row22, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B22&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row22.getRowNum(), row22.getRowNum(), columnIndex, columnIndex, sheet);
        Row row24 = sheet.getRow(23);
        ExcelUtil.formulaWithFormat(row24, columnIndex, style, "SUM(" + columnLetter + "22," + columnLetter + "19)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row24.getRowNum(), row24.getRowNum(), columnIndex, columnIndex, sheet);
        Row row26 = sheet.getRow(25);
        if (!catName.startsWith("Cost"))
            ExcelUtil.formulaWithFormatRedColor(row26, columnIndex, redFont, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B26&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", workbook, workbook.createDataFormat());
        else
            ExcelUtil.formulaWithFormat(row26, columnIndex, style, "SUMIF('TB cost center + cost+ section'!$D:$D,'Template cost center P&L'!$B26&'Template cost center P&L'!" + columnLetter + "$8,'TB cost center + cost+ section'!$T:$T)", sheet, workbook.createDataFormat());
        ExcelUtil.borderThin(row26.getRowNum(), row26.getRowNum(), columnIndex, columnIndex, sheet);
        Row row28 = sheet.getRow(27);
        ExcelUtil.formulaWithFormat(row28, columnIndex, style, "SUM(" + columnLetter + "26," + columnLetter + "24)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row28.getRowNum(), row28.getRowNum(), columnIndex, columnIndex, sheet);

    }

    public void generateAllocationKeys(XSSFWorkbook workbook, CostPlusTemplateProcess templateProcess, CellStyle redStyle, Map<String, List<String>> subcategoryCostColLetterMap, Map<Integer, Long> columnCostCenterIdMap, Map<String, String> costCenterColumnMap, List<String> costScopeCostCenterColLetter, String subTotalColLetter) throws Exception {
        try {
            String frontOfficeStartLetter = costScopeCostCenterColLetter.get(0);
            String frontOfficeEndLetter = costScopeCostCenterColLetter.get(costScopeCostCenterColLetter.size() - 1);
            XSSFFont font = workbook.createFont();
            CellStyle style = workbook.createCellStyle();

            String name = workbook.getSheetName(1);
            Sheet pAndLSheet = workbook.getSheet(name);
            Map<String, Map<Long, Double>> allocationKeysMap = new HashMap<>();
            List<String> months = ExcelUtil.getMonthList(templateProcess.getMonth());
            List<Tuple> sumOfBLAndTEU = volumeReportRepository.getSumOfBLAndTEUByMonthYear(months, templateProcess.getYear().toString(), templateProcess.getAgency().getId());
            List<String> allocationKeys = Arrays.asList("NBBL-LOADED-DISCHARGED", "NBBL-BOOKED", "NBTEU-LOADED", "NBTEU-DISCHARGED", "NB-TRANSHIPMENT", "NBTEU-IMPORT-EXPORT");
            Map<String, Map<String, Map<String, Double>>> costCenterTypeBLandTEUMap = new HashMap<>();
            for (Tuple tuple : sumOfBLAndTEU) {
                Map<String, Map<String, Double>> typeBLandTEUMap = new HashMap<>();
                Map<String, Double> blAndTEUMap = new HashMap<>();
                blAndTEUMap.put("BLNumber", Double.valueOf(tuple.get(0).toString()));
                blAndTEUMap.put("sumOfTEU", Double.valueOf(tuple.get(1).toString()));
                typeBLandTEUMap.put(tuple.get(2).toString(), blAndTEUMap);
                costCenterTypeBLandTEUMap.put(tuple.get(3) + "-" + tuple.get(2), typeBLandTEUMap);
            }
            for (String allocationKey : allocationKeys) {
                Map<Long, Double> nbblLoadedImportCostCenterValueMap = new HashMap<>();
                Map<Long, Double> nbblBookedCostCenterValueMap = new HashMap<>();
                Map<Long, Double> nbteuLoadedCostCenterValueMap = new HashMap<>();
                Map<Long, Double> nbteuImportCostCenterValueMap = new HashMap<>();
                Map<Long, Double> nbTranshipmentCostCenterValueMap = new HashMap<>();
                Map<Long, Double> nbteuImportExportCostCenterValueMap = new HashMap<>();
                for (Map.Entry<Integer, Long> columnCostCenterIdEntry : columnCostCenterIdMap.entrySet()) {
                    Long costCenterId = columnCostCenterIdEntry.getValue();
                    String costCenterExportLoaded = costCenterId + "-" + "EXPORT LOADED";
                    String costCenterExportBooked = costCenterId + "-" + "EXPORT BOOKED";
                    String costCenterExportImport = costCenterId + "-" + "IMPORT";
                    String costCenterTranshipment = costCenterId + "-" + "TRANSHIPMENT";
                    switch (allocationKey) {
                        case "NBBL-LOADED-DISCHARGED":
                            double nbBlLoad = costCenterTypeBLandTEUMap.containsKey(costCenterExportLoaded) ? costCenterTypeBLandTEUMap.get(costCenterExportLoaded).get("EXPORT LOADED").get("BLNumber") : 0d;
                            double nbBlImp = costCenterTypeBLandTEUMap.containsKey(costCenterExportImport) ? costCenterTypeBLandTEUMap.get(costCenterExportImport).get("IMPORT").get("BLNumber") : 0d;
                            Double totalLoadedAndImport = nbBlLoad + nbBlImp;
                            nbblLoadedImportCostCenterValueMap.put(costCenterId, totalLoadedAndImport);
                            allocationKeysMap.put("NBBL-LOADED-DISCHARGED", nbblLoadedImportCostCenterValueMap);
                            break;
                        case "NBBL-BOOKED":
                            double nbBlBook = costCenterTypeBLandTEUMap.containsKey(costCenterExportBooked) ? costCenterTypeBLandTEUMap.get(costCenterExportBooked).get("EXPORT BOOKED").get("BLNumber") : 0d;
                            nbblBookedCostCenterValueMap.put(costCenterId, nbBlBook);
                            allocationKeysMap.put("NBBL-BOOKED", nbblBookedCostCenterValueMap);
                            break;
                        case "NBTEU-LOADED":
                            double nbTeuLoad = costCenterTypeBLandTEUMap.containsKey(costCenterExportLoaded) ? costCenterTypeBLandTEUMap.get(costCenterExportLoaded).get("EXPORT LOADED").get("sumOfTEU") : 0d;
                            nbteuLoadedCostCenterValueMap.put(costCenterId, nbTeuLoad);
                            allocationKeysMap.put("NBTEU-LOADED", nbteuLoadedCostCenterValueMap);
                            break;
                        case "NBTEU-DISCHARGED":
                            double nbTeuImp = costCenterTypeBLandTEUMap.containsKey(costCenterExportImport) ? costCenterTypeBLandTEUMap.get(costCenterExportImport).get("IMPORT").get("sumOfTEU") : 0d;
                            nbteuImportCostCenterValueMap.put(costCenterId, nbTeuImp);
                            allocationKeysMap.put("NBTEU-DISCHARGED", nbteuImportCostCenterValueMap);
                            break;
                        case "NB-TRANSHIPMENT":
                            double nbTran = costCenterTypeBLandTEUMap.containsKey(costCenterTranshipment) ? costCenterTypeBLandTEUMap.get(costCenterTranshipment).get("TRANSHIPMENT").get("sumOfTEU") : 0d;
                            nbTranshipmentCostCenterValueMap.put(costCenterId, nbTran);
                            allocationKeysMap.put("NB-TRANSHIPMENT", nbTranshipmentCostCenterValueMap);
                            break;
                        case "NBTEU-IMPORT-EXPORT":
                            double expLoad = costCenterTypeBLandTEUMap.containsKey(costCenterExportLoaded) ? costCenterTypeBLandTEUMap.get(costCenterExportLoaded).get("EXPORT LOADED").get("sumOfTEU") : 0d;
                            double expImp = costCenterTypeBLandTEUMap.containsKey(costCenterExportImport) ? costCenterTypeBLandTEUMap.get(costCenterExportImport).get("IMPORT").get("sumOfTEU") : 0d;
                            double trans = costCenterTypeBLandTEUMap.containsKey(costCenterTranshipment) ? costCenterTypeBLandTEUMap.get(costCenterTranshipment).get("TRANSHIPMENT").get("sumOfTEU") : 0d;
                            Double totalLoadedImport = expLoad + expImp;
                            Double totalLoadedImport20 = (0.2 * trans);
                            Double totalImportExport = totalLoadedImport + totalLoadedImport20;
                            nbteuImportExportCostCenterValueMap.put(costCenterId, totalImportExport);
                            allocationKeysMap.put("NBTEU-IMPORT-EXPORT", nbteuImportExportCostCenterValueMap);
                            break;
                    }
                }
            }
            int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
            int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
            XSSFFont boldFont = workbook.createFont();
            CellStyle noFormulaGrayStyle = workbook.createCellStyle();

            Row row33 = pAndLSheet.getRow(33);
            Row row35 = pAndLSheet.getRow(35);
            Row row37 = pAndLSheet.getRow(37);
            Row row38 = pAndLSheet.getRow(38);
            Row row39 = pAndLSheet.getRow(39);
            Row row40 = pAndLSheet.getRow(40);

            for (Map.Entry<String, Map<Long, Double>> allocationKeysEntry : allocationKeysMap.entrySet()) {
                switch (allocationKeysEntry.getKey()) {
                    case "NBBL-LOADED-DISCHARGED":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (i == CellReference.convertColStringToIndex(costCenterColumnMap.get("C-CFAP"))) {
                                ExcelUtil.setCellValueWrapFillGrayColor(row33, noFormulaGrayStyle, boldFont, i, "");
                            } else {
                                if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                    value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                    ExcelUtil.setCellValueAsNumericRedColor(row33, redStyle, font, i, value.toString(), workbook.createDataFormat());
                                } else {
                                    ExcelUtil.setCellValueAsNumericRedColor(row33, redStyle, font, i, value.toString(), workbook.createDataFormat());
                                }
                            }
                            ExcelUtil.borderThin(row33.getRowNum(), row33.getRowNum(), i, i, pAndLSheet);
                        }
                        ExcelUtil.formulaWithFormat(row33, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "34:" + frontOfficeEndLetter + "34)", pAndLSheet, workbook.createDataFormat());
                        ExcelUtil.borderThin(row33.getRowNum(), row33.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), pAndLSheet);
                        break;
                    case "NBBL-BOOKED":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                ExcelUtil.setCellValueAsNumericRedColor(row35, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            } else {
                                ExcelUtil.setCellValueAsNumericRedColor(row35, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            }
                            ExcelUtil.borderThin(row35.getRowNum(), row35.getRowNum(), i, i, pAndLSheet);
                        }
                        ExcelUtil.formulaWithFormat(row35, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "36:" + frontOfficeEndLetter + "36)", pAndLSheet, workbook.createDataFormat());
                        ExcelUtil.borderThin(row35.getRowNum(), row35.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), pAndLSheet);
                        break;
                    case "NBTEU-LOADED":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                ExcelUtil.setCellValueAsNumericRedColor(row37, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            } else {
                                ExcelUtil.setCellValueAsNumericRedColor(row37, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            }
                        }
                        ExcelUtil.formulaWithFormat(row37, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "38:" + frontOfficeEndLetter + "38)", pAndLSheet, workbook.createDataFormat());
                        break;
                    case "NBTEU-DISCHARGED":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                ExcelUtil.setCellValueAsNumericRedColor(row38, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            } else {
                                ExcelUtil.setCellValueAsNumericRedColor(row38, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            }
                        }
                        ExcelUtil.formulaWithFormat(row38, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "39:" + frontOfficeEndLetter + "39)", pAndLSheet, workbook.createDataFormat());
                        break;
                    case "NB-TRANSHIPMENT":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                ExcelUtil.setCellValueAsNumericRedColor(row39, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            } else {
                                ExcelUtil.setCellValueAsNumericRedColor(row39, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            }
                        }
                        ExcelUtil.formulaWithFormat(row39, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "40:" + frontOfficeEndLetter + "40)", pAndLSheet, workbook.createDataFormat());
                        break;
                    case "NBTEU-IMPORT-EXPORT":
                        for (int i = startIndex; i <= endIndex; i++) {
                            Double value = 0d;
                            if (allocationKeysEntry.getValue().containsKey(columnCostCenterIdMap.get(i))) {
                                value = allocationKeysEntry.getValue().get(columnCostCenterIdMap.get(i));
                                ExcelUtil.setCellValueAsNumericRedColor(row40, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            } else {
                                ExcelUtil.setCellValueAsNumericRedColor(row40, redStyle, font, i, value.toString(), workbook.createDataFormat());
                            }
                        }
                        ExcelUtil.formulaWithFormat(row40, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "41:" + frontOfficeEndLetter + "41)", pAndLSheet, workbook.createDataFormat());
                        break;
                }
            }
            ExcelUtil.borderThin(row37.getRowNum(), row40.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), pAndLSheet);
            for (int startRow = 43; startRow <= 45; startRow++) {
                int i = 3;
                Row row = pAndLSheet.getRow(startRow);
                switch (startRow) {
                    case 43:
                        for (i = startIndex; i <= endIndex; i++) {
                            Cell cell = row.createCell(i);
                            if (i == CellReference.convertColStringToIndex(costCenterColumnMap.get("C-CFAP"))) {
                                ExcelUtil.setCellValueWrapFillGrayColor(row, noFormulaGrayStyle, boldFont, i, "");
                            } else {
                                String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
                                ExcelUtil.percentageFormula(row, i, style, columnLetter + "34/$" + subTotalColLetter + "34", pAndLSheet, workbook.createDataFormat());
                            }
                        }
                        ExcelUtil.percentageFormula(row, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "44:" + frontOfficeEndLetter + "44)", pAndLSheet, workbook.createDataFormat());
                        break;
                    case 44:
                        for (i = startIndex; i <= endIndex; i++) {
                            Cell cell = row.createCell(i);
                            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
                            ExcelUtil.percentageFormula(row, i, style, "IF(ISERROR(" + columnLetter + "36/$" + subTotalColLetter + "36)=TRUE,0,(" + columnLetter + "36/$" + subTotalColLetter + "36))", pAndLSheet, workbook.createDataFormat());
                        }
                        ExcelUtil.percentageFormula(row, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "45:" + frontOfficeEndLetter + "45)", pAndLSheet, workbook.createDataFormat());
                        break;
                    case 45:
                        for (i = startIndex; i <= endIndex; i++) {
                            Cell cell = row.createCell(i);
                            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
                            ExcelUtil.percentageFormula(row, i, style, columnLetter + "41/$" + subTotalColLetter + "41", pAndLSheet, workbook.createDataFormat());
                        }
                        ExcelUtil.percentageFormula(row, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "46:" + frontOfficeEndLetter + "46)", pAndLSheet, workbook.createDataFormat());
                        break;
                }
            }
            for (int i = startIndex; i <= endIndex; i++) {
                ExcelUtil.borderThin(row37.getRowNum(), row40.getRowNum(), i, i, pAndLSheet);
                ExcelUtil.borderThin(pAndLSheet.getRow(43).getRowNum(), pAndLSheet.getRow(45).getRowNum(), i, i, pAndLSheet);
            }
            ExcelUtil.borderThin(pAndLSheet.getRow(43).getRowNum(), pAndLSheet.getRow(45).getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), pAndLSheet);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void applyFormulaForAllocationOfCostPerCarrier(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, List<String> costScopeCostCenterColLetter, String subTotalColLetter) throws Exception {
        String frontOfficeStartLetter = costScopeCostCenterColLetter.get(0);
        String frontOfficeEndLetter = costScopeCostCenterColLetter.get(costScopeCostCenterColLetter.size() - 1);

        applyFormulaForFrontOffice(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForFrontOfficeMutualized(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForDocumentation(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForSupportToOperation(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForSupportFunctions(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForIndirectCosts(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForManagementItFees(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForOpeningCostBase(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);

        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        Row row51 = sheet.getRow(50);
        Row row56 = sheet.getRow(55);
        Row row57 = sheet.getRow(56);
        for (int i = startIndex; i <= endIndex; i++) {
            ExcelUtil.borderThin(row51.getRowNum(), row56.getRowNum(), i, i, sheet);
        }
        ExcelUtil.borderThin(row51.getRowNum(), row56.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);
        ExcelUtil.borderThin(row57.getRowNum(), row57.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);

    }

    private void applyFormulaForOpeningCostBase(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();
        Row row59 = sheet.getRow(58);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row59.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row59, i, style, "SUM(" + columnLetter + "51:" + columnLetter + "57)", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row59, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
            ExcelUtil.border(row59.getRowNum(), row59.getRowNum(), i, i, sheet);
        }
        ExcelUtil.formulaWithFormat(row59, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "59:" + frontOfficeEndLetter + "59)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row59.getRowNum(), row59.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);
    }

    private void applyFormulaForManagementItFees(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = costCenterColumnMap.get("C-CFMU");
        String endLetter = costCenterColumnMap.get("C-CGEN");
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();
        Row row57 = sheet.getRow(56);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row57.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            String formula = columnLetter + "15+SUM(" + startLetter + "15:" + endLetter + "15)*" + columnLetter + "46";
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row57, i, style, formula, sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row57, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
            ExcelUtil.borderThin(row57.getRowNum(), row57.getRowNum(), i, i, sheet);
        }
        ExcelUtil.formulaWithFormat(row57, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "57:" + frontOfficeEndLetter + "57)", sheet, workbook.createDataFormat());
    }

    private void applyFormulaForIndirectCosts(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Indirect costs", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Indirect costs", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row56 = sheet.getRow(55);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row56.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            String formula = columnLetter + "46*SUM($" + startLetter + "$13:$" + endLetter + "$14)";
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row56, i, style, formula, sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row56, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row56, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "56:" + frontOfficeEndLetter + "56)", sheet, workbook.createDataFormat());
    }

    private void applyFormulaForSupportFunctions(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        Row row55 = sheet.getRow(54);
        String startLetter = ExcelUtil.getColumnLetter("Support functions", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Support functions", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell8 = row55.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell8.getColumnIndex());
            String formula = columnLetter + "46*SUM($" + startLetter + "$13:$" + endLetter + "$14)";
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row55, i, style, formula, sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row55, noFormulaGrayStyle, boldFont, cell8.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row55, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "55:" + frontOfficeEndLetter + "55)", sheet, workbook.createDataFormat());

    }

    private void applyFormulaForSupportToOperation(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        Row row54 = sheet.getRow(53);
        String startLetter = ExcelUtil.getColumnLetter("Support to Operations", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Support to Operations", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row54.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            String formula = columnLetter + "46*SUM($" + startLetter + "$13:$" + endLetter + "$14)";
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row54, i, style, formula, sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row54, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row54, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "54:" + frontOfficeEndLetter + "54)", sheet, workbook.createDataFormat());

    }

    private void applyFormulaForDocumentation(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Documentation", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Documentation", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        String aplColumnLetter = costCenterColumnMap.get("C-CFAP");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row53 = sheet.getRow(52);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row53.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter) && !columnLetter.equals(aplColumnLetter))
                ExcelUtil.formulaWithFormat(row53, i, style, "SUM($" + startLetter + "$13:$" + endLetter + "$14)*" + columnLetter + "44", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row53, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row53, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "53:" + frontOfficeEndLetter + "53)", sheet, workbook.createDataFormat());

    }

    private void applyFormulaForFrontOfficeMutualized(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) throws Exception {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row52 = sheet.getRow(51);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row52.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            try {
                if (!columnLetter.equals(mutualizedColumnLetter))
                    ExcelUtil.formulaWithFormat(row52, i, style, "SUM($" + mutualizedColumnLetter + "$13:$" + mutualizedColumnLetter + "$14)*" + columnLetter + "45", sheet, workbook.createDataFormat());
                else
                    ExcelUtil.setCellValueWrapFillGrayColor(row52, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            }
        }
        ExcelUtil.formulaWithFormat(row52, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "52:" + frontOfficeEndLetter + "52)", sheet, workbook.createDataFormat());

    }

    public void applyFormulaForFrontOffice(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row51 = sheet.getRow(50);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row51.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row51, i, style, "SUM(" + columnLetter + "13:" + columnLetter + "14)", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row51, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row51, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "51:" + frontOfficeEndLetter + "51)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRemunerationCalculation(XSSFWorkbook workbook, Sheet sheet, CellStyle style, XSSFWorkbook sheets, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, Agency agency, String subTotalColLetter, List<String> costScopeCostCenterColLetter) {
        String frontOfficeStartLetter = costScopeCostCenterColLetter.get(0);
        String frontOfficeEndLetter = costScopeCostCenterColLetter.get(costScopeCostCenterColLetter.size() - 1);

        applyMarkupPercentage(agency, sheet, workbook);
        applyFormulaForRCFrontOffice(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCFrontOfficeMutualized(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCDocumentation(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCSupportToOperation(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCSupportFunctions(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCIndirectCosts(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCManagementItFees(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);
        applyFormulaForRCCarrierRemuneration(sheet, style, workbook, subcategoryCostColLetterMap, costCenterColumnMap, subTotalColLetter, frontOfficeStartLetter, frontOfficeEndLetter);

        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        Row row66 = sheet.getRow(65);
        Row row71 = sheet.getRow(70);
        Row row72 = sheet.getRow(71);
        for (int i = startIndex; i <= endIndex; i++) {
            ExcelUtil.borderThin(row66.getRowNum(), row71.getRowNum(), i, i, sheet);
        }
        ExcelUtil.borderThin(row66.getRowNum(), row71.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);
        ExcelUtil.borderThin(row72.getRowNum(), row72.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);
    }

    private void applyMarkupPercentage(Agency agency, Sheet sheet, XSSFWorkbook workbook) {
        Cell cell = sheet.getRow(63).getCell(3);
        cell.setCellFormula(agency.getMarkup() + "/100");
    }

    public void applyFormulaForRCFrontOffice(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row66 = sheet.getRow(65);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row66.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row66, i, style, "-" + columnLetter + "51*(1+$D$64)-" + columnLetter + "9", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row66, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row66, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "66:" + frontOfficeEndLetter + "66)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCFrontOfficeMutualized(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row67 = sheet.getRow(66);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row67.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row67, i, style, "-" + columnLetter + "52*(1+$D$64)-$" + mutualizedColumnLetter + "$9*" + columnLetter + "$45", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row67, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row67, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "67:" + frontOfficeEndLetter + "67)", sheet, workbook.createDataFormat());

    }

    public void applyFormulaForRCDocumentation(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Documentation", subcategoryCostColLetterMap, true);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        String aplColumnLetter = costCenterColumnMap.get("C-CFAP");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row68 = sheet.getRow(67);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row68.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter) && !columnLetter.equals(aplColumnLetter))
                ExcelUtil.formulaWithFormat(row68, i, style, "-" + columnLetter + "53*(1+$D$64)-$" + startLetter + "$9*" + columnLetter + "$44", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row68, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row68, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "68:" + frontOfficeEndLetter + "68)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCSupportToOperation(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Support to Operations", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Support to Operations", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row69 = sheet.getRow(68);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row69.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row69, i, style, "-" + columnLetter + "54*(1+$D$64)-SUM($" + startLetter + "$9:$" + endLetter + "$9)*" + columnLetter + "$46", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row69, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row69, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "69:" + frontOfficeEndLetter + "69)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCSupportFunctions(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Support functions", subcategoryCostColLetterMap, true);
        String endLetter = ExcelUtil.getColumnLetter("Support functions", subcategoryCostColLetterMap, false);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row70 = sheet.getRow(69);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row70.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row70, i, style, "-" + columnLetter + "55*(1+$D$64)-SUM($" + startLetter + "$9:$" + endLetter + "$9)*" + columnLetter + "$46", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row70, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row70, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "70:" + frontOfficeEndLetter + "70)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCIndirectCosts(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        String startLetter = ExcelUtil.getColumnLetter("Indirect costs", subcategoryCostColLetterMap, true);
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row71 = sheet.getRow(70);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row71.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row71, i, style, "-" + columnLetter + "56*(1+$D$64)-$" + startLetter + "$9*" + columnLetter + "$46", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row71, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
        }
        ExcelUtil.formulaWithFormat(row71, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "71:" + frontOfficeEndLetter + "71)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCManagementItFees(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row72 = sheet.getRow(71);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row72.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row72, i, style, "-" + columnLetter + "57", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row72, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
            ExcelUtil.borderThin(row72.getRowNum(), row72.getRowNum(), i, i, sheet);
        }
        ExcelUtil.formulaWithFormat(row72, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "72:" + frontOfficeEndLetter + "72)", sheet, workbook.createDataFormat());
    }

    public void applyFormulaForRCCarrierRemuneration(Sheet sheet, CellStyle style, XSSFWorkbook workbook, Map<String, List<String>> subcategoryCostColLetterMap, Map<String, String> costCenterColumnMap, String subTotalColLetter, String frontOfficeStartLetter, String frontOfficeEndLetter) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subcategoryCostColLetterMap, false);
        String mutualizedColumnLetter = costCenterColumnMap.get("C-CFMU");
        XSSFFont boldFont = workbook.createFont();
        CellStyle noFormulaGrayStyle = workbook.createCellStyle();

        Row row74 = sheet.getRow(73);
        for (int i = startIndex; i <= endIndex; i++) {
            Cell cell = row74.createCell(i);
            String columnLetter = CellReference.convertNumToColString(cell.getColumnIndex());
            if (!columnLetter.equals(mutualizedColumnLetter))
                ExcelUtil.formulaWithFormat(row74, i, style, "SUM(" + columnLetter + "66:" + columnLetter + "72)", sheet, workbook.createDataFormat());
            else
                ExcelUtil.setCellValueWrapFillGrayColor(row74, noFormulaGrayStyle, boldFont, cell.getColumnIndex(), "");
            ExcelUtil.border(row74.getRowNum(), row74.getRowNum(), i, i, sheet);
        }
        ExcelUtil.formulaWithFormat(row74, CellReference.convertColStringToIndex(subTotalColLetter), style, "SUM(" + frontOfficeStartLetter + "74:" + frontOfficeEndLetter + "74)", sheet, workbook.createDataFormat());
        ExcelUtil.border(row74.getRowNum(), row74.getRowNum(), CellReference.convertColStringToIndex(subTotalColLetter), CellReference.convertColStringToIndex(subTotalColLetter), sheet);
    }

    private void generateAccountingScheme(XSSFWorkbook workbook, CostPlusTemplateProcess templateProcess, Agency agency, Map<Long, String> costCenterRemunerationMap, Map<String, List<String>> subCategoryCostCenterColumnMap, Map<Integer, Long> columnCostCenterIdMap, Map<Long, CostPlusRemunerationDTO> finalAmountMap, User user, List<String> totalLegalColAndAccSchSum, List<Long> mappedVatCarries) {
        int startIndex = ExcelUtil.getExcelColumnIndex(subCategoryCostCenterColumnMap, true);
        int endIndex = ExcelUtil.getExcelColumnIndex(subCategoryCostCenterColumnMap, false);
        String name = workbook.getSheetName(1);
        Sheet pAndLSheet = workbook.getSheet(name);
        Row row74 = pAndLSheet.getRow(73);
        for (int i = startIndex; i < endIndex; i++) {
            Cell cell = row74.getCell(i);
            String value = ExcelUtil.getCellRawValue(workbook, row74, cell.getColumnIndex());
            costCenterRemunerationMap.put(columnCostCenterIdMap.get(i), value);
        }

        String accountingSheetName = workbook.getSheetName(3);
        CellStyle style = workbook.createCellStyle();
        Sheet accountingSheet = workbook.getSheet(accountingSheetName);

        List<CostPlusRemunerationDTO> costPlusRemunerationDTOS = new ArrayList<>();
        Map<Long, CostPlusRemunerationDTO> costCenterAmountMap = new HashMap<>();
        Map<Long, String> ytdAmountMap = new HashMap<>();
        for (Map.Entry<Long, String> costCenterRemuneration :
                costCenterRemunerationMap.entrySet()) {
            CostCenter costCenter = costCenterRepository.findCostCenterById(costCenterRemuneration.getKey());
            CostPlusRemunerationDTO costPlusRemunerationDTO = new CostPlusRemunerationDTO();
            costPlusRemunerationDTO.setAgencyId(templateProcess.getAgency().getId());
            costPlusRemunerationDTO.setMonth(templateProcess.getMonth());
            costPlusRemunerationDTO.setYear(templateProcess.getYear());
            costPlusRemunerationDTO.setCostCenterId(costCenter.getId());
            costPlusRemunerationDTO.setCarrierName(costCenter.getCarrierName());
            costPlusRemunerationDTO.setCarrierCode(costCenter.getCarrierCode());
            costPlusRemunerationDTO.setCostCenterCode(costCenter.getCode());
            costPlusRemunerationDTO.setAmount(Double.valueOf(costCenterRemuneration.getValue()));
            costCenterAmountMap.put(costCenter.getId(), costPlusRemunerationDTO);
        }
        List<String> months = ExcelUtil.getMonthList(templateProcess.getMonth());
        costPlusRemunerationRepository.disableSavedRemuneration(templateProcess.getId(), user.getId());
        List<Tuple> ytdCarrierRemuneration = costPlusRemunerationRepository.sumOfYTDCarrierRemuneration(templateProcess.getYear(), months, agency.getId());
        for (Tuple tuple :
                ytdCarrierRemuneration) {
            ytdAmountMap.put(Long.valueOf(tuple.get("ccId").toString()), tuple.get("ytdAmount").toString());
        }
        for (Map.Entry<Long, CostPlusRemunerationDTO> entry :
                costCenterAmountMap.entrySet()) {
            CostPlusRemunerationDTO costPlusRemunerationDTO = entry.getValue();
            costPlusRemunerationDTO.setCreatedBy(user.getId());
            if (ytdAmountMap.containsKey(costPlusRemunerationDTO.getCostCenterId())) {
                costPlusRemunerationDTO.setYtdAmount(Double.valueOf(ytdAmountMap.get(costPlusRemunerationDTO.getCostCenterId())));
            } else {
                costPlusRemunerationDTO.setYtdAmount(0d);
            }
            costPlusRemunerationDTOS.add(costPlusRemunerationDTO);
        }
        createAccountingSchemeSheet(costPlusRemunerationDTOS, workbook, accountingSheet, style, templateProcess, agency, finalAmountMap, totalLegalColAndAccSchSum, mappedVatCarries);
    }

    private void createAccountingSchemeSheet(List<CostPlusRemunerationDTO> costPlusRemunerationDTOS, XSSFWorkbook workbook, Sheet accountingSheet, CellStyle style, CostPlusTemplateProcess templateProcess, Agency agency, Map<Long, CostPlusRemunerationDTO> finalAmountMap, List<String> totalLegalColAndAccSchSum, List<Long> mappedVatCarries) {
        try {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
            List<CostPlusRemunerationDTO> finalRemunerationDTOS = new ArrayList<>();
            CellStyle headStyle = workbook.createCellStyle();
            XSSFFont headBoldFont = workbook.createFont();
            Row headerOneRow = accountingSheet.getRow(3);
            Row headerTwoRow = accountingSheet.getRow(4);
            ExcelUtil.setCellStyleFillBlueColorLeft(headerOneRow, headStyle, headBoldFont, 2);
            for (int startCol = 2; startCol <= 6; startCol++) {
                ExcelUtil.setCellStyleFillBlueColorCentre(headerTwoRow, headStyle, headBoldFont, startCol);
            }
            if (agency.isVatGstFlag()) {
                CellStyle headerStyle = workbook.createCellStyle();
                XSSFFont boldFont = workbook.createFont();
                ExcelUtil.setCellValueWrapFillBlueColor(headerTwoRow, headerStyle, boldFont, 7, "VAT " + agency.getVatPercentage() + " %");
                ExcelUtil.setCellValueWrapFillBlueColor(headerTwoRow, headerStyle, boldFont, 8, "Total");
                accountingSheet.addMergedRegion(CellRangeAddress.valueOf("C4:" + "I4"));
                List<AgencyVatCarriers> savedAgencyVatCarriers = agencyVatCarriersRepository.findByAgencyIdAndDeleted(agency.getId(), false);
                if (savedAgencyVatCarriers.size() > 0) {
                    for (AgencyVatCarriers vatCarriers : savedAgencyVatCarriers) {
                        mappedVatCarries.add(vatCarriers.getCostCenterId());
                    }
                }
            } else {
                accountingSheet.addMergedRegion(CellRangeAddress.valueOf("C4:" + "G4"));
            }

            for (int startRow = 5, index = 0, j = 6; startRow < (costPlusRemunerationDTOS.size() + 5); startRow++, index++, j++) {
                Row row = accountingSheet.createRow(startRow);
                ExcelUtil.setExcelCellValue(row, 2, costPlusRemunerationDTOS.get(index).getCostCenterCode());
                ExcelUtil.setExcelCellValue(row, 3, costPlusRemunerationDTOS.get(index).getCarrierCode());
                ExcelUtil.setCellValueAsNumeric(row, style, 4, costPlusRemunerationDTOS.get(index).getYtdAmount().toString(), workbook.createDataFormat());
                ExcelUtil.setCellValueAsNumeric(row, style, 5, costPlusRemunerationDTOS.get(index).getAmount().toString(), workbook.createDataFormat());

                ExcelUtil.formulaWithFormat(row, 6, style, "F" + j + "-E" + j, accountingSheet, workbook.createDataFormat());
                if (agency.isVatGstFlag()) {
                    if (agency.isVatAllCarrier() || mappedVatCarries.contains(costPlusRemunerationDTOS.get(index).getCostCenterId())) {
                        ExcelUtil.formulaWithFormat(row, 7, style, "G" + j + "*" + agency.getVatPercentage() + "/100", accountingSheet, workbook.createDataFormat());
                    } else {
                        ExcelUtil.setCellValueAsNumeric(row, style, 7, "0", workbook.createDataFormat());
                    }
                    ExcelUtil.formulaWithFormat(row, 8, style, "G" + j + "+H" + j, accountingSheet, workbook.createDataFormat());
                }
            }

            workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
            for (int startRow = 5, index = 0; startRow < (costPlusRemunerationDTOS.size() + 5); startRow++, index++) {
                Row row = accountingSheet.getRow(startRow);
                double ytdAmount = Double.parseDouble(ExcelUtil.getCellRawValue(workbook, row, 6));
                double vatAmount = 0d, totalAmount = 0d;
                if (agency.isVatGstFlag()) {
                    vatAmount = Double.parseDouble(ExcelUtil.getCellRawValue(workbook, row, 7));
                    totalAmount = Double.parseDouble(ExcelUtil.getCellRawValue(workbook, row, 8));
                }
                if (ytdAmount != 0.0) {
                    costPlusRemunerationDTOS.get(index).setYtdAmountCurrentMonth(ytdAmount);
                    costPlusRemunerationDTOS.get(index).setVatAmount(vatAmount);
                    costPlusRemunerationDTOS.get(index).setTotalAmount(totalAmount);
                    finalAmountMap.put(costPlusRemunerationDTOS.get(index).getCostCenterId(), costPlusRemunerationDTOS.get(index));
                    finalRemunerationDTOS.add(costPlusRemunerationDTOS.get(index));
                }
            }

            int acSheetLastRowNum = accountingSheet.getLastRowNum();
            Row totalRow = accountingSheet.createRow(acSheetLastRowNum);
            int endRow = costPlusRemunerationDTOS.size() - 1 + 5;
            totalLegalColAndAccSchSum.add(String.valueOf(endRow));
            ExcelUtil.formulaWithFormat(totalRow, 4, style, "SUM(E" + 6 + ":E" + endRow + ")", accountingSheet, workbook.createDataFormat());
            ExcelUtil.formulaWithFormat(totalRow, 5, style, "SUM(F" + 6 + ":F" + endRow + ")", accountingSheet, workbook.createDataFormat());
            ExcelUtil.formulaWithFormat(totalRow, 6, style, "SUM(G" + 6 + ":G" + endRow + ")", accountingSheet, workbook.createDataFormat());
            if (agency.isVatGstFlag()) {
                ExcelUtil.formulaWithFormat(totalRow, 7, style, "SUM(H" + 6 + ":H" + endRow + ")", accountingSheet, workbook.createDataFormat());
                ExcelUtil.formulaWithFormat(totalRow, 8, style, "SUM(I" + 6 + ":I" + endRow + ")", accountingSheet, workbook.createDataFormat());
            }

            CellStyle boldStyle = workbook.createCellStyle();
            CellStyle grayStyle = workbook.createCellStyle();
            CellStyle headerStyle = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            XSSFFont boldFont = workbook.createFont();

            CellStyle fillColorStyle = workbook.createCellStyle();
            fillColorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            fillColorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            List<String> accSheetTable = Arrays.asList("CIE", "SITE", "ACCOUNT", "NATURE", "COST CENTER", "PORT", "SPARE1", "SPARE2", "ICP", "Debit", "Credit");
            int endIndex = accountingSheet.getLastRowNum() + 2;
            int adiCarrierTableRowNum = endIndex;
            for (int startRow = adiCarrierTableRowNum, i = 0; startRow <= (finalRemunerationDTOS.size() + endIndex - 1); startRow++, i++) {
                boolean vatApplicableToCarrier = false;
                if (agency.isVatAllCarrier() || mappedVatCarries.contains(finalRemunerationDTOS.get(i).getCostCenterId()))
                    vatApplicableToCarrier = true;

                Row carrierCodeRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(carrierCodeRow, fillColorStyle, boldFont, 2, finalRemunerationDTOS.get(i).getCarrierCode());

                String totalOrCurrentMonthAmt = finalRemunerationDTOS.get(i).getYtdAmountCurrentMonth().toString();
                if (agency.isVatGstFlag() && vatApplicableToCarrier)
                    totalOrCurrentMonthAmt = finalRemunerationDTOS.get(i).getTotalAmount().toString();

                Row arTableRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(arTableRow, boldStyle, boldFont, 2, "Month M D+5 in AR: invoice entering");

                Row arTableHeadingEmptyRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row arTableHeadingRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                for (int startCol = 2, j = 0; startCol < accSheetTable.size() + 2; startCol++, j++) {
                    ExcelUtil.setCellValueWrapFillBlueColor(arTableHeadingEmptyRow, headerStyle, boldFont, startCol, "");
                    ExcelUtil.setCellValueWrapFillBlueColor(arTableHeadingRow, headerStyle, boldFont, startCol, accSheetTable.get(j));
                }
                ExcelUtil.borderBottom(arTableHeadingRow.getRowNum(), arTableHeadingRow.getRowNum(), 2, accSheetTable.size() + 1, accountingSheet);

                Row arTableVatRow = null;
                Row arTableDebitRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                if (agency.isVatGstFlag() && vatApplicableToCarrier) {
                    arTableVatRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                }
                Row arTableCreditRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row arTableTotalRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setExcelCellValue(arTableDebitRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(arTableDebitRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(arTableDebitRow, 4, "411100");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 5, "-");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 6, "-");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 7, "-");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 8, "-");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 9, "-");
                ExcelUtil.setExcelCellValue(arTableDebitRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(arTableDebitRow, style, 11, totalOrCurrentMonthAmt, workbook.createDataFormat());

                if (agency.isVatGstFlag() && vatApplicableToCarrier) {
                    ExcelUtil.setExcelCellValue(arTableVatRow, 2, agency.getCompany());
                    ExcelUtil.setExcelCellValue(arTableVatRow, 3, agency.getSite());
                    ExcelUtil.setExcelCellValue(arTableVatRow, 4, agency.getVatAccount());
                    ExcelUtil.setExcelCellValue(arTableVatRow, 5, "-");
                    ExcelUtil.setExcelCellValue(arTableVatRow, 6, "-");
                    ExcelUtil.setExcelCellValue(arTableVatRow, 7, "-");
                    ExcelUtil.setExcelCellValue(arTableVatRow, 8, "-");
                    ExcelUtil.setExcelCellValue(arTableVatRow, 9, "-");
                    ExcelUtil.setExcelCellValue(arTableVatRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                    ExcelUtil.setCellValueAsNumericAlignBottomRight(arTableVatRow, style, 12, finalRemunerationDTOS.get(i).getVatAmount().toString(), workbook.createDataFormat());
                }
                ExcelUtil.setExcelCellValue(arTableCreditRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(arTableCreditRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(arTableCreditRow, 4, "706200");
                ExcelUtil.setExcelCellValue(arTableCreditRow, 5, agency.getNatureCode());
                ExcelUtil.setExcelCellValue(arTableCreditRow, 6, finalRemunerationDTOS.get(i).getCostCenterCode());
                ExcelUtil.setExcelCellValue(arTableCreditRow, 7, "-");
                ExcelUtil.setExcelCellValue(arTableCreditRow, 8, "-");
                ExcelUtil.setExcelCellValue(arTableCreditRow, 9, "-");
                ExcelUtil.setExcelCellValue(arTableCreditRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(arTableCreditRow, style, 12, finalRemunerationDTOS.get(i).getYtdAmountCurrentMonth().toString(), workbook.createDataFormat());

                Cell debitCell = arTableDebitRow.getCell(11);
                Cell creditCell = arTableCreditRow.getCell(12);
                String debitColumnLetter = CellReference.convertNumToColString(debitCell.getColumnIndex());
                String creditColumnLetter = CellReference.convertNumToColString(creditCell.getColumnIndex());

                for (int startCol = 2, j = 0; startCol < accSheetTable.size(); startCol++, j++) {
                    ExcelUtil.setCellGrayColor(arTableTotalRow, startCol, font, grayStyle);
                }
                ExcelUtil.formulaWithFormatBoldWrap(arTableTotalRow, 11, "SUM(" + debitColumnLetter + (arTableDebitRow.getRowNum() + 1) + ":" + debitColumnLetter + (arTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());
                ExcelUtil.formulaWithFormatBoldWrap(arTableTotalRow, 12, "SUM(" + creditColumnLetter + (arTableDebitRow.getRowNum() + 1) + ":" + creditColumnLetter + (arTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());

                adiCarrierTableRowNum = adiCarrierTableRowNum + 1;
                Row arGlTableRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(arGlTableRow, boldStyle, boldFont, 2, "Month M D+5 in AR: Matching AR and GL with entering a AR receipt");

                Row arGlTableHeadingEmptyRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row arGlTableHeadingRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                for (int startCol = 2, j = 0; startCol < accSheetTable.size() + 2; startCol++, j++) {
                    ExcelUtil.setCellValueWrapFillBlueColor(arGlTableHeadingEmptyRow, headerStyle, boldFont, startCol, "");
                    ExcelUtil.setCellValueWrapFillBlueColor(arGlTableHeadingRow, headerStyle, boldFont, startCol, accSheetTable.get(j));
                }
                ExcelUtil.borderBottom(arGlTableHeadingRow.getRowNum(), arGlTableHeadingRow.getRowNum(), 2, accSheetTable.size() + 1, accountingSheet);

                Row arGlTableDebitRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row arGlTableCreditRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row arGlTableTotalRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 4, "478702");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 5, "-");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 6, "-");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 7, "-");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 8, "-");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 9, "-");
                ExcelUtil.setExcelCellValue(arGlTableDebitRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(arGlTableDebitRow, style, 11, totalOrCurrentMonthAmt, workbook.createDataFormat());

                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 4, "411100");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 5, "-");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 6, "-");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 7, "-");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 8, "-");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 9, "-");
                ExcelUtil.setExcelCellValue(arGlTableCreditRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(arGlTableCreditRow, style, 12, totalOrCurrentMonthAmt, workbook.createDataFormat());

                for (int startCol = 2, j = 0; startCol < accSheetTable.size(); startCol++, j++) {
                    ExcelUtil.setCellGrayColor(arGlTableTotalRow, startCol, font, grayStyle);
                }
                ExcelUtil.formulaWithFormatBoldWrap(arGlTableTotalRow, 11, "SUM(" + debitColumnLetter + (arGlTableDebitRow.getRowNum() + 1) + ":" + debitColumnLetter + (arGlTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());
                ExcelUtil.formulaWithFormatBoldWrap(arGlTableTotalRow, 12, "SUM(" + creditColumnLetter + (arGlTableDebitRow.getRowNum() + 1) + ":" + creditColumnLetter + (arGlTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());

                adiCarrierTableRowNum = adiCarrierTableRowNum + 1;
                Row glDATableRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(glDATableRow, boldStyle, boldFont, 2, "Month M D+5 in GL: DA entering (Before MGA rendering)");

                Row glDATableHeadingEmptyRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glDATableHeadingRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                for (int startCol = 2, j = 0; startCol < accSheetTable.size() + 2; startCol++, j++) {
                    ExcelUtil.setCellValueWrapFillBlueColor(glDATableHeadingEmptyRow, headerStyle, boldFont, startCol, "");
                    ExcelUtil.setCellValueWrapFillBlueColor(glDATableHeadingRow, headerStyle, boldFont, startCol, accSheetTable.get(j));
                }
                ExcelUtil.borderBottom(glDATableHeadingRow.getRowNum(), glDATableHeadingRow.getRowNum(), 2, accSheetTable.size() + 1, accountingSheet);

                Row glDATableVatRow = null;
                Row glDATableDebitRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                String totalOrMonthDebitAmtDA = finalRemunerationDTOS.get(i).getYtdAmountCurrentMonth().toString();
                String totalOrMonthCreditAmtDA = totalOrMonthDebitAmtDA;
                if (agency.isVatGstFlag() && vatApplicableToCarrier && agency.isVatDaLine()) {
                    glDATableVatRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                    totalOrMonthDebitAmtDA = finalRemunerationDTOS.get(i).getTotalAmount().toString();
                } else if (agency.isVatGstFlag() && vatApplicableToCarrier && !agency.isVatDaLine()) {
                    totalOrMonthDebitAmtDA = finalRemunerationDTOS.get(i).getTotalAmount().toString();
                    totalOrMonthCreditAmtDA = totalOrMonthDebitAmtDA;
                }

                Row glDATableCreditRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glDATableTotalRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 4, "467420");
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 5, agency.getNatureCode());
                int monthOrdinal = MonthEnum.valueOf(templateProcess.getMonth()).ordinal() + 1;
                String ccCodeOfMonthYear = "RCP" + templateProcess.getYear().toString().concat(String.valueOf(monthOrdinal).length() == 1 ? "0" + monthOrdinal : String.valueOf(monthOrdinal));
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 6, ccCodeOfMonthYear);
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 7, agency.getPort());
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 8, "-");
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 9, "-");
                ExcelUtil.setExcelCellValue(glDATableDebitRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glDATableDebitRow, style, 11, totalOrMonthDebitAmtDA, workbook.createDataFormat());

                if (agency.isVatGstFlag() && agency.isVatDaLine() && vatApplicableToCarrier) {
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 2, agency.getCompany());
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 3, agency.getSite());
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 4, agency.getVatAccount());
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 5, "-");
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 6, "-");
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 7, "-");
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 8, "-");
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 9, "-");
                    ExcelUtil.setExcelCellValue(glDATableVatRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                    ExcelUtil.setCellValueAsNumericAlignBottomRight(glDATableVatRow, style, 12, finalRemunerationDTOS.get(i).getVatAmount().toString(), workbook.createDataFormat());
                }

                ExcelUtil.setExcelCellValue(glDATableCreditRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 4, "478702");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 5, "-");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 6, "-");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 7, "-");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 8, "-");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 9, "-");
                ExcelUtil.setExcelCellValue(glDATableCreditRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glDATableCreditRow, style, 12, totalOrMonthCreditAmtDA, workbook.createDataFormat());

                for (int startCol = 2, j = 0; startCol < accSheetTable.size(); startCol++, j++) {
                    ExcelUtil.setCellGrayColor(glDATableTotalRow, startCol, font, grayStyle);
                }
                ExcelUtil.formulaWithFormatBoldWrap(glDATableTotalRow, 11, "SUM(" + debitColumnLetter + (glDATableDebitRow.getRowNum() + 1) + ":" + debitColumnLetter + (glDATableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());
                ExcelUtil.formulaWithFormatBoldWrap(glDATableTotalRow, 12, "SUM(" + creditColumnLetter + (glDATableDebitRow.getRowNum() + 1) + ":" + creditColumnLetter + (glDATableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());

                adiCarrierTableRowNum = adiCarrierTableRowNum + 1;
                Row glTableRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(glTableRow, boldStyle, boldFont, 2, "Month M+1 D+5 in GL: invoice to be established (Book an accrual based on last month actual - above balance)");

                Row glTableSheetRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glTableHeadingRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                for (int startCol = 2, j = 0; startCol < accSheetTable.size() + 2; startCol++, j++) {
                    ExcelUtil.setCellValueWrapFillBlueColor(glTableHeadingRow, headerStyle, boldFont, startCol, accSheetTable.get(j));
                    ExcelUtil.setCellValueWrapFillBlueColorLeftAlign(glTableSheetRow, headerStyle, boldFont, startCol, startCol == 2 ? "ADI Spreadsheet" : "");
                }
                ExcelUtil.borderBottom(glTableHeadingRow.getRowNum(), glTableHeadingRow.getRowNum(), 2, accSheetTable.size() + 1, accountingSheet);

                Row glTableVatRow = null;
                String totalOrMonthDebitAmtAccrual = finalRemunerationDTOS.get(i).getYtdAmountCurrentMonth().toString();
                String totalOrMonthCreditAmtAccrual = totalOrMonthDebitAmtAccrual;
                Row glTableDebitRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                if (agency.isVatGstFlag() && vatApplicableToCarrier && agency.isVatAccrual()) {
                    glTableVatRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                    totalOrMonthDebitAmtAccrual = finalRemunerationDTOS.get(i).getTotalAmount().toString();
                } else if (agency.isVatGstFlag() && vatApplicableToCarrier && !agency.isVatAccrual()) {
                    totalOrMonthDebitAmtAccrual = finalRemunerationDTOS.get(i).getTotalAmount().toString();
                    totalOrMonthCreditAmtAccrual = totalOrMonthDebitAmtAccrual;
                }
                Row glTableCreditRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glTableTotalRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setExcelCellValue(glTableDebitRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glTableDebitRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glTableDebitRow, 4, agency.getDebitAccount());
                ExcelUtil.setExcelCellValue(glTableDebitRow, 5, "-");
                ExcelUtil.setExcelCellValue(glTableDebitRow, 6, "-");
                ExcelUtil.setExcelCellValue(glTableDebitRow, 7, "-");
                ExcelUtil.setExcelCellValue(glTableDebitRow, 8, "-");
                ExcelUtil.setExcelCellValue(glTableDebitRow, 9, "-");
                ExcelUtil.setExcelCellValue(glTableDebitRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glTableDebitRow, style, 11, totalOrMonthDebitAmtAccrual, workbook.createDataFormat());

                if (agency.isVatGstFlag() && agency.isVatAccrual() && vatApplicableToCarrier) {
                    ExcelUtil.setExcelCellValue(glTableVatRow, 2, agency.getCompany());
                    ExcelUtil.setExcelCellValue(glTableVatRow, 3, agency.getSite());
                    ExcelUtil.setExcelCellValue(glTableVatRow, 4, agency.getVatAccount());
                    ExcelUtil.setExcelCellValue(glTableVatRow, 5, "-");
                    ExcelUtil.setExcelCellValue(glTableVatRow, 6, "-");
                    ExcelUtil.setExcelCellValue(glTableVatRow, 7, "-");
                    ExcelUtil.setExcelCellValue(glTableVatRow, 8, "-");
                    ExcelUtil.setExcelCellValue(glTableVatRow, 9, "-");
                    ExcelUtil.setExcelCellValue(glTableVatRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                    ExcelUtil.setCellValueAsNumericAlignBottomRight(glTableVatRow, style, 12, finalRemunerationDTOS.get(i).getVatAmount().toString(), workbook.createDataFormat());
                }

                ExcelUtil.setExcelCellValue(glTableCreditRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glTableCreditRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glTableCreditRow, 4, agency.getCreditAccount());
                ExcelUtil.setExcelCellValue(glTableCreditRow, 5, agency.getNatureCode());
                ExcelUtil.setExcelCellValue(glTableCreditRow, 6, finalRemunerationDTOS.get(i).getCostCenterCode());
                ExcelUtil.setExcelCellValue(glTableCreditRow, 7, "-");
                ExcelUtil.setExcelCellValue(glTableCreditRow, 8, "-");
                ExcelUtil.setExcelCellValue(glTableCreditRow, 9, "-");
                ExcelUtil.setExcelCellValue(glTableCreditRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glTableCreditRow, style, 12, totalOrMonthCreditAmtAccrual, workbook.createDataFormat());

                for (int startCol = 2, j = 0; startCol < accSheetTable.size(); startCol++, j++) {
                    ExcelUtil.setCellGrayColor(glTableTotalRow, startCol, font, grayStyle);
                }
                ExcelUtil.formulaWithFormatBoldWrap(glTableTotalRow, 11, "SUM(" + debitColumnLetter + (glTableDebitRow.getRowNum() + 1) + ":" + debitColumnLetter + (glTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());
                ExcelUtil.formulaWithFormatBoldWrap(glTableTotalRow, 12, "SUM(" + creditColumnLetter + (glTableDebitRow.getRowNum() + 1) + ":" + creditColumnLetter + (glTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());

                adiCarrierTableRowNum = adiCarrierTableRowNum + 1;
                Row glReverseTableRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setCellValueWithStyle(glReverseTableRow, boldStyle, boldFont, 2, "Month M+2 D+5 in GL: reversal of invoice to establish entry (Reversal of accrual)");

                Row glReverseTableSheetRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glReverseTableHeadingRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                for (int startCol = 2, j = 0; startCol < accSheetTable.size() + 2; startCol++, j++) {
                    ExcelUtil.setCellValueWrapFillBlueColor(glReverseTableHeadingRow, headerStyle, boldFont, startCol, accSheetTable.get(j));
                    ExcelUtil.setCellValueWrapFillBlueColorLeftAlign(glReverseTableSheetRow, headerStyle, boldFont, startCol, startCol == 2 ? "ADI Spreadsheet" : "");
                }
                ExcelUtil.borderBottom(glReverseTableHeadingRow.getRowNum(), glReverseTableHeadingRow.getRowNum(), 2, accSheetTable.size() + 1, accountingSheet);

                Row glReverseTableVatRow = null;
                Row glReverseTableDebitRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                if (agency.isVatGstFlag() && vatApplicableToCarrier && agency.isVatAccrual()) {
                    glReverseTableVatRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                }
                Row glReverseTableCreditRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                Row glReverseTableTotalRow = accountingSheet.createRow(adiCarrierTableRowNum++);
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 4, agency.getCreditAccount());
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 5, agency.getNatureCode());
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 6, finalRemunerationDTOS.get(i).getCostCenterCode());
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 7, "-");
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 8, "-");
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 9, "-");
                ExcelUtil.setExcelCellValue(glReverseTableDebitRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glReverseTableDebitRow, style, 11, totalOrMonthCreditAmtAccrual, workbook.createDataFormat());

                if (agency.isVatGstFlag() && agency.isVatAccrual() && vatApplicableToCarrier) {
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 2, agency.getCompany());
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 3, agency.getSite());
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 4, agency.getVatAccount());
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 5, "-");
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 6, "-");
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 7, "-");
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 8, "-");
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 9, "-");
                    ExcelUtil.setExcelCellValue(glReverseTableVatRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                    ExcelUtil.setCellValueAsNumericAlignBottomRight(glReverseTableVatRow, style, 11, finalRemunerationDTOS.get(i).getVatAmount().toString(), workbook.createDataFormat());
                }

                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 2, agency.getCompany());
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 3, agency.getSite());
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 4, agency.getDebitAccount());
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 5, "-");
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 6, "-");
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 7, "-");
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 8, "-");
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 9, "-");
                ExcelUtil.setExcelCellValue(glReverseTableCreditRow, 10, finalRemunerationDTOS.get(i).getCarrierCode());
                ExcelUtil.setCellValueAsNumericAlignBottomRight(glReverseTableCreditRow, style, 12, totalOrMonthDebitAmtAccrual, workbook.createDataFormat());

                for (int startCol = 2, j = 0; startCol < accSheetTable.size(); startCol++, j++) {
                    ExcelUtil.setCellGrayColor(glReverseTableTotalRow, startCol, font, grayStyle);
                }
                ExcelUtil.formulaWithFormatBoldWrap(glReverseTableTotalRow, 11, "SUM(" + debitColumnLetter + (glReverseTableDebitRow.getRowNum() + 1) + ":" + debitColumnLetter + (glReverseTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());
                ExcelUtil.formulaWithFormatBoldWrap(glReverseTableTotalRow, 12, "SUM(" + creditColumnLetter + (glReverseTableDebitRow.getRowNum() + 1) + ":" + creditColumnLetter + (glReverseTableCreditRow.getRowNum() + 1) + ")", font, grayStyle, workbook.createDataFormat());

                adiCarrierTableRowNum = accountingSheet.getLastRowNum() + 2;
            }
            workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
            accountingSheet.autoSizeColumn(4);
            accountingSheet.autoSizeColumn(5);
            accountingSheet.autoSizeColumn(6);
            accountingSheet.autoSizeColumn(11);
            accountingSheet.autoSizeColumn(12);

            for (Map.Entry<Long, CostPlusRemunerationDTO> entry : finalAmountMap.entrySet()) {
                CostPlusRemuneration costPlusRemuneration = modelMapper.map(entry.getValue(), CostPlusRemuneration.class);
                costPlusRemuneration.setCostPlusTemplate(templateProcess);
                costPlusRemuneration.setAmount(entry.getValue().getYtdAmountCurrentMonth());
                costPlusRemunerationRepository.save(costPlusRemuneration);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void generateADIJournal(Agency agency, CostPlusTemplateProcess
            templateProcess, Map<Long, CostPlusRemunerationDTO> finalAmountMap, User user, List<Long> mappedVatCarries) throws Exception {
        XSSFWorkbook journalWbInput = null;
        InputStream file = null, journalFile = null;
        ByteArrayOutputStream baos = null;
        try {
            String journalTemplatePath = envConfiguration.getAwsBucketPath().concat("/Templates/");
            /*boolean isExist = S3BucketUtil.fileExist("General Ledger More4ppas Journal ADI Template R11.xlsx", envConfiguration.getAwsBucketName(), journalTemplatePath);
            if (isExist) {
                file = S3BucketUtil.getFileInputStream("General Ledger More4ppas Journal ADI Template R11.xlsx", envConfiguration.getAwsBucketName(), journalTemplatePath);
            } else {
                throw new CostPlusException("ADI Journal template file does not exist");
            }*/
            file = new FileInputStream(journalTemplatePath + "General Ledger More4ppas Journal ADI Template R11.xlsx");
            String fileName = agency.getCode().concat(" - ").concat(agency.getName() + " " + templateProcess.getMonth() + " - " + templateProcess.getYear()).concat(" ADI Journal Actual").concat(".xlsx");
            journalWbInput = new XSSFWorkbook(file);
            fillADIJournalTemplate(journalWbInput, agency, templateProcess, finalAmountMap, mappedVatCarries);
            String journalPath = envConfiguration.getAwsBucketPath().concat("/Reports/ADIJOURNAL/");
            baos = new ByteArrayOutputStream();
            journalWbInput.write(baos);
            journalFile = new ByteArrayInputStream(baos.toByteArray());

            FileUtil.developmentUpload(baos, journalPath, fileName);
            //S3BucketUtil.upload(fileName, journalFile, envConfiguration.getAwsBucketName(), journalPath);
            AdiJournal adiJournal = adiJournalRepository.findByCostPlusTemplateId(templateProcess.getId());
            if (adiJournal == null) {
                adiJournal = new AdiJournal();
                adiJournal.setJournalType("Actual");
                adiJournal.setJournalFileName(fileName);
                adiJournal.setCreatedBy(user.getId());
                adiJournal.setCostPlusTemplate(templateProcess);
            } else {
                adiJournal.setUpdatedBy(user.getId());
            }
            adiJournalRepository.save(adiJournal);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CostPlusException("Failed to generate ADI Journal");
        } finally {
            file.close();
            Objects.requireNonNull(journalWbInput).close();
            Objects.requireNonNull(baos).close();
            Objects.requireNonNull(journalFile).close();
        }
    }

    private void fillADIJournalTemplate(XSSFWorkbook workbook, Agency agency, CostPlusTemplateProcess
            templateProcess, Map<Long, CostPlusRemunerationDTO> finalAmountMap, List<Long> mappedVatCarries) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String period = templateProcess.getMonth() + "-" + templateProcess.getYear();
        String category = "'DA_COMMISSIONS";
        String effDate = simpleDateFormat.format(new Date());
        String journalType = "I";
        String source = "Spreadsheet";
        String currency = agency.getFcCcy();
        String lineDesc = "COMMISSION COST+ " + period;
        CellStyle style = workbook.createCellStyle();

        List<CostPlusRemuneration> costPlusRemunerations = costPlusRemunerationRepository.findByCostPlusTemplateIdAndDeleted(templateProcess.getId(), false);
        String exampleActualSheet = workbook.getSheetName(0);
        Sheet actualSheet = workbook.getSheet(exampleActualSheet);
        int monthOrdinal = MonthEnum.valueOf(templateProcess.getMonth()).ordinal() + 1;
        String ccCodeOfMonthYear = "RCP" + templateProcess.getYear().toString().concat(String.valueOf(monthOrdinal).length() == 1 ? "0" + monthOrdinal : String.valueOf(monthOrdinal));
        int initialRow = 10, slNo = 1;
        for (CostPlusRemuneration costPlusRemuneration :
                costPlusRemunerations) {
            boolean vatApplicableToCarrier = false;
            String carrierCode = finalAmountMap.get(costPlusRemuneration.getCostCenterId()) != null ? finalAmountMap.get(costPlusRemuneration.getCostCenterId()).getCarrierCode() : "";
            String debitAccount = agency.getCompany() + "." + (agency.getSite() == null ? "N/A" : agency.getSite()) + "." + "467420" + "."
                    + agency.getNatureCode() + "." + ccCodeOfMonthYear + "." + agency.getPort() + "." +
                    carrierCode + "." + "-" + "." + "-";
            String creditAccount = agency.getCompany() + "." + (agency.getSite() == null ? "N/A" : agency.getSite()) + "." + "478702" + "."
                    + "-" + "." + "-" + "." + "-" + "." +
                    carrierCode + "." + "-" + "." + "-";
            String journalDesc = "Cost + Commission -" + carrierCode + "-" + templateProcess.getMonth() + "-" + templateProcess.getYear();

            String vatAccount = agency.getCompany() + "." + (agency.getSite() == null ? "N/A" : agency.getSite()) + "." + agency.getVatAccount() + "."
                    + "-" + "." + "-" + "." + "-" + "." +
                    carrierCode + "." + "-" + "." + "-";
            double vatAmount = finalAmountMap.get(costPlusRemuneration.getCostCenterId()).getVatAmount();
            double totalAmount = finalAmountMap.get(costPlusRemuneration.getCostCenterId()).getTotalAmount();

            if (agency.isVatAllCarrier() || mappedVatCarries.contains(costPlusRemuneration.getCostCenterId()))
                vatApplicableToCarrier = true;

            Cell batchDecCell = actualSheet.getRow(initialRow).getCell(11);
            batchDecCell.setCellValue(journalDesc);
            Cell journalNameCell = actualSheet.getRow(initialRow).getCell(12);
            journalNameCell.setCellValue(journalDesc);
            Cell journalDecCell = actualSheet.getRow(initialRow).getCell(13);
            journalDecCell.setCellValue(journalDesc);
            Cell journalRefCell = actualSheet.getRow(initialRow).getCell(14);
            journalRefCell.setCellValue(journalDesc);
            Cell periodCell = actualSheet.getRow(initialRow).getCell(15);
            periodCell.setCellValue(period);
            Cell categoryCell = actualSheet.getRow(initialRow).getCell(16);
            categoryCell.setCellValue(category);
            Cell effDateCell = actualSheet.getRow(initialRow).getCell(17);
            effDateCell.setCellValue(effDate);
            Cell journalTypeCell = actualSheet.getRow(initialRow).getCell(18);
            journalTypeCell.setCellValue(journalType);
            Cell sourceCell = actualSheet.getRow(initialRow).getCell(19);
            sourceCell.setCellValue(source);
            Cell currencyCell = actualSheet.getRow(initialRow).getCell(22);
            currencyCell.setCellValue(currency);
            Cell lineNoCell = actualSheet.getRow(initialRow).getCell(43);
            lineNoCell.setCellValue(slNo);

            Cell debitAccountCell = actualSheet.getRow(initialRow).getCell(44);
            debitAccountCell.setCellValue(debitAccount);
            String debitAmt = String.valueOf(Math.abs(costPlusRemuneration.getAmount()));
            String creditAmt = debitAmt;
            if (agency.isVatGstFlag() && vatApplicableToCarrier && agency.isVatDaLine()) {
                debitAmt = String.valueOf(Math.abs(totalAmount));
            } else if (agency.isVatGstFlag() && vatApplicableToCarrier && !agency.isVatDaLine()) {
                debitAmt = String.valueOf(Math.abs(totalAmount));
                creditAmt = debitAmt;
            }
            Cell debitCell = actualSheet.getRow(initialRow).getCell(45);
            ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, debitCell.getColumnIndex(), debitAmt, workbook.createDataFormat());
            Cell accDebitCell = actualSheet.getRow(initialRow).getCell(47);
            ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, accDebitCell.getColumnIndex(), debitAmt, workbook.createDataFormat());
            Cell debitLineDescCell = actualSheet.getRow(initialRow).getCell(50);
            debitLineDescCell.setCellValue(lineDesc);

            initialRow++;
            if (agency.isVatGstFlag() && vatApplicableToCarrier && agency.isVatDaLine()) {
                Cell batchDecCreditCell = actualSheet.getRow(initialRow).getCell(11);
                batchDecCreditCell.setCellValue(journalDesc);
                Cell journalNameCreditCell = actualSheet.getRow(initialRow).getCell(12);
                journalNameCreditCell.setCellValue(journalDesc);
                Cell journalDecCreditCell = actualSheet.getRow(initialRow).getCell(13);
                journalDecCreditCell.setCellValue(journalDesc);
                Cell journalRefCreditCell = actualSheet.getRow(initialRow).getCell(14);
                journalRefCreditCell.setCellValue(journalDesc);

                Cell creditAccountCell = actualSheet.getRow(initialRow).getCell(44);
                creditAccountCell.setCellValue(vatAccount);
                Cell creditCell = actualSheet.getRow(initialRow).getCell(46);
                ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, creditCell.getColumnIndex(), String.valueOf(Math.abs(vatAmount)), workbook.createDataFormat());
                Cell accCreditCell = actualSheet.getRow(initialRow).getCell(48);
                ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, accCreditCell.getColumnIndex(), String.valueOf(Math.abs(vatAmount)), workbook.createDataFormat());
                Cell creditLineDescCell = actualSheet.getRow(initialRow).getCell(50);
                creditLineDescCell.setCellValue(lineDesc);

                initialRow++;
            }
            Cell batchDecCreditCell = actualSheet.getRow(initialRow).getCell(11);
            batchDecCreditCell.setCellValue(journalDesc);
            Cell journalNameCreditCell = actualSheet.getRow(initialRow).getCell(12);
            journalNameCreditCell.setCellValue(journalDesc);
            Cell journalDecCreditCell = actualSheet.getRow(initialRow).getCell(13);
            journalDecCreditCell.setCellValue(journalDesc);
            Cell journalRefCreditCell = actualSheet.getRow(initialRow).getCell(14);
            journalRefCreditCell.setCellValue(journalDesc);

            Cell creditAccountCell = actualSheet.getRow(initialRow).getCell(44);
            creditAccountCell.setCellValue(creditAccount);
            Cell creditCell = actualSheet.getRow(initialRow).getCell(46);
            ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, creditCell.getColumnIndex(), creditAmt, workbook.createDataFormat());
            Cell accCreditCell = actualSheet.getRow(initialRow).getCell(48);
            ExcelUtil.setCellValueAsNumericGetCell(actualSheet.getRow(initialRow), style, accCreditCell.getColumnIndex(), creditAmt, workbook.createDataFormat());
            Cell creditLineDescCell = actualSheet.getRow(initialRow).getCell(50);
            creditLineDescCell.setCellValue(lineDesc);

            initialRow += 2;
            slNo++;
        }
        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
        actualSheet.autoSizeColumn(45);
        actualSheet.autoSizeColumn(46);
    }

}
