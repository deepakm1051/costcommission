package com.costcommission.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Grant;
import com.costcommission.config.EnvConfiguration;
import com.costcommission.entity.*;
import com.costcommission.repository.*;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestService {

    @Autowired
    private DesignationRepository designationRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SubCategoryRepository subCategoryRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private ClusterRepository clusterRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private EnvConfiguration envConfiguration;

    public void upload(MultipartFile file) {
        XSSFWorkbook workbook = null;
        try {
            workbook = new XSSFWorkbook(file.getInputStream());

            String desSheetName = workbook.getSheetName(0);
            Sheet desSheet = workbook.getSheet(desSheetName);
            List<Designation> designations = new ArrayList<>();
            for (Row row : desSheet) {
                if(row.getRowNum()>=1) {
                    Designation designation = new Designation();
                    designation.setName(getCellValue(row, 0).trim());
                    designations.add(designation);
                }
            }
            designationRepository.saveAll(designations);

            String roleSheetName = workbook.getSheetName(1);
            Sheet roleSheet = workbook.getSheet(roleSheetName);
            List<Role> roles = new ArrayList<>();
            for (Row row : roleSheet) {
                if(row.getRowNum()>=1) {
                    Role role = new Role();
                    role.setName(getCellValue(row, 0).trim());
                    roles.add(role);
                }
            }
            roleRepository.saveAll(roles);


            String catSheetName = workbook.getSheetName(2);
            Sheet catSheet = workbook.getSheet(catSheetName);
            List<Category> categories = new ArrayList<>();
            for (Row row : catSheet) {
                if(row.getRowNum()>=1) {
                    Category category = new Category();
                    category.setName(getCellValue(row, 0).trim());
                    category.setCategorySeq(Integer.valueOf(getCellValue(row, 1)));
                    category.setCreatedBy(1L);
                    categories.add(category);
                }
            }
            categoryRepository.saveAll(categories);

            String subCatSheetName = workbook.getSheetName(3);
            Sheet subCatSheet = workbook.getSheet(subCatSheetName);
            List<SubCategory> subCategories = new ArrayList<>();
            for (Row row : subCatSheet) {
                if(row.getRowNum()>=1) {
                    SubCategory subCategory = new SubCategory();
                    subCategory.setName(getCellValue(row, 0).trim());
                    subCategory.setSubCategorySeq(Integer.valueOf(getCellValue(row, 1)));
                    subCategory.setCategory(categoryRepository.findByName(getCellValue(row, 2)));
                    subCategory.setCreatedBy(1L);
                    subCategories.add(subCategory);
                }
            }
            subCategoryRepository.saveAll(subCategories);

            String ccR11SheetName = workbook.getSheetName(4);
            Sheet ccR11Sheet = workbook.getSheet(ccR11SheetName);
            List<CostCenter> costCenters = new ArrayList<>();
            for (Row row : ccR11Sheet) {
                if(row.getRowNum()>=1) {
                    CostCenter costCenter = new CostCenter();
                    costCenter.setCode(getCellValue(row, 0).trim());
                    costCenter.setName(getCellValue(row, 1).trim());
                    costCenter.setSubCategory(subCategoryRepository.findByName(getCellValue(row, 2).trim()));
                    costCenter.setSequence(Integer.parseInt(getCellValue(row, 3).trim()));
                    costCenter.setKeyAllocation(getCellValue(row, 4).trim());
                    costCenter.setCarrierCode(getCellValue(row, 5).trim());
                    costCenter.setCarrierName(getCellValue(row, 6).trim());
                    costCenter.setType("R11");
                    costCenter.setCreatedBy(1L);
                    costCenters.add(costCenter);
                }
            }
            costCenterRepository.saveAll(costCenters);

            String ccR12SheetName = workbook.getSheetName(5);
            Sheet ccR12Sheet = workbook.getSheet(ccR12SheetName);
            List<CostCenter> centers = new ArrayList<>();
            for (Row row : ccR12Sheet) {
                if(row.getRowNum()>=1) {
                    CostCenter costCenter = new CostCenter();
                    costCenter.setCode(getCellValue(row, 0).trim());
                    costCenter.setName(getCellValue(row, 1).trim());
                    costCenter.setSubCategory(subCategoryRepository.findByName(getCellValue(row, 2).trim()));
                    costCenter.setSequence(Integer.parseInt(getCellValue(row, 3).trim()));
                    costCenter.setKeyAllocation(getCellValue(row, 4).trim());
                    costCenter.setCarrierCode(getCellValue(row, 5).trim());
                    costCenter.setCarrierName(getCellValue(row, 6).trim());
                    costCenter.setType("R12");
                    costCenter.setCreatedBy(1L);
                    centers.add(costCenter);
                }
            }
            costCenterRepository.saveAll(centers);

            String regionSheetName = workbook.getSheetName(6);
            Sheet regionSheet = workbook.getSheet(regionSheetName);
            List<Region> regions = new ArrayList<>();
            for (Row row : regionSheet) {
                if(row.getRowNum()>=1) {
                    Region region = new Region();
                    region.setCode(getCellValue(row, 0).trim());
                    region.setName(getCellValue(row, 1).trim());
                    region.setCreatedBy(1L);
                    regions.add(region);
                }
            }
            regionRepository.saveAll(regions);

            String clusterSheetName = workbook.getSheetName(7);
            Sheet clusterSheet = workbook.getSheet(clusterSheetName);
            List<Cluster> clusters = new ArrayList<>();
            for (Row row : clusterSheet) {
                if(row.getRowNum()>=1) {
                    Cluster cluster = new Cluster();
                    cluster.setCode(getCellValue(row, 0).trim());
                    cluster.setName(getCellValue(row, 1).trim());
                    cluster.setRegion(regionRepository.findByName(getCellValue(row, 2).trim()));
                    cluster.setCreatedBy(1L);
                    clusters.add(cluster);
                }
            }
            clusterRepository.saveAll(clusters);

            String agencySheetName = workbook.getSheetName(8);
            Sheet agencySheet = workbook.getSheet(agencySheetName);
            List<Agency> agencies = new ArrayList<>();
            for (Row row : agencySheet) {
                if(row.getRowNum()>=1) {
                    Agency agency = new Agency();
                    agency.setCode(getCellValue(row, 0).trim());
                    agency.setName(getCellValue(row, 1).trim());
                    agency.setCluster(clusterRepository.findByName(getCellValue(row, 2).trim()));
                    agency.setCompany(getCellValue(row, 3).trim());
                    agency.setSite(getCellValue(row,4).trim());
                    agency.setNatureCode(getCellValue(row,5).trim());
                    agency.setCreditAccount(getCellValue(row, 6).trim());
                    agency.setDebitAccount(getCellValue(row, 7).trim());
                    agency.setLedgerName(ExcelUtil.getCellRawValue(workbook, row, 8));
                    agency.setPort(getCellValue(row, 9).trim());
                    agency.setSafEntityNumber(Long.valueOf(getCellValue(row, 10).trim()));
                    agency.setLegalEntityName(getCellValue(row, 11).trim());
                    agency.setOceanType(getCellValue(row, 12).trim());
                    agency.setLocation(getCellValue(row, 13).trim());
                    agency.setFcCcy(getCellValue(row, 14).trim());
                    agency.setMarkup(Integer.parseInt(getCellValue(row, 15).trim()));
                    agency.setHeadCountFlag(getCellValue(row, 16).trim().equalsIgnoreCase("Yes"));
                    agency.setVatGstFlag(getCellValue(row, 17).trim().equalsIgnoreCase("Yes"));
                    agency.setVatPercentage(Integer.parseInt(getCellValue(row, 18).trim()));
                    agency.setArTempRecipient(getCellValue(row, 19).trim());
                    agency.setAgencyTempRecipient(getCellValue(row, 20).trim());
                    agency.setManagerTempRecipient(getCellValue(row, 21).trim());
                    agency.setCreatedBy(1L);
                    agencies.add(agency);
                }
            }
            agencyRepository.saveAll(agencies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCellValue(Row row, int column) {
        Cell cell = row.getCell(column);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public void checkS3Access() throws Exception {
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_1).build();
        try {
            String bucket_name = envConfiguration.getAwsBucketName();
            AccessControlList acl = s3.getBucketAcl(bucket_name);
            List<Grant> grants = acl.getGrantsAsList();
            for (Grant grant : grants) {
                System.out.format("  %s: %s\n", grant.getGrantee().getIdentifier(),
                        grant.getPermission().toString());
            }
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            throw new Exception(e.getMessage());
        }
    }
}
