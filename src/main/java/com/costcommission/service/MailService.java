package com.costcommission.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Grant;
import com.costcommission.config.EnvConfiguration;
import com.costcommission.exception.CostPlusException;
import com.costcommission.util.FileUtil;
import com.costcommission.util.S3BucketUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.persistence.Tuple;
import java.io.InputStream;
import java.util.*;

@Service
public class MailService {

    @Autowired
    private EnvConfiguration envConfiguration;

    private Properties getProperties() {
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", envConfiguration.getSmtpPort());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", "false");
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.starttls.required", "false");
        return props;
    }

    private Properties getDevelopmentProp() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", envConfiguration.getEmailHost());
        props.put("mail.smtp.port", envConfiguration.getSmtpPort());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", envConfiguration.getEmailHost());
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        return props;
    }

    private MimeMessage getMimeMessage(Session session, String fromMail, String[] to, String[] cc, String subject, String htmlText, List<String> fileNames, String mailType) throws MessagingException {
        String consolidatedPath = envConfiguration.getEmailFileTempPath() + "/CONSOLIDATED REPORT/";
        String costPlusFilePath = envConfiguration.getEmailFileTempPath() + "/COSTPLUS/";
        String adiJournalFilePath = envConfiguration.getEmailFileTempPath() + "/ADIJOURNAL/";
        InternetAddress[] toRecipientAddress = new InternetAddress[to.length];
        int toCounter = 0;
        for (String recipient : to) {
            toRecipientAddress[toCounter] = new InternetAddress(recipient.trim());
            toCounter++;
        }
        InternetAddress[] ccRecipientAddress = null;
        if(cc!=null && cc.length!=0) {
            ccRecipientAddress = new InternetAddress[cc.length];
            int ccCounter = 0;
            for (String recipient : cc) {
                ccRecipientAddress[ccCounter] = new InternetAddress(recipient.trim());
                ccCounter++;
            }
        }
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(fromMail));
        msg.setRecipients(Message.RecipientType.TO, toRecipientAddress);
        if(ccRecipientAddress!=null) {
            msg.setRecipients(Message.RecipientType.CC, ccRecipientAddress);
        }
        msg.setSubject(subject);
        Multipart multipart = new MimeMultipart();
        if (fileNames != null && fileNames.size() > 0) {
            for (String filename : fileNames) {
                String filePath = "";
                if (mailType.equalsIgnoreCase("consolidated")) {
                    filePath = consolidatedPath;
                } else if (mailType.equalsIgnoreCase("AR")) {
                    filePath = costPlusFilePath;
                } else if (mailType.equalsIgnoreCase("agency")) {
                    if (filename.contains("ADI Journal")) {
                        filePath = adiJournalFilePath;
                    } else {
                        filePath = costPlusFilePath;
                    }
                }

                MimeBodyPart messageBody = new MimeBodyPart();
                DataSource source = new FileDataSource(filePath + filename);
                messageBody.setDataHandler(new DataHandler(source));
                filename = filename.split("#")[1];
                messageBody.setFileName(filename);
                multipart.addBodyPart(messageBody);
            }
        }
        BodyPart htmlBodyPart = new MimeBodyPart();
        htmlBodyPart.setContent(htmlText, "text/html");
        multipart.addBodyPart(htmlBodyPart);
        msg.setContent(multipart);
        return msg;
    }

    private void sendViaTransport(Session session, MimeMessage msg) throws MessagingException {
        Transport transport = session.getTransport();
        transport.connect(envConfiguration.getEmailHost(), envConfiguration.getEmailUserName(),
                envConfiguration.getEmailPassword());
        transport.sendMessage(msg, msg.getAllRecipients());
    }

    @Async
    public void sendMail(String[] to,String[] cc, String subject, String htmlText, List<String> fileName, String mailType) throws Exception {
        try {
            //Properties props = getProperties();
            Properties props = getDevelopmentProp();
            Session session = Session.getDefaultInstance(props);
            String fromMail = envConfiguration.getFromMail();
            MimeMessage msg = getMimeMessage(session, fromMail, to,cc, subject, htmlText, fileName, mailType);
            sendViaTransport(session, msg);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }

    public void saveS3FileInTempPath(String fileName, String s3FilePath, String tempPath, Long userId, String errorMessage) throws Exception {
        InputStream inputStream = null;
        try {
            boolean isExist = S3BucketUtil.fileExist(fileName, envConfiguration.getAwsBucketName(), s3FilePath);
            if (isExist) {
                inputStream = S3BucketUtil.getFileInputStream(fileName, envConfiguration.getAwsBucketName(), s3FilePath);
                FileUtil.createFolder(tempPath);
                fileName = userId+"#"+fileName;
                FileUtil.createFile(tempPath, fileName);
                FileUtil.saveFiles(inputStream, tempPath + "/" + fileName);
            } else {
                throw new CostPlusException(errorMessage);
            }
        }finally {
            Objects.requireNonNull(inputStream).close();
        }
    }

    public String[] getRecipientEmailIds(Tuple agencyEmailIds) {
        String emailIds = agencyEmailIds.get(0).toString();
        List<String> emailIdList = new ArrayList<>();
        if (emailIds.contains(",")) {
            String[] arEmailIds = emailIds.split(",");
            emailIdList.addAll(Arrays.asList(arEmailIds));
        } else {
            emailIdList.add(emailIds);
        }
        return emailIdList.stream().toArray(String[]::new);
    }
}
