package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.BkfDTO;
import com.costcommission.repository.CostCenterRepository;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class BkfMappingService {

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Autowired
    private EnvConfiguration envConfiguration;

    @Autowired
    private ExcelUtil excelUtil;

    public List<BkfDTO> getBkfMapping(String type) {
        List<BkfDTO> bkfDTOS = new ArrayList<>();
        List<Tuple> bkfMapping = costCenterRepository.findBkfMapping(type);
        for (Tuple tuple :
                bkfMapping) {
            BkfDTO bkfDTO = new BkfDTO();
            bkfDTO.setCostCenterCode((tuple.get("code").toString()));
            bkfDTO.setCostCenterName((tuple.get("name").toString()));
            bkfDTO.setCarrierName(tuple.get("carrierName")!=null ?tuple.get("carrierName").toString():"");
            bkfDTOS.add(bkfDTO);
        }
        return bkfDTOS;
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "BKF172Mapping.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createBKFMappingSheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createBKFMappingSheet(Sheet sheet, SXSSFWorkbook workbook) {
        int rowCount = 0;
        String[] type = {"OCEAN R11", "OCEAN R12", "Volume Report"};
        String[] header = {"Cost Center Code", "Cost Center Name", "Carrier Name"};
        Row rowIndex = sheet.createRow(rowCount);
        excelUtil.createHeaderCell(rowIndex, 1, type[0], sheet, workbook);
        rowCount++;
        Row rowR11Caption = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowR11Caption, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<BkfDTO> bkfR11DTOS = getBkfMapping("R11");
        List<BkfDTO> bkfR12DTOS = getBkfMapping("R12");
        for (BkfDTO bkfDTO : bkfR11DTOS) {
            rowCount = rowCount + 1;
            columnCount = 0;
            Row row = sheet.createRow(rowCount);
            excelUtil.createCell(row, columnCount, bkfDTO.getCostCenterCode(), sheet);
            excelUtil.createCell(row, ++columnCount, bkfDTO.getCostCenterName(), sheet);
            excelUtil.createCell(row, ++columnCount, bkfDTO.getCarrierName(), sheet);
        }
        int lastRowR11Index = sheet.getLastRowNum();
        Row rowR12Caption = sheet.createRow(lastRowR11Index+2);
        excelUtil.createHeaderCell(rowR12Caption, 1, type[1], sheet, workbook);
        int columnR12Count = 0;
        int lastRowR12Caption = sheet.getLastRowNum();
        for (BkfDTO bkfR12DTO : bkfR12DTOS) {
            Row row = sheet.createRow(lastRowR12Caption+1);
            excelUtil.createCell(row, columnR12Count, bkfR12DTO.getCostCenterCode(), sheet);
            excelUtil.createCell(row, ++columnR12Count, bkfR12DTO.getCostCenterName(), sheet);
            excelUtil.createCell(row, ++columnR12Count, bkfR12DTO.getCarrierName(), sheet);
        }
        int lastRowIndex = sheet.getLastRowNum();
        String[] volumeHeader = {"Type", "Report Type", "Country/Agency Code", "Carrier type"};
        String[] volumeTrans = {"Transhipment", "Transhipment Country text from column A1 to A26", "Ref Transhipment Country (parameter)"
                , "Ref Column E in Transhipment volume report"};
        String[] volumeImport = {"Import", "Ref POD/FPD Country text from column A1 to A26", "Ref POD/FPD Country (parameter)"
                , "Ref Column E in Import volume report"};
        String[] volumeExpLoaded = {"Export Loaded", "Ref POL Country text from column A1 to A26", "Ref POL Country (parameter)"
                , "Ref Column E in Export Loaded volume report"};
        String[] volumeExpBooked = {"Export Booked", "Ref Booking Agent Country text from column A1 to A26", "Ref Booking Agent Country Column"
                , "Ref Column E in Export Booked volume report"};

        Row volumeCaption = sheet.createRow(lastRowIndex + 2);
        excelUtil.createHeaderCell(volumeCaption, 1, type[2], sheet, workbook);
        Row volumeHead = sheet.createRow(lastRowIndex + 3);
        for (int i = 0; i < volumeHeader.length; i++) {
            excelUtil.createHeaderCell(volumeHead, i, volumeHeader[i], sheet, workbook);
        }
        Row volumeTran = sheet.createRow(lastRowIndex + 3);
        for (int i = 0; i < volumeTrans.length; i++) {
            excelUtil.createCell(volumeTran, i, volumeTrans[i], sheet);
        }
        Row volumeImp = sheet.createRow(lastRowIndex + 4);
        for (int i = 0; i < volumeImport.length; i++) {
            excelUtil.createCell(volumeImp, i, volumeImport[i], sheet);
        }
        Row volumeExLoad = sheet.createRow(lastRowIndex + 5);
        for (int i = 0; i < volumeExpLoaded.length; i++) {
            excelUtil.createCell(volumeExLoad, i, volumeExpLoaded[i], sheet);
        }
        Row volumeExBook = sheet.createRow(lastRowIndex + 6);
        for (int i = 0; i < volumeExpBooked.length; i++) {
            excelUtil.createCell(volumeExBook, i, volumeExpBooked[i], sheet);
        }
    }
}
