package com.costcommission.service;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.CostPlusDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.jpaspecification.ReleaseTemplateSpecification;
import com.costcommission.repository.AgencyRepository;
import com.costcommission.repository.CostPlusTemplateRepository;
import com.costcommission.repository.CostPlusWorkflowRepository;
import com.costcommission.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReleaseTemplateService {

    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;


    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        ReleaseTemplateSpecification spec = new ReleaseTemplateSpecification(specificationDTO);
        Page<CostPlusTemplateProcess> results = costPlusTemplateRepository.findAll(spec, pageable);
        List<CostPlusDTO> costPlusDTOS =new ArrayList<>();
        for (CostPlusTemplateProcess costPlusTemplateProcess:
             results) {
            Agency agency = agencyRepository.findAgencyById(costPlusTemplateProcess.getAgency().getId());

            CostPlusDTO costPlusDTO = modelMapper.map(costPlusTemplateProcess, CostPlusDTO.class);
            costPlusDTO.setReviewer(costPlusTemplateProcess.getReviewer()!=null?costPlusTemplateProcess.getReviewer().getFullName():"NA");
            costPlusDTO.setPreparer(costPlusTemplateProcess.getPreparer()!=null?costPlusTemplateProcess.getPreparer().getFullName():"NA");
            costPlusDTO.setApprover(costPlusTemplateProcess.getApprover()!=null?costPlusTemplateProcess.getApprover().getFullName():"NA");
            costPlusDTO.setDisplayStatus(CostPlusStatus.get(costPlusDTO.getStatus()).name().replaceAll("_"," ").toLowerCase());
            costPlusDTO.setAgencyName(agency.getName());
            costPlusDTOS.add(costPlusDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(costPlusDTOS);
        return paginationResponseDTO;
    }
}
