package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.costenum.MonthEnum;
import com.costcommission.dto.CostPlusTemplateDTO;
import com.costcommission.dto.CostPlusWorkflowDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.repository.*;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.ExcelUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.*;

@Service
public class ActualProcessService {

    @Autowired
    private CostPlusWorkflowRepository costPlusWorkflowRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private PeriodControlRepository periodControlRepository;
    @Autowired
    private VolumeReportRepository volumeReportRepository;
    @Autowired
    private TBVolumeTransactionRepository tbVolumeTransactionRepository;
    @Autowired
    private HeadCountRepository headCountRepository;
    @Autowired
    private CostPlusTemplateService costPlusTemplateService;
    @Autowired
    private ARInvoiceService arInvoiceService;
    @Autowired
    private MailService mailService;
    @Autowired
    private EmailTemplateUtil emailTemplateUtil;
    @Autowired
    private EnvConfiguration envConfiguration;

    Set<Long> inProgressCostPlus = new HashSet<>();

    public void concurrentUserAccess(CostPlusWorkflowDTO costPlusWorkflowDTO) throws Exception {
        if (inProgressCostPlus.contains(costPlusWorkflowDTO.getCostPlusTemplateId())) {
            throw new CostPlusException("Process is locked by other user");
        } else {
            inProgressCostPlus.add(costPlusWorkflowDTO.getCostPlusTemplateId());
        }
    }

    public void actualProcessConfirmation(CostPlusWorkflowDTO costPlusWorkflowDTO) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        try {
            User user = userRepository.findUserById(costPlusWorkflowDTO.getUserId());
            CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findCostPlusTemplateById(costPlusWorkflowDTO.getCostPlusTemplateId());
            CostPlusWorkflow costPlusWorkflow = modelMapper.map(costPlusWorkflowDTO, CostPlusWorkflow.class);

            int costPlusStatus = CostPlusStatus.OPEN.ordinal();
            if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("VOLUME")) {
                costPlusStatus = CostPlusStatus.VOLUME_CONFIRMED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Volume Confirmation is already done by other user");
                }
                Long count = tbVolumeTransactionRepository.countVolumeDetailsByYearMonthAgency(costPlusTemplateProcess.getYear(), costPlusTemplateProcess.getMonth(), costPlusTemplateProcess.getAgency().getId());
                if (count != 4) {
                    throw new CostPlusException("All 4 Volume Reports are not uploaded");
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("TBBYCC")) {
                costPlusStatus = CostPlusStatus.TB_BY_COST_CENTER_CONFIRMED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Trail balance Confirmation is already done by other user");
                }
                Long count = tbVolumeTransactionRepository.countTbByCCByYearMonthAgency(costPlusTemplateProcess.getYear(), costPlusTemplateProcess.getMonth(), costPlusTemplateProcess.getAgency().getId());
                if (count == 0) {
                    throw new CostPlusException("Trial balance is not uploaded");
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("HEADCOUNT")) {
                costPlusStatus = CostPlusStatus.HEAD_COUNT_CONFIRMED.ordinal();
                Long count = headCountRepository.countByAgencyIdAndMonthAndYear(costPlusTemplateProcess.getAgency().getId(), costPlusTemplateProcess.getMonth(), costPlusTemplateProcess.getYear());
                if (count == 0) {
                    throw new CostPlusException("Head Count is not submitted");
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("COSTPLUSCALCULATIONSTART")) {
                List<Tuple> workflowUser = volumeReportRepository.getWorkflowUsers(costPlusTemplateProcess.getAgency().getId());
                int reviewerCount = 0, approveCount = 0;
                for (Tuple tuple : workflowUser) {
                    if (tuple.get("name").toString().equalsIgnoreCase("Reviewer"))
                        reviewerCount = Integer.parseInt(tuple.get("countOfUser").toString());
                    if (tuple.get("name").toString().equalsIgnoreCase("Approver"))
                        approveCount = Integer.parseInt(tuple.get("countOfUser").toString());
                }
                if (reviewerCount == 0 || approveCount == 0) {
                    throw new CostPlusException("Reviewer or approver is not assigned to the selected agency. Contact admin to proceed with process.");
                }

                List<String> months = ExcelUtil.getMonthListExceptCurrent(costPlusTemplateProcess.getMonth());
                Long count = periodControlRepository.countPeriodControlOpenStatus(costPlusTemplateProcess.getAgency().getId(), months, costPlusTemplateProcess.getYear());
                if (count > 0) {
                    throw new CostPlusException("Previous month period control status is in open state. So, cost+ remuneration calculation is not allowed.");
                } else {
                    costPlusStatus = CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal();
                    if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                        throw new CostPlusException("Cost+ Remuneration calculation is already generated by other user");
                    }
                    costPlusTemplateService.generate(costPlusTemplateProcess, user);
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("SENDFORAPPROVAL")) {
                costPlusStatus = CostPlusStatus.SENT_FOR_APPROVAL.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Cost+ Remuneration Template is already sent for approval by other user");
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("REVIEWED")) {
                costPlusStatus = CostPlusStatus.REVIEWED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Cost+ Remuneration template is already reviewed by other user");
                } else if (costPlusTemplateProcess.getStatus() == CostPlusStatus.REJECTED.ordinal()) {
                    throw new CostPlusException("Cost+ Remuneration template is already Rejected by other user");
                } else
                    costPlusTemplateRepository.updateReviewerId(user.getId(), costPlusTemplateProcess.getId());
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("REJECTED")) {
                costPlusStatus = CostPlusStatus.REJECTED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Cost+ Remuneration template is already rejected by other user");
                } else {
                    List<String> reviewers = new ArrayList<>();
                    List<String> approvers = new ArrayList<>();
                    List<Tuple> approversAndReviewers = userRepository.findApproversAndReviewers(costPlusTemplateProcess.getAgency().getId());
                    for (Tuple tuple : approversAndReviewers) {
                        if (tuple.get("roleName").toString().equalsIgnoreCase("Reviewer"))
                            reviewers.add(tuple.get("userId").toString());
                        if (tuple.get("roleName").toString().equalsIgnoreCase("Approver"))
                            approvers.add(tuple.get("userId").toString());
                    }
                    if (costPlusTemplateProcess.getStatus() == CostPlusStatus.REVIEWED.ordinal() && reviewers.contains(user.getId().toString())) {
                        throw new CostPlusException("Cost+ Remuneration template is already Reviewed by other user");
                    } else if (costPlusTemplateProcess.getStatus() == CostPlusStatus.CLOSED.ordinal() && approvers.contains(user.getId().toString())) {
                        throw new CostPlusException("Cost+ Remuneration template is already Approved by other user");
                    }
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("ARINVOICE")) {
                costPlusStatus = CostPlusStatus.AR_INVOICE_REQUEST.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("AR Invoice Request is already requested by other user");
                } else {
                    costPlusTemplateRepository.updateARInvoiceRequestDate(costPlusTemplateProcess.getId());
                    arInvoiceService.sendARInvoiceMail(costPlusTemplateProcess.getId(), user.getId());
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("CLOSED")) {
                costPlusStatus = CostPlusStatus.CLOSED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Cost+ Remuneration template is already approved by other user");
                } else if (costPlusTemplateProcess.getStatus() == CostPlusStatus.REJECTED.ordinal()) {
                    throw new CostPlusException("Cost+ Remuneration template is already Rejected by other user");
                } else {
                    costPlusTemplateRepository.updateApprovedIdAndDate(user.getId(), costPlusTemplateProcess.getId());
                    PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(costPlusTemplateProcess.getAgency().getId(), costPlusTemplateProcess.getMonth(), costPlusTemplateProcess.getYear());
                    if (periodControl != null) {
                        periodControlRepository.updateStatus(false, periodControl.getId(), user.getId());
                    }
                }
            } else if (costPlusWorkflowDTO.getAction().equalsIgnoreCase("RELEASED")) {
                costPlusStatus = CostPlusStatus.RELEASED.ordinal();
                if (costPlusTemplateProcess.getStatus() == costPlusStatus) {
                    throw new CostPlusException("Cost+ Remuneration template is already released by other user");
                } else {
                    PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(costPlusTemplateProcess.getAgency().getId(), costPlusTemplateProcess.getMonth(), costPlusTemplateProcess.getYear());
                    if (periodControl != null) {
                        periodControlRepository.updateStatus(true, periodControl.getId(), user.getId());
                    }
                }
            }
            costPlusTemplateRepository.updateStatus(costPlusStatus, costPlusWorkflowDTO.getCostPlusTemplateId(), user.getId());
            costPlusWorkflow.setUser(user);
            costPlusWorkflow.setCreatedBy(user.getId());
            costPlusWorkflow.setUpdatedBy(user.getId());
            costPlusWorkflow.setCostPlusTemplateProcess(costPlusTemplateProcess);
            costPlusWorkflowRepository.save(costPlusWorkflow);
            inProgressCostPlus.remove(costPlusWorkflowDTO.getCostPlusTemplateId());
            if (costPlusStatus == CostPlusStatus.SENT_FOR_APPROVAL.ordinal() || costPlusStatus == CostPlusStatus.REVIEWED.ordinal() ||
                    costPlusStatus == CostPlusStatus.CLOSED.ordinal() || costPlusStatus == CostPlusStatus.REJECTED.ordinal() ||
                    costPlusStatus == CostPlusStatus.RELEASED.ordinal()) {
                sendWorkflowMailNotification(costPlusTemplateProcess, costPlusStatus, costPlusWorkflowDTO.getComments(), user);
            }
        } catch (
                Exception e) {
            inProgressCostPlus.remove(costPlusWorkflowDTO.getCostPlusTemplateId());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }


    private void sendWorkflowMailNotification(CostPlusTemplateProcess costPlusTemplateProcess, int costPlusStatus, String comments, User user) throws Exception {
        Agency agency = agencyRepository.findAgencyById(costPlusTemplateProcess.getAgency().getId());
        Map<String, Object> itemData = new HashMap<>();
        String subject = "", actionType = "",actionMessage="";
        List<String> toEmailIdList = new ArrayList<>();
        List<String> ccEmailIdList = new ArrayList<>();
        List<Tuple> workflowUser = userRepository.findWorkflowUsersMailId(agency.getId());
        for (Tuple tuple : workflowUser) {
            if (user.getId().equals(Long.valueOf(tuple.get("userId").toString())))
                ccEmailIdList.add(tuple.get("loginMail").toString());
            else
                toEmailIdList.add(tuple.get("loginMail").toString());
        }
        if (costPlusStatus == CostPlusStatus.SENT_FOR_APPROVAL.ordinal()) {
            subject = "Action Required - Send for review - " + agency.getName();
            actionType = "sent for review";
            actionMessage = "Please log-in to the system to review/reject.";
            comments = "Send for approval confirmed.";
        } else if (costPlusStatus == CostPlusStatus.REVIEWED.ordinal()) {
            subject = "Action Required - Reviewed and send for approval - " + agency.getName();
            actionType = "reviewed and send for approval";
            actionMessage = "Please log-in to the system to approve/reject.";
        } else if (costPlusStatus == CostPlusStatus.CLOSED.ordinal()) {
            subject = "Cost+ Remuneration template is Approved - " + agency.getName();
            actionType = "closed";
            actionMessage = "Kindly proceed to AR invoice request.";
        } else if (costPlusStatus == CostPlusStatus.REJECTED.ordinal()) {
            subject = "Action Required - Rejected - " + agency.getName();
            actionType = "rejected";
            actionMessage = "Kindly check the comments and re-submit for approval.";
        } else if (costPlusStatus == CostPlusStatus.RELEASED.ordinal()) {
            subject = "Action Required - Released - " + agency.getName();
            actionType = "released";
            actionMessage = "Kindly check the comments and re-submit for approval.";
        }
        itemData.put("userName", "All");
        itemData.put("agencyName", agency.getName());
        itemData.put("period", costPlusTemplateProcess.getMonth() + "-" + costPlusTemplateProcess.getYear());
        itemData.put("actionMessage", actionMessage);
        itemData.put("actionType", actionType);
        itemData.put("comments", comments);
        itemData.put("prodUrl", envConfiguration.getAppUrl());
        String[] finalToEmailIds = toEmailIdList.stream().toArray(String[]::new);
        String[] finalCcEmailIds = ccEmailIdList.stream().toArray(String[]::new);
        String process = emailTemplateUtil.getEmailTemplateData("workflow-template", itemData);
        mailService.sendMail(finalToEmailIds, finalCcEmailIds, subject, process, null, "workflow");
    }

    public CostPlusTemplateDTO getCostPlusTemplate(UserCurrentSessionDTO userCurrentSessionDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Agency agency = agencyRepository.findAgencyById(userCurrentSessionDTO.getAgencyId());
        CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
        if (costPlusTemplateProcess != null) {
            CostPlusTemplateDTO costPlusTemplateDTO = modelMapper.map(costPlusTemplateProcess, CostPlusTemplateDTO.class);
            costPlusTemplateDTO.setDisplayStatus(CostPlusStatus.get(costPlusTemplateDTO.getStatus()).name());
            costPlusTemplateDTO.setHeadCountApplicable(agency.isHeadCountFlag());
            return costPlusTemplateDTO;
        } else {
            PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
            if (periodControl != null && periodControl.isStatus()) {
                CostPlusTemplateProcess templateProcess = new CostPlusTemplateProcess();
                templateProcess.setPreparer(userRepository.findUserById(userCurrentSessionDTO.getUserId()));
                templateProcess.setAgency(agency);
                templateProcess.setMonth(userCurrentSessionDTO.getMonth());
                templateProcess.setYear(userCurrentSessionDTO.getYear());
                templateProcess.setStatus(CostPlusStatus.OPEN.ordinal());
                templateProcess.setCreatedBy(userCurrentSessionDTO.getUserId());
                costPlusTemplateRepository.save(templateProcess);
                CostPlusTemplateProcess savedCostPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
                CostPlusTemplateDTO costPlusTemplateDTO = modelMapper.map(savedCostPlusTemplateProcess, CostPlusTemplateDTO.class);
                costPlusTemplateDTO.setDisplayStatus(CostPlusStatus.get(costPlusTemplateDTO.getStatus()).name());
                costPlusTemplateDTO.setHeadCountApplicable(agency.isHeadCountFlag());
                return costPlusTemplateDTO;
            } else {
                throw new CostPlusException("Period control is not in open state for selected agency, month and year");
            }
        }
    }
}
