package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.ClusterDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Cluster;
import com.costcommission.entity.Region;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.ClusterSpecification;
import com.costcommission.repository.AgencyRepository;
import com.costcommission.repository.ClusterRepository;
import com.costcommission.repository.RegionRepository;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ClusterService {

    @Autowired
    private ClusterRepository clusterRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private EnvConfiguration envConfiguration;

    public ClusterDTO create(ClusterDTO clusterDTO) throws CostPlusException {
        Long countByCode = clusterRepository.countByCode(clusterDTO.getCode().trim().toLowerCase());
        Long countByName = clusterRepository.countByName(clusterDTO.getName().trim().toLowerCase());
        if (countByCode > 0) {
            throw new CostPlusException("Cluster code already exists");
        } else if (countByName > 0) {
            throw new CostPlusException("Cluster already exists");
        } else {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
            Cluster cluster = modelMapper.map(clusterDTO, Cluster.class);
            cluster.setRegion(regionRepository.findRegionById(clusterDTO.getRegionId()));
            return modelMapper.map(clusterRepository.save(cluster), ClusterDTO.class);
        }
    }

    public ClusterDTO update(ClusterDTO clusterDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Cluster savedCluster = clusterRepository.findByNameIgnoreCase(clusterDTO.getName());
        if (savedCluster != null && !savedCluster.getId().equals(clusterDTO.getId())) {
            throw new CostPlusException("Cluster already exists");
        } else {
            Cluster cluster = modelMapper.map(clusterDTO, Cluster.class);
            Region region = regionRepository.findRegionById(clusterDTO.getRegionId());
            cluster.setRegion(region);
            return modelMapper.map(clusterRepository.save(cluster), ClusterDTO.class);

        }
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {
            if (!status) {
                Long count = agencyRepository.countActiveCluster(id);
                if (count > 0) {
                    throw new CostPlusException("Cluster is in use.");
                } else {
                    return clusterRepository.enableOrDisable(status, userId, id);
                }
            } else {
                return clusterRepository.enableOrDisable(status, userId, id);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<ClusterDTO> allActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Cluster> clusters = clusterRepository.allActive();
            List<ClusterDTO> clusterDTOS = new ArrayList<>();
            for (Cluster cluster : clusters) {
                ClusterDTO clusterDTO = mapper.map(cluster, ClusterDTO.class);
                clusterDTO.setRegionId(cluster.getRegion().getId());
                clusterDTO.setRegionName(cluster.getRegion().getName());
                clusterDTOS.add(clusterDTO);
            }
            return clusterDTOS;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, int page, int size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        ClusterSpecification spec = new ClusterSpecification(specificationDTO);
        Page<Cluster> clusters = clusterRepository.findAll(spec, pageable);
        List<ClusterDTO> clusterDTOS = new ArrayList<>();
        for (Cluster cluster : clusters) {
            ClusterDTO clusterDTO = modelMapper.map(cluster, ClusterDTO.class);
            clusterDTO.setRegionId(cluster.getRegion().getId());
            clusterDTO.setRegionName(cluster.getRegion().getName());
            clusterDTOS.add(clusterDTO);
        }
        paginationResponseDTO.setTotalSize(clusters.getTotalElements());
        paginationResponseDTO.setData(clusterDTOS);
        return paginationResponseDTO;
    }

    public List<ClusterDTO> allIsActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Cluster> clusters = clusterRepository.findClusterByActive(true);
            List<ClusterDTO> clusterDTOS = new ArrayList<>();
            for (Cluster cluster : clusters) {
                ClusterDTO clusterDTO = mapper.map(cluster, ClusterDTO.class);
                clusterDTO.setRegionId(cluster.getRegion().getId());
                clusterDTO.setRegionName(cluster.getRegion().getName());
                clusterDTOS.add(clusterDTO);
            }
            return clusterDTOS;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "ClusterList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createClusterSheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createClusterSheet(SXSSFSheet sheet, SXSSFWorkbook workbook) throws Exception {
        int rowCount = 0;
        String[] header = {"Sl No", "Code", "Name", "Region"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        List<ClusterDTO> clusterDTOS = allActive();
        for (ClusterDTO clusterDTO : clusterDTOS) {
            rowCount = rowCount + 1;
            Row row = sheet.createRow(rowCount);
            int columnCount = 0;
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, clusterDTO.getCode(), sheet);
            excelUtil.createCell(row, ++columnCount, clusterDTO.getName(), sheet);
            excelUtil.createCell(row, ++columnCount, clusterDTO.getRegionName(), sheet);
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}

