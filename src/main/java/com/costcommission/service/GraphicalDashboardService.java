package com.costcommission.service;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.*;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.costcommission.costenum.CostPlusStatus.REVIEWED;

@Service
public class GraphicalDashboardService {

    @Autowired
    private TBVolumeTransactionRepository tbVolumeTransactionRepository;
    @Autowired
    private VolumeReportRepository volumeReportRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;


    public DashboardReportDTO getDetails(UserCurrentSessionDTO userCurrentSessionDTO) {
        DashboardReportDTO dashboardReportDTO = new DashboardReportDTO();
        String regionName = "";
        List<AgencyWiseReportDTO> agencyWiseReportDTOS = null;
        List<RegionWiseReportDTO> regionWiseReportDTOS = new ArrayList<>();
        List<GraphReportDTO> graphReportDTOS = new ArrayList<>();
        Map<String, RegionWiseReportDTO> regionWiseReportDTOMap = new HashMap<>();
        List<Tuple> tuples = regionRepository.getAgencyMappedToRegion();
        for (Tuple tuple : tuples) {
            Long agencyId = Long.valueOf(tuple.get("agencyId").toString());
            Long tbCount = tbVolumeTransactionRepository.countTbByCCByYearMonthAgency(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), agencyId);
            Long volumeCount = tbVolumeTransactionRepository.countVolumeDetailsByYearMonthAgency(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), agencyId);
            long uploadCount = tbCount + volumeCount;

            CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(agencyId, userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
            int costStatus = (costPlusTemplateProcess != null) ? costPlusTemplateProcess.getStatus() : 0;

            if (regionName.equalsIgnoreCase(tuple.get("region").toString())) {
                regionName = tuple.get("region").toString();

                AgencyWiseReportDTO agencyWiseReportDTO = new AgencyWiseReportDTO();
                agencyWiseReportDTO.setAgencyId(agencyId);
                agencyWiseReportDTO.setAgencyName(tuple.get("agency").toString());
                agencyWiseReportDTO.setUploaded(uploadCount == 5);
                agencyWiseReportDTO.setNotUploaded(uploadCount != 5);
                agencyWiseReportDTO.setReviewed(costStatus == REVIEWED.ordinal());
                agencyWiseReportDTO.setRejected(costStatus == CostPlusStatus.REJECTED.ordinal());
                agencyWiseReportDTO.setReleased(costStatus == CostPlusStatus.RELEASED.ordinal());
                agencyWiseReportDTO.setApproved(costStatus == CostPlusStatus.CLOSED.ordinal() || costStatus == CostPlusStatus.AR_INVOICE_REQUEST.ordinal());
                assert agencyWiseReportDTOS != null;
                agencyWiseReportDTOS.add(agencyWiseReportDTO);

                if (regionWiseReportDTOMap.containsKey(regionName)){
                     RegionWiseReportDTO regionWiseReportDTO = regionWiseReportDTOMap.get(regionName);
                    regionWiseReportDTO.setAgencyData(agencyWiseReportDTOS);
                    regionWiseReportDTOMap.replace(regionName, regionWiseReportDTO);
                }

            } else {
                regionName = tuple.get("region").toString();
                agencyWiseReportDTOS = new ArrayList<>();

                AgencyWiseReportDTO agencyWiseReportDTO = new AgencyWiseReportDTO();
                agencyWiseReportDTO.setAgencyId(agencyId);
                agencyWiseReportDTO.setAgencyName(tuple.get("agency").toString());
                agencyWiseReportDTO.setUploaded(uploadCount == 5);
                agencyWiseReportDTO.setNotUploaded(uploadCount != 5);
                agencyWiseReportDTO.setReviewed(costStatus == CostPlusStatus.REVIEWED.ordinal());
                agencyWiseReportDTO.setRejected(costStatus == CostPlusStatus.REJECTED.ordinal());
                agencyWiseReportDTO.setReleased(costStatus == CostPlusStatus.RELEASED.ordinal());
                agencyWiseReportDTO.setApproved(costStatus == CostPlusStatus.CLOSED.ordinal() || costStatus == CostPlusStatus.AR_INVOICE_REQUEST.ordinal());
                agencyWiseReportDTOS.add(agencyWiseReportDTO);

                RegionWiseReportDTO regionWiseReportDTO = new RegionWiseReportDTO();
                regionWiseReportDTO.setRegion(regionName);
                regionWiseReportDTO.setAgencyData(agencyWiseReportDTOS);

                if (regionWiseReportDTOMap.containsKey(regionName))
                    regionWiseReportDTOMap.replace(regionName, regionWiseReportDTO);
                else
                    regionWiseReportDTOMap.put(regionName, regionWiseReportDTO);
            }
        }


        for (Map.Entry<String, RegionWiseReportDTO> entry : regionWiseReportDTOMap.entrySet()) {
            long noOfUploaded = 0L, noOfNotUploaded = 0L, noOfReviewed = 0L, noOfRejected = 0L, noOfReleased = 0L, noOfApproved = 0L;
            GraphReportDTO graphReportDTO = new GraphReportDTO();
            for (AgencyWiseReportDTO agencyWiseReportDTO : entry.getValue().getAgencyData()) {
                if (agencyWiseReportDTO.isUploaded())
                    noOfUploaded++;
                if (agencyWiseReportDTO.isNotUploaded())
                    noOfNotUploaded++;
                if (agencyWiseReportDTO.isReviewed())
                    noOfReviewed++;
                if (agencyWiseReportDTO.isRejected())
                    noOfRejected++;
                if (agencyWiseReportDTO.isReleased())
                    noOfReleased++;
                if (agencyWiseReportDTO.isApproved())
                    noOfApproved++;
            }
            graphReportDTO.setRegion(entry.getKey());
            graphReportDTO.setNoOfUploaded(noOfUploaded);
            graphReportDTO.setNoOfNotUploaded(noOfNotUploaded);
            graphReportDTO.setNoOfReviewed(noOfReviewed);
            graphReportDTO.setNoOfRejected(noOfRejected);
            graphReportDTO.setNoOfReleased(noOfReleased);
            graphReportDTO.setNoOfApproved(noOfApproved);
            graphReportDTOS.add(graphReportDTO);
            regionWiseReportDTOS.add(entry.getValue());
        }

        dashboardReportDTO.setTableData(regionWiseReportDTOS);
        dashboardReportDTO.setGraphData(graphReportDTOS);

        return dashboardReportDTO;
    }
}
