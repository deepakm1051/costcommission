package com.costcommission.service;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.entity.Agency;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.entity.User;
import com.costcommission.exception.CostPlusException;
import com.costcommission.repository.AgencyRepository;
import com.costcommission.repository.CostPlusTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CostPlusTemplateService {

    @Autowired
    private CostPlusTemplateR11Service costPlusTemplateR11Service;
    @Autowired
    private CostPlusTemplateR12Service costPlusTemplateR12Service;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;


    public void generate(CostPlusTemplateProcess templateProcess, User user) throws Exception {
        Agency agency = agencyRepository.findAgencyById(templateProcess.getAgency().getId());
        if (agency.getOceanType().equalsIgnoreCase("R11") && !agency.isHeadCountFlag()) {
            costPlusTemplateR11Service.generate(templateProcess, agency, user);
        } else if (agency.getOceanType().equalsIgnoreCase("R12") && !agency.isHeadCountFlag()) {
            costPlusTemplateR12Service.generate(templateProcess, agency, user);
        } else {
            throw new CostPlusException("Development is in-progress for selected agency.");
        }
    }

    public List<CostPlusTemplateProcess> getActiveCostPlusTemplate() {
        List<Integer> costPlusStatus = new ArrayList<>();
        costPlusStatus.add(CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal());
        costPlusStatus.add(CostPlusStatus.SENT_FOR_APPROVAL.ordinal());
        costPlusStatus.add(CostPlusStatus.REVIEWED.ordinal());
        return costPlusTemplateRepository.findAllByStatusIn(costPlusStatus);
    }

}