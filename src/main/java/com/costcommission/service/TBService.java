package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.costenum.MonthEnum;
import com.costcommission.dto.*;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.AuditTrialSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.FileUtil;
import com.costcommission.util.S3BucketUtil;
import com.monitorjbl.xlsx.StreamingReader;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.ss.usermodel.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TBService {

    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private TBVolumeTransactionRepository tBvolumeTransactionRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private COARepository coaRepository;
    @Autowired
    private PeriodControlRepository periodControlRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private TBVolumeTransactionRepository tbVolumeTransactionRepository;

    public Object upload(MultipartFile file, UserCurrentSessionDTO userCurrentSessionDTO, String preparerConfirm) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Workbook workbook = null;
        try {
            Agency agency = agencyRepository.findAgencyById(userCurrentSessionDTO.getAgencyId());
            User user = userRepository.findUserById(userCurrentSessionDTO.getUserId());
            HashSet<String> costCenterCodeSet = costCenterRepository.getByCode();
            List<COA> natureCodeList = coaRepository.findAll();
            TBVolumeTransaction tbVolumeTransaction = modelMapper.map(userCurrentSessionDTO, TBVolumeTransaction.class);
            PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
            if (periodControl != null && !periodControl.isStatus()) {
                    throw new CostPlusException("Period control for selected agency, month and year is closed. TB BY CC file upload is not allowed.");
            }else{
                CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
                if(costPlusTemplateProcess !=null)
                if (costPlusTemplateProcess.getStatus() == CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal() || costPlusTemplateProcess.getStatus() == CostPlusStatus.SENT_FOR_APPROVAL.ordinal() || costPlusTemplateProcess.getStatus() == CostPlusStatus.REVIEWED.ordinal())
                    throw new CostPlusException("Cost+ remuneration template is in generated/approval stage. TB BY CC file upload is not allowed.");
            }
            workbook = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(file.getInputStream());
            Object inValidTB = validateCompanyCodeCostCentre(userCurrentSessionDTO, agency, costCenterCodeSet, tbVolumeTransaction, natureCodeList, workbook, preparerConfirm);
            if (inValidTB instanceof Map && preparerConfirm.equalsIgnoreCase("NO")) {
                return inValidTB;
            } else {
                String fileName = saveTbByCCFile(file, userCurrentSessionDTO, agency);
                tBvolumeTransactionRepository.disableTBByCCAudit(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId());
                tbVolumeTransaction.setFileType("TRIALBALANCE");
                tbVolumeTransaction.setFileName(fileName);
                tbVolumeTransaction.setDisplayFileName(file.getOriginalFilename());
                tbVolumeTransaction.setAgency(agency);
                tbVolumeTransaction.setCreatedBy(userCurrentSessionDTO.getUserId());
                tbVolumeTransaction.setUser(user);
                tBvolumeTransactionRepository.save(tbVolumeTransaction);
                return "success-" + inValidTB;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            file.getInputStream().close();
        }
    }

    private String saveTbByCCFile(MultipartFile file, UserCurrentSessionDTO userCurrentSessionDTO, Agency agency) throws IOException {
        String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/").concat("TRIALBALANCE/");
        String fileName = agency.getCompany() + "-" + agency.getCode() + "-" + userCurrentSessionDTO.getMonth() + "-" + userCurrentSessionDTO.getYear() + ".xlsx";
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        arrayOutputStream.write(file.getBytes());
        FileUtil.developmentUpload(arrayOutputStream, filePath, fileName);
        //S3BucketUtil.upload(fileName, file.getInputStream(), envConfiguration.getAwsBucketName(), filePath);
        return fileName;
    }

    private Object validateCompanyCodeCostCentre(UserCurrentSessionDTO userCurrentSessionDTO, Agency agency, HashSet<String> costCenterCodeSet, TBVolumeTransaction tbVolumeTransaction, List<COA> natureCodeList, Workbook workbook, String preparerConfirm) throws Exception {
        Map<String, Set<String>> inValidTBMap = new HashMap<>();
        Map<String, COA> coaMap = new HashMap<>();
        Set<String> invalidCc = new HashSet<>();
        Set<String> invalidNatureCode = new HashSet<>();
        Set<String> inActiveNatureCode = new HashSet<>();

        for (COA coa : natureCodeList) {
            coaMap.put(coa.getNatureCode(), coa);
        }

        String name = workbook.getSheetName(0);
        Sheet sheet = workbook.getSheet(name);
        String periodValue = "", tbtype = "";
        Integer totalRowCount = 0;
        if (sheet.getLastRowNum() == 0) {
            throw new CostPlusException("Trial Balance report format is incorrect.");
        }
        for (Row row : sheet) {
            if (row.getRowNum() == 1) {
                tbtype = getCellValue(row, 0).trim();
                if (tbtype.equalsIgnoreCase("Ledger name")) {
                    tbVolumeTransaction.setType("R12");
                }
            }
            if (row.getRowNum() >= 2 && row.getRowNum() < 19) {
                if (tbVolumeTransaction.getType() != null && tbVolumeTransaction.getType().equals("R12")) {
                    if (!agency.getOceanType().equals(tbVolumeTransaction.getType())) {
                        throw new CostPlusException("Selected agency ocean version and uploaded trial balance template mismatch.");
                    } else {
                        if (!getCellValue(row, 3).equalsIgnoreCase(agency.getCompany())) {
                            throw new CostPlusException("Invalid company code " + getCellValue(row, 3));
                        }
                        String natureCode = getCellValue(row, 7).trim().toUpperCase();
                        if (!coaMap.containsKey(natureCode)) {
                            invalidNatureCode.add(natureCode);
                        } else if (coaMap.containsKey(natureCode) && !coaMap.get(natureCode).isActive()) {
                            inActiveNatureCode.add(natureCode);
                        }
                        String ccCode = getCellValue(row, 5).trim().toUpperCase();
                        if (ccCode.startsWith("D")) {
                            if (!costCenterCodeSet.contains(ccCode)) {
                                invalidCc.add(getCellValue(row, 5).toUpperCase());
                            }
                        }
                    }
                    totalRowCount++;
                }
            }
            if (row.getRowNum() == 5) {
                periodValue = getCellValue(row, 2).trim();
            }

            if (row.getRowNum() == 17) {
                if (tbVolumeTransaction.getType() == null) {
                    tbtype = getCellValue(row, 0);
                    if (tbtype.equalsIgnoreCase("company")) {
                        tbVolumeTransaction.setType("R11");
                    }
                }

                if (tbVolumeTransaction.getType() != null && tbVolumeTransaction.getType().equals("R11")) {
                    if (periodValue.length() >= 7) {
                        String[] value = periodValue.split(":");
                        String[] trimMonthValue = value[1].split("-");
                        String tbByCCFileMonth = trimMonthValue[0].substring(trimMonthValue[0].length() - 3);
                        String tbByCCFileYear = periodValue.substring(periodValue.length() - 2);
                        validateMonthYear(userCurrentSessionDTO, agency, tbVolumeTransaction, tbByCCFileMonth, tbByCCFileYear);
                    } else {
                        throw new CostPlusException("Period Name is mandatory in file uploaded.");
                    }
                }
            }
            if (row.getRowNum() == 18) {
                if (tbVolumeTransaction.getType() == null) {
                    throw new CostPlusException("Invalid File");
                } else {
                    if (!agency.getOceanType().equals(tbVolumeTransaction.getType())) {
                        throw new CostPlusException("Selected agency ocean version and uploaded trial balance template mismatch.");
                    }
                }
            }
            if (row.getRowNum() >= 19 && row.getRowNum() <= sheet.getLastRowNum()) {
                if (tbVolumeTransaction.getType().equals("R11")) {
                    if (getCellValue(row, 0).equals("") && getCellValue(row, 4).equals("") && getCellValue(row, 6).equals("")) {
                        break;
                    }
                    if (!getCellValue(row, 0).equalsIgnoreCase(agency.getCompany())) {
                        throw new CostPlusException("Invalid company code " + getCellValue(row, 0));
                    }
                    String natureCode = getCellValue(row, 4).trim().toUpperCase();
                    if (!coaMap.containsKey(natureCode)) {
                        invalidNatureCode.add(getCellValue(row, 4).toUpperCase());
                    } else if (coaMap.containsKey(natureCode) && !coaMap.get(natureCode).isActive()) {
                        inActiveNatureCode.add(natureCode);
                    }
                    String ccCode = getCellValue(row, 6).trim().toUpperCase();
                    if (ccCode.startsWith("C")) {
                        if (!costCenterCodeSet.contains(ccCode)) {
                            invalidCc.add(getCellValue(row, 6).toUpperCase());
                        }
                    }

                    totalRowCount++;
                } else {
                    if (getCellValue(row, 0).startsWith("Overall")) {
                        break;
                    }
                    if (!getCellValue(row, 3).equalsIgnoreCase(agency.getCompany())) {
                        throw new CostPlusException("Invalid company code " + getCellValue(row, 3));
                    }
                    String natureCode = getCellValue(row, 7).trim().toUpperCase();
                    if (!coaMap.containsKey(natureCode)) {
                        invalidNatureCode.add(natureCode);
                    } else if (coaMap.containsKey(natureCode) && !coaMap.get(natureCode).isActive()) {
                        inActiveNatureCode.add(natureCode);
                    }
                    String ccCode = getCellValue(row, 5).trim().toUpperCase();
                    if (ccCode.startsWith("D")) {
                        if (!costCenterCodeSet.contains(ccCode)) {
                            invalidCc.add(getCellValue(row, 5).toUpperCase());
                        }
                    }
                    totalRowCount++;
                }
            }
        }

        if (tbVolumeTransaction.getType() != null && tbVolumeTransaction.getType().equals("R12")) {
            String sheetName = workbook.getSheetName(1);
            Sheet sheetTwo = workbook.getSheet(sheetName);
            for (Row row : sheetTwo) {
                if (row.getRowNum() == 2) {
                    periodValue = getCellValue(row, 1);
                }
                if (row.getRowNum() == 3) {
                    if (periodValue.length() >= 7) {
                        String tbByCCFileMonth = periodValue.trim().substring(5);
                        int parsedMonth = Integer.parseInt(tbByCCFileMonth);
                        if (parsedMonth <= 12) {
                            MonthEnum[] monthsEnum = MonthEnum.values();
                            for (MonthEnum monthEnum : monthsEnum) {
                                if (monthEnum.ordinal() + 1 == parsedMonth) {
                                    tbByCCFileMonth = monthEnum.name();
                                    break;
                                }
                            }
                        }
                        String tbByCCFileYear = periodValue.substring(0, 4);
                        validateMonthYear(userCurrentSessionDTO, agency, tbVolumeTransaction, tbByCCFileMonth, tbByCCFileYear);
                    } else {
                        throw new CostPlusException("Period Name is mandatory in file uploaded.");
                    }
                }
                if (row.getRowNum() >= 4) {
                    break;
                }
            }
        } else if (tbVolumeTransaction.getType() == null) {
            throw new CostPlusException("Invalid File");
        }

        if (invalidNatureCode.size() > 0 && inActiveNatureCode.size() > 0 && preparerConfirm.equalsIgnoreCase("NO")) {
            inValidTBMap.put("inValidNC", invalidNatureCode);
            inValidTBMap.put("inActiveNC", inActiveNatureCode);
            return inValidTBMap;
        } else if (invalidNatureCode.size() > 0 && preparerConfirm.equalsIgnoreCase("NO")) {
            inValidTBMap.put("inValidNC", invalidNatureCode);
            return inValidTBMap;
        } else if (inActiveNatureCode.size() > 0 && preparerConfirm.equalsIgnoreCase("NO")) {
            inValidTBMap.put("inActiveNC", inActiveNatureCode);
            return inValidTBMap;
        } else if (invalidCc.size() > 0 && preparerConfirm.equalsIgnoreCase("NO")) {
            inValidTBMap.put("inValidCC", invalidCc);
            return inValidTBMap;
        }
        return totalRowCount;
    }

    private void validateMonthYear(UserCurrentSessionDTO userCurrentSessionDTO, Agency agency, TBVolumeTransaction tbVolumeTransaction, String tbByCCFileMonth, String tbByCCFileYear) throws Exception {
        if (!userCurrentSessionDTO.getMonth().equalsIgnoreCase(tbByCCFileMonth) || !userCurrentSessionDTO.getYear().toString().endsWith(tbByCCFileYear))
            throw new CostPlusException("TBByCC File Period Name value is not matching with the month and year selection.");
    }

    public String getCellValue(Row row, int column) {
        Cell cell = row.getCell(column);
        DataFormatter formatter = new DataFormatter();
        return formatter.formatCellValue(cell);
    }

    public CellStyle getCellStyle(Row row, int column) {
        Cell cell = row.getCell(column);
        return cell.getCellStyle();
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        AuditTrialSpecification spec = new AuditTrialSpecification(specificationDTO);
        Page<TBVolumeTransaction> results = tBvolumeTransactionRepository.findAll(spec, pageable);
        List<TBVolumeTransactionDTO> tbVolumeTransactionDTOS = new ArrayList<>();
        for (TBVolumeTransaction tbVolumeTransaction : results) {
            TBVolumeTransactionDTO tbVolumeTransactionDTO = modelMapper.map(tbVolumeTransaction, TBVolumeTransactionDTO.class);
            tbVolumeTransactionDTO.setAgencyId(tbVolumeTransaction.getAgency() == null ? 0 : tbVolumeTransaction.getAgency().getId());
            tbVolumeTransactionDTO.setAgencyName(tbVolumeTransaction.getAgency() == null ? "" : tbVolumeTransaction.getAgency().getName());
            tbVolumeTransactionDTO.setDisplayCreatedBy(tbVolumeTransaction.getUser().getFullName() != null ? tbVolumeTransaction.getUser().getFullName() : "NA");
            tbVolumeTransactionDTOS.add(tbVolumeTransactionDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(tbVolumeTransactionDTOS);
        return paginationResponseDTO;
    }

    public TBVolumeTransactionDTO getTBDetails(UserCurrentSessionDTO userCurrentSessionDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        TBVolumeTransaction tbVolumeTransaction = tbVolumeTransactionRepository.findByAgencyIdAndMonthAndYearAndDeletedAndFileType(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear(), false, "TRIALBALANCE");
        if (tbVolumeTransaction == null)
            return null;
        else
            return modelMapper.map(tbVolumeTransaction, TBVolumeTransactionDTO.class);
    }

    public ResponseEntity<ByteArrayResource> export(String fileName) throws Exception {
        InputStream file = null;
        try {
            String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/TRIALBALANCE/");
            /*boolean isExist= S3BucketUtil.fileExist(fileName,envConfiguration.getAwsBucketName(),filePath);
            if(isExist){
                file =  S3BucketUtil.getFileInputStream(fileName,envConfiguration.getAwsBucketName(),filePath);
            }else{
                throw new CostPlusException("Trial Balance file does not exist");
            }*/
            file = new FileInputStream(filePath + fileName);
            byte[] bytes = IOUtils.toByteArray(file);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(bytes));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(file).close();
        }
    }


}
