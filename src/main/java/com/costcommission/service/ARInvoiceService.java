package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.*;
import com.costcommission.entity.Agency;
import com.costcommission.entity.AgencyVatCarriers;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.jpaspecification.ARInvoiceSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.ExcelUtil;
import com.costcommission.util.FileUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ARInvoiceService {

    @Autowired
    private CostPlusWorkflowRepository costPlusWorkflowRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private CostPlusRemunerationRepository costPlusRemunerationRepository;
    @Autowired
    private EmailTemplateUtil emailTemplateUtil;
    @Autowired
    private AgencyVatCarriersRepository agencyVatCarriersRepository;

    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO, Long agencyId) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        ARInvoiceSpecification spec = new ARInvoiceSpecification(specificationDTO, agencyId);
        Page<CostPlusTemplateProcess> results = costPlusTemplateRepository.findAll(spec, pageable);
        List<CostPlusTemplateDTO> costPlusDTOS = new ArrayList<>();
        for (CostPlusTemplateProcess costPlusTemplateProcess :
                results) {
            Agency agency = agencyRepository.findAgencyById(costPlusTemplateProcess.getAgency().getId());

            CostPlusTemplateDTO costPlusTemplateDTO = modelMapper.map(costPlusTemplateProcess, CostPlusTemplateDTO.class);
            costPlusTemplateDTO.setPreparer(costPlusTemplateProcess.getPreparer().getFullName());
            costPlusTemplateDTO.setAgencyName(agency.getName());
            costPlusDTOS.add(costPlusTemplateDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(costPlusDTOS);
        return paginationResponseDTO;
    }


    public void sendARInvoiceMail(Long costPlusId, Long userId) throws Exception {
        String s3FilePath = envConfiguration.getAwsBucketPath() + "/Reports/COSTPLUS/";
        String tempPath = envConfiguration.getEmailFileTempPath() + "/COSTPLUS/";
        String fileName = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            CostPlusTemplateProcess templateProcess = costPlusTemplateRepository.findCostPlusTemplateById(costPlusId);
            Date lastDayOfMonth = ExcelUtil.lastDayOfMonth(templateProcess.getMonth(),templateProcess.getYear());
            String effDate = simpleDateFormat.format(lastDayOfMonth);
            Agency agency = agencyRepository.findAgencyById(templateProcess.getAgency().getId());
            Tuple arTeamEmailIds = agencyRepository.findARTeamEmailIds(agency.getId());
            List<Tuple> arAmount = costPlusRemunerationRepository.getARInvoiceAmount(templateProcess.getMonth(), templateProcess.getYear(), templateProcess.getAgency().getId());
            List<ARInvoiceDTO> arInvoiceTOS = new ArrayList<>();
            List<Long> mappedVatCarries = new ArrayList<>();

            List<AgencyVatCarriers> savedAgencyVatCarriers = agencyVatCarriersRepository.findByAgencyIdAndDeleted(agency.getId(), false);
            if (savedAgencyVatCarriers.size() > 0) {
                for (AgencyVatCarriers vatCarriers : savedAgencyVatCarriers) {
                    mappedVatCarries.add(vatCarriers.getCostCenterId());
                }
            }

            for (Tuple tuple : arAmount) {
                ARInvoiceDTO arInvoiceDTO = new ARInvoiceDTO();
                arInvoiceDTO.setCarrierName(tuple.get(0).toString());
                arInvoiceDTO.setArAmount(ExcelUtil.doubleToCommaSeparatedValue(String.valueOf(Math.abs(Math.round(Double.parseDouble(tuple.get(1).toString()))))));
                arInvoiceTOS.add(arInvoiceDTO);

                if(agency.isVatGstFlag()){
                    if (agency.isVatAllCarrier() || mappedVatCarries.contains(Long.valueOf(tuple.get("costCentreId").toString()))) {
                        double vatAmount = (agency.getVatPercentage() / 100.0f) * Double.parseDouble(tuple.get(1).toString());
                        ARInvoiceDTO arVatInvoiceDTO = new ARInvoiceDTO();
                        arVatInvoiceDTO.setCarrierName(tuple.get(0).toString() + " (VAT- " + agency.getVatPercentage() + " %)");
                        arVatInvoiceDTO.setArAmount(ExcelUtil.doubleToCommaSeparatedValue(String.valueOf(Math.abs(Math.round(vatAmount)))));
                        arInvoiceTOS.add(arVatInvoiceDTO);
                    }
                }
            }
            fileName = templateProcess.getFileName();
            String subject = agency.getCode() + " Cost+ Remuneration Invoice details for " + templateProcess.getMonth() + " " + templateProcess.getYear();
            String[] finalArEmailIds = mailService.getRecipientEmailIds(arTeamEmailIds);

            FileUtil.saveEmailTempForDevelopment(tempPath, fileName, s3FilePath, userId, "Cost+ Remuneration template is not available");
            //mailService.saveS3FileInTempPath(fileName, s3FilePath, tempPath, userId, "Cost+ Remuneration template is not available.");

            Map<String, Object> itemData = new HashMap<>();
            itemData.put("monthYear", templateProcess.getMonth() + " " + templateProcess.getYear());
            itemData.put("dateMonthYear", effDate);
            itemData.put("arAmounts", arInvoiceTOS);
            String process = emailTemplateUtil.getEmailTemplateData("ar-template", itemData);
            mailService.sendMail(finalArEmailIds, null,subject, process, Collections.singletonList(userId + "#" + fileName), "AR");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            FileUtil.delete(tempPath, userId + "#" + fileName);
        }
    }

}


