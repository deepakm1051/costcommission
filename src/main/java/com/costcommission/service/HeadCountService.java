package com.costcommission.service;

import com.costcommission.dto.HeadCountDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.entity.*;
import com.costcommission.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HeadCountService {

    @Autowired
    private HeadCountRepository headCountRepository;
    @Autowired
    private PeriodControlRepository periodControlRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TBVolumeTransactionRepository tBvolumeTransactionRepository;


    public void create(List<HeadCountDTO> headCountDTO, String userId) {
        Long agencyId= null;
        ModelMapper modelMapper = new ModelMapper();
        for (HeadCountDTO headCount : headCountDTO) {
            Long count = headCountRepository.countByAgencyIdAndMonthAndYear(headCount.getAgencyId(), headCount.getMonth(), headCount.getYear());
            if (count > 0) {
                headCountRepository.update(headCount.getAgencyId(),
                        headCount.getMonth(), headCount.getYear(),
                        headCount.getAllCCBEmployees(), headCount.getInWhichContainerShip(),Long.valueOf(userId));
            } else {
                headCount.setCreatedBy(Long.valueOf(userId));
                headCountRepository.save(modelMapper.map(headCount, HeadCount.class));
            }
            agencyId= headCount.getAgencyId();
        }
        Agency agency = agencyRepository.findAgencyById(agencyId);
        User user = userRepository.findUserById(Long.parseLong(userId));

        TBVolumeTransaction tbVolumeTransaction = new TBVolumeTransaction();
        tbVolumeTransaction.setAgency(agency);
        tbVolumeTransaction.setType("HEADCOUNT");
        tbVolumeTransaction.setCreatedBy(Long.parseLong(userId));
        tbVolumeTransaction.setUser(user);
        tBvolumeTransactionRepository.save(tbVolumeTransaction);
    }

    public List<HeadCountDTO> get(UserCurrentSessionDTO currentSessionDTO) {
        ModelMapper modelMapper = new ModelMapper();
        List<HeadCount> headCountList = headCountRepository.findByAgencyIdAndYear(currentSessionDTO.getAgencyId(),currentSessionDTO.getYear());
        List<HeadCountDTO> headCountDTOS = new ArrayList<>();
        headCountList.forEach(headCounts -> {
            PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(headCounts.getAgencyId(), headCounts.getMonth(), headCounts.getYear());
            HeadCountDTO headCount = new HeadCountDTO();
            if (periodControl == null)
                headCount.setPeriod(false);
            else
                headCount.setPeriod(periodControl.isStatus());
            modelMapper.map(headCounts, headCount);
            headCountDTOS.add(headCount);
        });
        return headCountDTOS;
    }
}
