package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.costenum.MonthEnum;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.dto.VolumeReportDTO;
import com.costcommission.dto.VolumeTeuAndBOLDTO;
import com.costcommission.entity.*;
import com.costcommission.exception.CostPlusException;
import com.costcommission.repository.*;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.ExcelUtil;
import com.costcommission.util.FileUtil;
import com.costcommission.util.S3BucketUtil;
import com.monitorjbl.xlsx.StreamingReader;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.ss.usermodel.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Tuple;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

@Service
public class VolumeService {

    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TBVolumeTransactionRepository tBvolumeTransactionRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private VolumeReportRepository volumeReportRepository;
    @Autowired
    private PeriodControlRepository periodControlRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private EmailTemplateUtil emailTemplateUtil;

    public void upload(MultipartFile file, String volumeType, String mtdOrYtd, UserCurrentSessionDTO userCurrentSessionDTO) throws Exception {
        Workbook workbook = null;
        try {
            Map<String, Map<String, Double>> teuAndBlMap = new HashMap<>();
            Agency agency = agencyRepository.findAgencyById(userCurrentSessionDTO.getAgencyId());
            User user = userRepository.findUserById(userCurrentSessionDTO.getUserId());
            PeriodControl periodControl = periodControlRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
            if (periodControl != null && !periodControl.isStatus()) {
                throw new CostPlusException("Period control for selected agency, month and year is closed. Volume report file upload is not allowed.");
            } else {
                CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
                if(costPlusTemplateProcess !=null)
                if (costPlusTemplateProcess.getStatus() == CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal() || costPlusTemplateProcess.getStatus() == CostPlusStatus.SENT_FOR_APPROVAL.ordinal() || costPlusTemplateProcess.getStatus() == CostPlusStatus.REVIEWED.ordinal())
                    throw new CostPlusException("Cost+ remuneration template is in generated/approval stage. Volume report file upload is not allowed.");
            }
            workbook = StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(file.getInputStream());
            String name = workbook.getSheetName(0);
            Sheet sheet = workbook.getSheet(name);
            validateMTDOrYTDAndPeriodControl(userCurrentSessionDTO, mtdOrYtd);
            getTEUAndBL(teuAndBlMap, sheet);
            saveVolumeCalculation(file, mtdOrYtd, userCurrentSessionDTO, teuAndBlMap, agency, user, volumeType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            file.getInputStream().close();
        }
    }

    private void validateMTDOrYTDAndPeriodControl(UserCurrentSessionDTO userCurrentSessionDTO, String mtdOrYtd) throws CostPlusException {
        List<String> months = ExcelUtil.getMonthListExceptCurrent(userCurrentSessionDTO.getMonth());
        Long count = periodControlRepository.countPeriodControlOpenStatus(userCurrentSessionDTO.getAgencyId(), months, userCurrentSessionDTO.getYear());
        if (count > 0) {
            throw new CostPlusException("Previous month period control status is in open state. So, cost+ remuneration calculation is not allowed.");
        }
        List<VolumeReportDTO> volumeReportDTOS = getVolumeDetailsByMonthYearAgency(userCurrentSessionDTO);
        Set<String> mtdOrYtdSet = volumeReportDTOS.stream().map(VolumeReportDTO::getMtdOrYtd).collect(Collectors.toSet());
        if (mtdOrYtdSet.size() > 0)
            if (!mtdOrYtdSet.contains(mtdOrYtd)) {
                String mtdYtd = mtdOrYtd.equalsIgnoreCase("YTD") ? "MTD" : "YTD";
                throw new CostPlusException("Previous Volume report uploaded is of " + mtdYtd + ". Please upload " + mtdYtd + " report only for selected month and year. If " + mtdOrYtd + " report needs to be uploaded then remove uploaded details from below table");
            }
    }

    private String saveVolumeFile(MultipartFile file, UserCurrentSessionDTO userCurrentSessionDTO, Agency agency, String volumeType) throws IOException {
        String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/").concat("VOLUME/").concat(agency.getCode() + "-" + agency.getName() + "/");
        String fileName = agency.getCode() + "-" + userCurrentSessionDTO.getMonth() + "-" + userCurrentSessionDTO.getYear() + "-" + volumeType + ".xlsx";
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        arrayOutputStream.write(file.getBytes());
        FileUtil.developmentUpload(arrayOutputStream, filePath, fileName);
        //S3BucketUtil.upload(fileName, file.getInputStream(), envConfiguration.getAwsBucketName(), filePath);
        return fileName;
    }

    private void saveVolumeCalculation(MultipartFile file, String mtdOrYtd, UserCurrentSessionDTO userCurrentSessionDTO, Map<String, Map<String, Double>> teuAndBlMap, Agency agency, User user, String volumeType) throws Exception {
        List<VolumeReport> volumeReports = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        TBVolumeTransaction tbVolumeTransaction = modelMapper.map(userCurrentSessionDTO, TBVolumeTransaction.class);
        tbVolumeTransaction.setType(volumeType);
        for (Map.Entry<String, Map<String, Double>> entry : teuAndBlMap.entrySet()) {
            VolumeReport volumeReport = new VolumeReport();
            Long costCenterId = costCenterRepository.getIdByCarrierName(entry.getKey(), agency.getOceanType());
            if (costCenterId == null) {
                throw new CostPlusException("Carrier Name is not available in system. Please contact Admin -> " + entry.getKey());
            }
            volumeReport.setAgencyId(agency.getId());
            volumeReport.setMonth(userCurrentSessionDTO.getMonth());
            volumeReport.setYear(userCurrentSessionDTO.getYear());
            volumeReport.setCostCenterId(costCenterId);
            volumeReport.setCountOfBOLNumber(entry.getValue().get("COUNTOFBOL"));
            volumeReport.setSumOfTues(entry.getValue().get("SUMOFTEU"));
            volumeReport.setType(tbVolumeTransaction.getType());
            volumeReport.setCreatedBy(userCurrentSessionDTO.getUserId());
            volumeReport.setMtdOrYtd(mtdOrYtd);
            volumeReports.add(volumeReport);
        }
        String fileName = saveVolumeFile(file, userCurrentSessionDTO, agency, volumeType);
        if (mtdOrYtd.equalsIgnoreCase("YTD")) {
            List<String> months = ExcelUtil.getMonthList(userCurrentSessionDTO.getMonth());
            volumeReportRepository.disableSavedVolumeDetailsOfMonths(userCurrentSessionDTO.getYear(), months, userCurrentSessionDTO.getAgencyId(), tbVolumeTransaction.getType());
        } else {
            volumeReportRepository.disableSavedVolumeDetails(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId(), tbVolumeTransaction.getType());
        }
        volumeReportRepository.saveAll(volumeReports);
        tBvolumeTransactionRepository.disableVolumeAudit(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId(), tbVolumeTransaction.getType());
        tbVolumeTransaction.setFileType("VOLUME");
        tbVolumeTransaction.setFileName(fileName);
        tbVolumeTransaction.setDisplayFileName(file.getOriginalFilename());
        tbVolumeTransaction.setCreatedBy(userCurrentSessionDTO.getUserId());
        tbVolumeTransaction.setUser(user);
        tBvolumeTransactionRepository.save(tbVolumeTransaction);
        Long count = periodControlRepository.countByAgencyMonthYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
        if (!(count > 0)) {
            PeriodControl periodControl = new PeriodControl();
            periodControl.setMonth(userCurrentSessionDTO.getMonth());
            periodControl.setYear(userCurrentSessionDTO.getYear());
            periodControl.setAgency(agency);
            periodControl.setStatus(true);
            periodControl.setCreatedBy(userCurrentSessionDTO.getUserId());
            periodControlRepository.save(periodControl);
            CostPlusTemplateProcess costPlusTemplateProcess = new CostPlusTemplateProcess();
            costPlusTemplateProcess.setPreparer(userRepository.findUserById(userCurrentSessionDTO.getUserId()));
            costPlusTemplateProcess.setAgency(agency);
            costPlusTemplateProcess.setMonth(userCurrentSessionDTO.getMonth());
            costPlusTemplateProcess.setYear(userCurrentSessionDTO.getYear());
            costPlusTemplateProcess.setStatus(CostPlusStatus.OPEN.ordinal());
            costPlusTemplateProcess.setCreatedBy(user.getId());
            costPlusTemplateRepository.save(costPlusTemplateProcess);
            sendAdminMailNotification(agency);
        }
    }

    private void sendAdminMailNotification(Agency agency) throws Exception {
        List<Tuple> workflowUser = volumeReportRepository.getWorkflowUsers(agency.getId());
        int reviewerCount = 0, approveCount = 0;
        for (Tuple tuple : workflowUser) {
            if (tuple.get("name").toString().equalsIgnoreCase("Reviewer"))
                reviewerCount = Integer.parseInt(tuple.get("countOfUser").toString());
            if (tuple.get("name").toString().equalsIgnoreCase("Approver"))
                approveCount = Integer.parseInt(tuple.get("countOfUser").toString());
        }
        if (reviewerCount == 0 || approveCount == 0) {
            List<Tuple> adminMailIds = userRepository.findAdminMailIds();
            List<String> emailIdList = new ArrayList<>();
            for (Tuple tuple : adminMailIds) {
                emailIdList.addAll(Arrays.asList(mailService.getRecipientEmailIds(tuple)));
            }
            String subject = "Action Required - Assign Reviewer/Approver for - " + agency.getName();
            String[] finalArEmailIds = emailIdList.stream().toArray(String[]::new);
            String process = emailTemplateUtil.getEmailTemplateData("admin-template", null);
            mailService.sendMail(finalArEmailIds, null, subject, process, null, "admin");
        }
    }

    public void getTEUAndBL(Map<String, Map<String, Double>> teuAndBlMap, Sheet sheet) throws Exception {
        try {
            List<VolumeTeuAndBOLDTO> volumeTeuAndBOLDTOS = new ArrayList<>();
            Map<String, Integer> cellNumber = new HashMap<>();
            String[] headers = {"BOL NUMBER", "CARRIER", "TOTAL VOLUME IN TEU", "LOST SLOT"};
            List<String> headerList = Arrays.asList(headers);
            for (Row rowItr : sheet) {
                if (rowItr.getRowNum() == 0) {
                    for (int j = 0; j <= rowItr.getLastCellNum(); j++) {
                        String cellValue = getCellValue(rowItr, j).trim().toUpperCase();
                        if (headerList.contains(cellValue))
                            cellNumber.put(cellValue, j);
                    }
                }

                if (rowItr.getRowNum() >= 1 && rowItr.getRowNum() <= sheet.getLastRowNum()) {
                    VolumeTeuAndBOLDTO volumeTeuAndBOLDTO = new VolumeTeuAndBOLDTO();
                    for (Map.Entry<String, Integer> entry : cellNumber.entrySet()) {
                        switch (entry.getKey()) {
                            case "BOL NUMBER":
                                volumeTeuAndBOLDTO.setBOLNumber(getCellValue(rowItr, entry.getValue()));
                                break;
                            case "CARRIER":
                                volumeTeuAndBOLDTO.setCarrier(getCellValue(rowItr, entry.getValue()));
                                break;
                            case "TOTAL VOLUME IN TEU":
                                volumeTeuAndBOLDTO.setTotalVolumeInTEU(Double.parseDouble(getCellValue(rowItr, entry.getValue())));
                                break;
                            case "LOST SLOT":
                                String lostSlotAsString = getCellValue(rowItr, entry.getValue());
                                if (lostSlotAsString != null && !lostSlotAsString.equals("")) {
                                    volumeTeuAndBOLDTO.setLostSlot(Double.parseDouble(lostSlotAsString));
                                } else {
                                    volumeTeuAndBOLDTO.setLostSlot(0d);
                                }
                                break;
                        }
                    }

                    if (cellNumber.size() == 0) {
                        throw new CostPlusException("Uploaded Volume report format is incorrect.");
                    }

                    if (!volumeTeuAndBOLDTO.getCarrier().equalsIgnoreCase("0")) {
                        volumeTeuAndBOLDTOS.add(volumeTeuAndBOLDTO);
                    }
                }
            }
            if (sheet.getLastRowNum() != 0 && volumeTeuAndBOLDTOS.size() == 0) {
                throw new CostPlusException("Uploaded Volume report format is incorrect.");
            }

            for (VolumeTeuAndBOLDTO itr : volumeTeuAndBOLDTOS) {
                if (teuAndBlMap.containsKey(itr.getCarrier())) {
                    Map<String, Double> teuAndBol = teuAndBlMap.get(itr.getCarrier());
                    Double tue = teuAndBol.get("SUMOFTEU");
                    tue = tue + itr.getTotalVolumeInTEU() + itr.getLostSlot();
                    teuAndBol.put("SUMOFTEU", tue);
                    Double bol = teuAndBol.get("COUNTOFBOL");
                    teuAndBol.put("COUNTOFBOL", ++bol);
                    teuAndBlMap.put(itr.getCarrier(), teuAndBol);
                } else {
                    Map<String, Double> teuAndBol = new HashMap<>();
                    teuAndBol.put("SUMOFTEU", itr.getTotalVolumeInTEU() + itr.getLostSlot());
                    teuAndBol.put("COUNTOFBOL", Double.valueOf(1));
                    teuAndBlMap.put(itr.getCarrier(), teuAndBol);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getCellValue(Row row, int column) {
        Cell cell = row.getCell(column);
        String value = "0";
        if (cell != null) {
            if (cell.getCellType() == CellType.STRING) {
                value = cell.getStringCellValue();
            } else if (cell.getCellType() == CellType.NUMERIC) {
                value = "" + cell.getNumericCellValue();
            }
        }
        return value;
    }

    public List<VolumeReportDTO> getVolumeDetails(UserCurrentSessionDTO userCurrentSessionDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        List<VolumeReportDTO> volumeReportDTOS = new ArrayList<>();
        List<VolumeReport> volumeReports = volumeReportRepository.getVolumeDetailsByYearAgency(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getAgencyId());
        for (VolumeReport volumeReport : volumeReports) {
            VolumeReportDTO volumeReportDTO = modelMapper.map(volumeReport, VolumeReportDTO.class);
            volumeReportDTO.setCarrierName(costCenterRepository.getCarrierNameById(volumeReportDTO.getCostCenterId()));
            volumeReportDTOS.add(volumeReportDTO);
        }
        return volumeReportDTOS;
    }

    public List<VolumeReportDTO> getVolumeDetailsByMonthYearAgency(UserCurrentSessionDTO userCurrentSessionDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        List<VolumeReportDTO> volumeReportDTOS = new ArrayList<>();
        List<VolumeReport> volumeReports = volumeReportRepository.getVolumeDetailsByYearMonthAgency(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId());
        String volumeType = "";
        for (VolumeReport volumeReport : volumeReports) {
            if (!volumeReport.getType().equalsIgnoreCase(volumeType)) {
                VolumeReportDTO volumeReportDTO = modelMapper.map(volumeReport, VolumeReportDTO.class);
                volumeReportDTOS.add(volumeReportDTO);
            }
            volumeType = volumeReport.getType();
        }
        return volumeReportDTOS;
    }

    public void deActivateVolumeByMonthYearAgency(UserCurrentSessionDTO userCurrentSessionDTO) {
        List<VolumeReportDTO> volumeReportDTOS = getVolumeDetailsByMonthYearAgency(userCurrentSessionDTO);
        Set<String> mtdOrYtdSet = volumeReportDTOS.stream().map(VolumeReportDTO::getMtdOrYtd).collect(Collectors.toSet());
        if (mtdOrYtdSet.contains("YTD")) {
            List<String> months = ExcelUtil.getMonthList(userCurrentSessionDTO.getMonth());
            volumeReportRepository.activateSavedVolumeDetailsOfMonths(userCurrentSessionDTO.getYear(), months, userCurrentSessionDTO.getAgencyId());
            volumeReportRepository.disableAllSavedVolumeDetailsByMonthYear(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId());
        } else {
            volumeReportRepository.disableAllSavedVolumeDetailsByMonthYear(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId());
        }
        tBvolumeTransactionRepository.disableVolumeAuditByMonthYear(userCurrentSessionDTO.getYear(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getAgencyId());
    }

    public boolean deactivateStatus(UserCurrentSessionDTO userCurrentSessionDTO) {
        CostPlusTemplateProcess costPlusTemplateProcess = costPlusTemplateRepository.findByAgencyIdAndMonthAndYear(userCurrentSessionDTO.getAgencyId(), userCurrentSessionDTO.getMonth(), userCurrentSessionDTO.getYear());
        return costPlusTemplateProcess.getStatus() > CostPlusStatus.OPEN.ordinal() && costPlusTemplateProcess.getStatus() != CostPlusStatus.REJECTED.ordinal() && costPlusTemplateProcess.getStatus() != CostPlusStatus.RELEASED.ordinal();
    }

    public ResponseEntity<ByteArrayResource> export(String month, Integer year, Long agencyId) throws Exception {
        ByteArrayOutputStream outputStream = null;
        try {
            Map<String, InputStream> inputStreamMap = new HashMap<>();
            Agency agency = agencyRepository.findAgencyById(agencyId);
            String filePath = envConfiguration.getAwsBucketPath().concat("/Reports/").concat("VOLUME/").concat(agency.getCode() + "-" + agency.getName() + "/");
            List<TBVolumeTransaction> tbVolumeTransactions = tBvolumeTransactionRepository.findVolumeReportDetails(agencyId, month, year);
            String zipFileName = agency.getName() + "-" + month + "-" + year + " -TEUs.zip";
            for (TBVolumeTransaction tbVolumeTransaction : tbVolumeTransactions) {
                /*boolean isExist = S3BucketUtil.fileExist(tbVolumeTransaction.getFileName(), envConfiguration.getAwsBucketName(), filePath);
                if (isExist) {
                    InputStream file = S3BucketUtil.getFileInputStream(tbVolumeTransaction.getFileName(), envConfiguration.getAwsBucketName(), filePath);
                    inputStreamMap.put(tbVolumeTransaction.getDisplayFileName(), file);
                }*/
                InputStream file = new FileInputStream(filePath + tbVolumeTransaction.getFileName());
                inputStreamMap.put(tbVolumeTransaction.getDisplayFileName(), file);
            }
            outputStream = FileUtil.zipFileWithoutSaveLocal(inputStreamMap);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + zipFileName + "\"")
                    .body(new ByteArrayResource(outputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
}

