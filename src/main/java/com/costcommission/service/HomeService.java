package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.*;
import com.costcommission.entity.AdiJournal;
import com.costcommission.entity.Agency;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.jpaspecification.CostPlusReportSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.FileUtil;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HomeService {

    @Autowired
    private CostPlusWorkflowRepository costPlusWorkflowRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private ADIJournalRepository adiJournalRepository;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private MailService mailService;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private  EmailTemplateUtil emailTemplateUtil;


    public PaginationResponseDTO getByPaginationAndSpecification(String column, Integer page, Integer size, boolean asc, SpecificationDTO specificationDTO, Long agencyId, Integer year) {
       /* List<Tuple> agencies =userRepository.findAgencyMappedToUser(2L);
        List<Long> agencyIds = new ArrayList<>();
        for (Tuple tuple : agencies){
            agencyIds.add(Long.parseLong(tuple.get(0).toString()));
        }*/
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        CostPlusReportSpecification spec = new CostPlusReportSpecification(specificationDTO,agencyId,year,"home");
        Page<CostPlusTemplateProcess> results = costPlusTemplateRepository.findAll(spec, pageable);
        List<CostPlusDTO> costPlusDTOS =new ArrayList<>();
        List<String> reviewers = new ArrayList<>();
        List<String> approvers = new ArrayList<>();
        List<Tuple> approversAndReviewers=userRepository.findApproversAndReviewers(agencyId);
        for (Tuple tuple :approversAndReviewers){
            if(tuple.get("roleName").toString().equalsIgnoreCase("Reviewer"))
                reviewers.add(tuple.get("userName").toString());
            if(tuple.get("roleName").toString().equalsIgnoreCase("Approver"))
                approvers.add(tuple.get("userName").toString());
        }
        for (CostPlusTemplateProcess costPlusTemplateProcess:
             results) {
            Agency agency = agencyRepository.findAgencyById(costPlusTemplateProcess.getAgency().getId());
            Type listType = new TypeToken<List<CostPlusWorkflowDTO>>() {
            }.getType();
            List<CostPlusWorkflowDTO> costPlusWorkflows =modelMapper.map(costPlusWorkflowRepository.findByCostPlusTemplateProcessId(costPlusTemplateProcess.getId()), listType);
            CostPlusDTO costPlusDTO = modelMapper.map(costPlusTemplateProcess, CostPlusDTO.class);
            costPlusDTO.setReviewer(costPlusTemplateProcess.getReviewer()!=null?costPlusTemplateProcess.getReviewer().getFullName():"NA");
            costPlusDTO.setPreparer(costPlusTemplateProcess.getPreparer()!=null?costPlusTemplateProcess.getPreparer().getFullName():"NA");
            costPlusDTO.setApprover(costPlusTemplateProcess.getApprover()!=null?costPlusTemplateProcess.getApprover().getFullName():"NA");
            costPlusDTO.setDisplayStatus(CostPlusStatus.get(costPlusDTO.getStatus()).name().replaceAll("_"," ").toLowerCase());
            costPlusDTO.setAgencyName(agency.getName());
            costPlusDTO.setCostPlusWorkflows(costPlusWorkflows);
            costPlusDTO.setReviewers(reviewers);
            costPlusDTO.setApprovers(approvers);
            costPlusDTOS.add(costPlusDTO);
        }
        paginationResponseDTO.setTotalSize(results.getTotalElements());
        paginationResponseDTO.setData(costPlusDTOS);
        return paginationResponseDTO;
    }


    public void sendAgencyMail(Long costPlusId, Long userId) throws Exception {
        String costPlusFileName ="",adiJournalFileName="";
        String s3CostPlusFilePath = envConfiguration.getAwsBucketPath() + "/Reports/COSTPLUS/";
        String costPlusTempPath = envConfiguration.getEmailFileTempPath() + "/COSTPLUS/";
        String s3ADIJournalFilePath = envConfiguration.getAwsBucketPath() + "/Reports/ADIJOURNAL/";
        String journalTempPath = envConfiguration.getEmailFileTempPath() + "/ADIJOURNAL/";
        try {
            CostPlusTemplateProcess templateProcess = costPlusTemplateRepository.findCostPlusTemplateById(costPlusId);
            Tuple agencyEmailIds = agencyRepository.findAgencyEmailIds(templateProcess.getAgency().getId());
            AdiJournal adiJournal = adiJournalRepository.findByCostPlusTemplateId(templateProcess.getId());
            costPlusFileName = templateProcess.getFileName();
            adiJournalFileName = adiJournal.getJournalFileName();
            String subject = templateProcess.getAgency().getCode() + " Cost+ Remuneration template approved for " + templateProcess.getMonth() + " " + templateProcess.getYear();
            String[] finalArEmailIds = mailService.getRecipientEmailIds(agencyEmailIds);
            List<String> fileList = new ArrayList<>();
            fileList.add(userId+"#"+costPlusFileName);
            fileList.add(userId+"#"+adiJournalFileName);

            FileUtil.saveEmailTempForDevelopment(costPlusTempPath,costPlusFileName,s3CostPlusFilePath,userId,"Cost+ Remuneration template is not available");
            FileUtil.saveEmailTempForDevelopment(journalTempPath,adiJournalFileName,s3ADIJournalFilePath,userId,"ADI Journal is not available");

            //mailService.saveS3FileInTempPath(costPlusFileName, s3CostPlusFilePath, costPlusTempPath, userId,"Cost+ Remuneration template is not available.");
            //mailService.saveS3FileInTempPath(adiJournalFileName, s3ADIJournalFilePath, journalTempPath,userId,"ADI Journal is not available.");

            Map<String, Object> itemData = new HashMap<>();
            itemData.put("agencyName", templateProcess.getAgency().getName());
            String process = emailTemplateUtil.getEmailTemplateData("agency-template", itemData);
            mailService.sendMail(finalArEmailIds, null,subject, process, fileList, "agency");
        }catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            FileUtil.delete(costPlusTempPath, userId+"#"+costPlusFileName);
            FileUtil.delete(journalTempPath, userId+"#"+adiJournalFileName);
        }
    }
}
