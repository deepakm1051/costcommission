package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.AgencyDTO;
import com.costcommission.dto.CostCenterDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.Agency;
import com.costcommission.entity.Cluster;
import com.costcommission.entity.AgencyVatCarriers;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.AgencySpecification;
import com.costcommission.repository.*;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class AgencyService {

    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private AgencyRepository agencyRepository;
    @Autowired
    private AgencyVatCarriersRepository agencyVatCarriersRepository;
    @Autowired
    private ClusterRepository clusterRepository;
    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private UserRoleAgencyRepository userRoleAgencyRepository;
    @Autowired
    private EnvConfiguration envConfiguration;


    public AgencyDTO create(AgencyDTO agencyDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Long countByCode = agencyRepository.countCode(agencyDTO.getCode().toLowerCase());
        Long countByName = agencyRepository.countName(agencyDTO.getName().toLowerCase());
        if (countByCode > 0) {
            throw new CostPlusException("Agency code already exists");
        } else if (countByName > 0) {
            throw new CostPlusException("Agency already exists");
        } else {
            Agency agency = modelMapper.map(agencyDTO, Agency.class);
            Cluster cluster = clusterRepository.findById(agencyDTO.getClusterId());
            agency.setCluster(cluster);
            Agency savedAgency = agencyRepository.save(agency);
            if (!agencyDTO.isVatAllCarrier()) {
                if (agencyDTO.getCostCenterIds().size() > 0) {
                    List<AgencyVatCarriers> vatCarriersList = new ArrayList<>();
                    for (Long costCentreId : agencyDTO.getCostCenterIds()) {
                        AgencyVatCarriers agencyVatCarriers = new AgencyVatCarriers();
                        agencyVatCarriers.setAgency(savedAgency);
                        agencyVatCarriers.setCostCenterId(costCentreId);
                        vatCarriersList.add(agencyVatCarriers);
                    }
                    agencyVatCarriersRepository.saveAll(vatCarriersList);
                }
            }
            return modelMapper.map(savedAgency, AgencyDTO.class);
        }
    }

    public AgencyDTO update(AgencyDTO agencyDTO) throws CostPlusException {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Agency savedAgency = agencyRepository.findByNameIgnoreCase(agencyDTO.getName().toLowerCase());
        if (savedAgency != null && !savedAgency.getId().equals(agencyDTO.getId())) {
            throw new CostPlusException("Agency already exists");
        } else {
            List<Integer> costPlusStatus = new ArrayList<>();
            costPlusStatus.add(CostPlusStatus.COST_PLUS_CALCULATION_START.ordinal());
            costPlusStatus.add(CostPlusStatus.SENT_FOR_APPROVAL.ordinal());
            costPlusStatus.add(CostPlusStatus.REVIEWED.ordinal());
            List<CostPlusTemplateProcess> templateProcesses = costPlusTemplateRepository.findAllByStatusInAndAgencyId(costPlusStatus, agencyDTO.getId());
            if (templateProcesses.size() > 0 && savedAgency != null) {
                if (!savedAgency.getVatPercentage().toString().equals(agencyDTO.getVatPercentage().toString()) ||
                        (!savedAgency.getVatAccount().toString().equals(agencyDTO.getVatAccount().toString())) ||
                        (savedAgency.isVatDaLine()!=agencyDTO.isVatDaLine()) ||
                        (savedAgency.isVatAccrual()!=agencyDTO.isVatAccrual()) ||
                                (savedAgency.isVatAllCarrier()!=agencyDTO.isVatAllCarrier())) {
                    throw new CostPlusException("Cost+ remuneration template is in generated/approval stage. Vat related configuration update is not allowed.");
                }
            }
            Agency agency = modelMapper.map(agencyDTO, Agency.class);
            Cluster cluster = clusterRepository.findById(agencyDTO.getClusterId());
            agency.setCluster(cluster);
            AgencyDTO updatedAgency = modelMapper.map(agencyRepository.save(agency), AgencyDTO.class);
            updatedAgency.setCostCenterIds(new ArrayList<>());
            List<AgencyVatCarriers> savedAgencyVatCarriers = agencyVatCarriersRepository.findByAgencyId(agency.getId());
            List<AgencyVatCarriers> finalVatCarriers = new ArrayList<>();
            List<Long> carriers = new ArrayList<>(agencyDTO.getCostCenterIds());
            if (!agencyDTO.isVatAllCarrier()) {
                if (agencyDTO.getCostCenterIds().size() > 0) {
                    List<Long> savedCarriers = new ArrayList<>();
                    if (savedAgencyVatCarriers.size() > 0) {
                        for (AgencyVatCarriers vatCarriers : savedAgencyVatCarriers) {
                            if (!carriers.contains(vatCarriers.getCostCenterId())) {
                                vatCarriers.setDeleted(true);
                                finalVatCarriers.add(vatCarriers);
                            }
                            if (!vatCarriers.isDeleted())
                                savedCarriers.add(vatCarriers.getCostCenterId());
                        }
                        carriers.removeAll(savedCarriers);
                        for (Long newCcId : carriers) {
                            AgencyVatCarriers agencyVatCarriers = new AgencyVatCarriers();
                            agencyVatCarriers.setAgency(savedAgency);
                            agencyVatCarriers.setCostCenterId(newCcId);
                            finalVatCarriers.add(agencyVatCarriers);
                        }
                        agencyVatCarriersRepository.saveAll(finalVatCarriers);
                    } else {
                        List<AgencyVatCarriers> vatCarriersList = new ArrayList<>();
                        for (Long costCentreId : agencyDTO.getCostCenterIds()) {
                            AgencyVatCarriers agencyVatCarriers = new AgencyVatCarriers();
                            agencyVatCarriers.setAgency(savedAgency);
                            agencyVatCarriers.setCostCenterId(costCentreId);
                            vatCarriersList.add(agencyVatCarriers);
                        }
                        agencyVatCarriersRepository.saveAll(vatCarriersList);
                    }
                }
            } else {
                for (AgencyVatCarriers vatCarriers : savedAgencyVatCarriers) {
                    vatCarriers.setDeleted(true);
                    finalVatCarriers.add(vatCarriers);
                }
                agencyVatCarriersRepository.saveAll(finalVatCarriers);
            }
            return updatedAgency;
        }
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {

            if (!status) {
                Long count = userRoleAgencyRepository.countActiveAgency(id);
                if (count > 0) {
                    throw new CostPlusException("Agency is in use.");
                } else {
                    return agencyRepository.enableOrDisable(status, userId, id);
                }
            } else {
                return agencyRepository.enableOrDisable(status, userId, id);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<AgencyDTO> allActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Agency> agencies = agencyRepository.allActive();
            List<AgencyDTO> agencyDTOS = new ArrayList<>();
            for (Agency agency : agencies) {
                List<Long> ccIds = new ArrayList<>();
                List<String> displayCarriers = new ArrayList<>();
                for (AgencyVatCarriers agencyVatCarriers : agency.getVatCarriers()) {
                    if (!agencyVatCarriers.isDeleted())
                        ccIds.add(agencyVatCarriers.getCostCenterId());
                }
                if (ccIds.size() > 0) {
                    List<Tuple> carrierCodeList = costCenterRepository.findCarrierCodeByCostCentreId(ccIds);
                    for (Tuple tuple : carrierCodeList) {
                        displayCarriers.add(tuple.get("carrierCode").toString());
                    }
                }
                AgencyDTO agencyDTO = mapper.map(agency, AgencyDTO.class);
                agencyDTO.setClusterId(agency.getCluster().getId());
                agencyDTO.setClusterName(agency.getCluster().getName());
                agencyDTO.setDisplayCarriers(displayCarriers);
                agencyDTOS.add(agencyDTO);
            }
            return agencyDTOS;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public PaginationResponseDTO getByPaginationAndSpecification(String column, int page, int size, boolean asc, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        AgencySpecification spec = new AgencySpecification(specificationDTO);
        Page<Agency> agencies = agencyRepository.findAll(spec, pageable);
        List<AgencyDTO> agencyDTOS = new ArrayList<>();
        for (Agency agency : agencies) {
            List<Long> ccIds = new ArrayList<>();
            List<String> displayCarriers = new ArrayList<>();
            for (AgencyVatCarriers agencyVatCarriers : agency.getVatCarriers()) {
                if (!agencyVatCarriers.isDeleted())
                    ccIds.add(agencyVatCarriers.getCostCenterId());
            }
            if (ccIds.size() > 0) {
                List<Tuple> carrierCodeList = costCenterRepository.findCarrierCodeByCostCentreId(ccIds);
                for (Tuple tuple : carrierCodeList) {
                    displayCarriers.add(tuple.get("carrierCode").toString());
                }
            }
            AgencyDTO agencyDTO = modelMapper.map(agency, AgencyDTO.class);
            agencyDTO.setClusterId(agency.getCluster().getId());
            agencyDTO.setClusterName(agency.getCluster().getName());
            agencyDTO.setCostCenterIds(ccIds);
            agencyDTO.setDisplayCarriers(displayCarriers);
            agencyDTOS.add(agencyDTO);
        }
        paginationResponseDTO.setTotalSize(agencies.getTotalElements());
        paginationResponseDTO.setData(agencyDTOS);
        return paginationResponseDTO;
    }

    public List<AgencyDTO> allIsActive() throws Exception {
        try {
            ModelMapper mapper = new ModelMapper();
            mapper.getConfiguration().setAmbiguityIgnored(true);
            List<Agency> agencies = agencyRepository.findClusterByActive(true);
            List<AgencyDTO> agencyDTOS = new ArrayList<>();
            for (Agency agency : agencies) {

                AgencyDTO agencyDTO = mapper.map(agency, AgencyDTO.class);
                agencyDTO.setClusterId(agency.getCluster().getId());
                agencyDTO.setClusterName(agency.getCluster().getName());
                agencyDTOS.add(agencyDTO);
            }
            return agencyDTOS;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public ResponseEntity<ByteArrayResource> export() throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "AgencyList.xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createAgencySheet(sheet, workbook);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createAgencySheet(SXSSFSheet sheet, SXSSFWorkbook workbook) throws Exception {
        int rowCount = 0;
        String[] header = {"Sl No", "Code", "Name", "Cluster", "Ocean Version", "SAFRAN Entity/OS Entity Number", "Legal Entity Name", "Template Name", "Mark up(%)", "Location"
                , "Head Count", "Ledger Name", "Company", "Site", "Debit Account", "Credit Account", "Nature Code", "VAT/GST applicable", "VAT/GST(%)", "VAT Account",
                "VAT is separated from DA line(Actual)", "VAT Applicable to Accrual", "VAT Applicable to all carrier", "Carrier", "Functional Currency"
                , "AR Email Template Recipient", "Manager Email Template Recipient", "Agency Email Template Recipient"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<AgencyDTO> agencyDTOS = allActive();
        for (AgencyDTO agencyDTO : agencyDTOS) {
            rowCount = rowCount + 1;
            columnCount = 0;
            Row row = sheet.createRow(rowCount);
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getCode(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getName(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getClusterName(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getOceanType(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getSafEntityNumber().toString(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getLegalEntityName(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getPort(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getMarkup().toString(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getLocation(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.isHeadCountFlag() ? "Yes" : "No", sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getLedgerName(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getCompany(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getSite(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getDebitAccount(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getCreditAccount(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getNatureCode(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.isVatGstFlag() ? "Yes" : "No", sheet);
            ;
            excelUtil.createCell(row, ++columnCount, agencyDTO.getVatPercentage().toString(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getVatAccount() == null ? "NA" : agencyDTO.getVatAccount(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.isVatDaLine() ? "Yes" : "No", sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.isVatAccrual() ? "Yes" : "No", sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.isVatAllCarrier() ? "Yes" : "No", sheet);
            excelUtil.createCell(row, ++columnCount, String.join(",", agencyDTO.getDisplayCarriers()), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getFcCcy(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getArTempRecipient(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getManagerTempRecipient(), sheet);
            excelUtil.createCell(row, ++columnCount, agencyDTO.getAgencyTempRecipient(), sheet);
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    public AgencyDTO getAgency(Long agencyId) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        Agency agency = agencyRepository.findAgencyById(agencyId);
        return modelMapper.map(agency, AgencyDTO.class);
    }
}
