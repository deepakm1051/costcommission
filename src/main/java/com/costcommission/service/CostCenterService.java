package com.costcommission.service;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.BkfDTO;
import com.costcommission.dto.CostCenterDTO;
import com.costcommission.dto.PaginationResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.entity.CostCenter;
import com.costcommission.entity.CostPlusTemplateProcess;
import com.costcommission.entity.SubCategory;
import com.costcommission.exception.CostPlusException;
import com.costcommission.jpaspecification.CostCenterSpecification;
import com.costcommission.repository.*;
import com.costcommission.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CostCenterService {

    @Autowired
    private CostCenterRepository costCenterRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SubCategoryRepository subCategoryRepository;
    @Autowired
    private SubCategoryService subCategoryService;
    @Autowired
    private ExcelUtil excelUtil;
    @Autowired
    private EnvConfiguration envConfiguration;
    @Autowired
    private CostPlusTemplateRepository costPlusTemplateRepository;
    @Autowired
    private CostPlusTemplateService costPlusTemplateService;
    @Autowired
    private AgencyVatCarriersRepository agencyVatCarriersRepository;


    public List<CostCenterDTO> findAllCostCenter(String type) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        List<CostCenterDTO> costCenterDTOS = new ArrayList<>();
        List<CostCenter> costCenters = costCenterRepository.findCostCenterByType(type);
        for (CostCenter costCenter : costCenters) {
            CostCenterDTO costCenterDTO = modelMapper.map(costCenter, CostCenterDTO.class);
            if (costCenter.getSubCategory() != null)
                costCenterDTO.setCategoryName(costCenter.getSubCategory().getCategory().getName());
            costCenterDTOS.add(costCenterDTO);
        }
        return costCenterDTOS;
    }

    public CostCenterDTO create(CostCenterDTO costCenterDTO) throws Exception {
        try {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration().setAmbiguityIgnored(true);
            Long count = costCenterRepository.countCode(costCenterDTO.getCode().trim().toLowerCase(), costCenterDTO.getType().trim());
            if (count > 0) {
                throw new CostPlusException("Cost Center code already exists");
            } else {
                CostCenter costCenter = modelMapper.map(costCenterDTO, CostCenter.class);
                SubCategory subCategory = subCategoryRepository.findById(costCenterDTO.getSubCategoryId());
                costCenter.setSubCategory(subCategory);
                return modelMapper.map(costCenterRepository.save(costCenter), CostCenterDTO.class);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public int enableOrDisable(Long id, boolean status, Long userId) throws Exception {
        try {
            CostCenter costCenter = costCenterRepository.findCostCenterById(id);
            if (!status) {
                List<CostPlusTemplateProcess> templateProcesses = costPlusTemplateService.getActiveCostPlusTemplate();
                if (templateProcesses.size() > 0) {
                    for (CostPlusTemplateProcess costPlusTemplateProcess : templateProcesses) {
                        if (costPlusTemplateProcess.getAgency().getOceanType().equalsIgnoreCase(costCenter.getType())) {
                            throw new CostPlusException("Cost+ remuneration template is in generated/approval stage. Cost center inactivate is not allowed.");
                        } else {
                            Long count = agencyVatCarriersRepository.countCarriersMappedToAgencyVat(id);
                            if (count > 0)
                                throw new CostPlusException("Please remove the carrier mapped to VAT in agency master.");
                        }
                    }
                }
            }
            return costCenterRepository.enableOrDisable(status, userId, id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public CostCenterDTO update(CostCenterDTO costCenterDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        CostCenter costCenter = modelMapper.map(costCenterDTO, CostCenter.class);
        SubCategory subCategory = subCategoryRepository.findById(costCenterDTO.getSubCategoryId());
        costCenter.setSubCategory(subCategory);
        return modelMapper.map(costCenterRepository.save(costCenter), CostCenterDTO.class);
    }


    public PaginationResponseDTO getByPaginationAndSpecification(String column, int page, int size, boolean asc, String type, SpecificationDTO specificationDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        PaginationResponseDTO paginationResponseDTO = new PaginationResponseDTO();
        Sort.Direction sort = Sort.Direction.DESC;
        if (asc) {
            sort = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(page, size, sort, column);
        CostCenterSpecification spec = new CostCenterSpecification(specificationDTO, type);
        Page<CostCenter> costCenters = costCenterRepository.findAll(spec, pageable);
        List<CostCenterDTO> costCenterDTOS = new ArrayList<>();
        for (CostCenter costCenter : costCenters) {
            CostCenterDTO costCenterDTO = modelMapper.map(costCenter, CostCenterDTO.class);
            if (costCenter.getSubCategory() != null) {
                SubCategory subCategory = subCategoryRepository.findById(costCenter.getSubCategory().getId());
                costCenterDTO.setSubCategoryId(subCategory.getId());
                costCenterDTO.setSubCategoryName(subCategory.getName());
                costCenterDTO.setCategoryId(subCategory.getCategory().getId());
                costCenterDTO.setCategoryName(subCategory.getCategory().getName());
            }
            costCenterDTOS.add(costCenterDTO);
        }
        paginationResponseDTO.setTotalSize(costCenters.getTotalElements());
        paginationResponseDTO.setData(costCenterDTOS);
        return paginationResponseDTO;
    }


    public ResponseEntity<ByteArrayResource> export(String type) throws Exception {
        SXSSFWorkbook workbook = null;
        ByteArrayOutputStream arrayOutputStream = null;
        try {
            String fileName = "CostCenter" + type + ".xlsx";
            arrayOutputStream = new ByteArrayOutputStream();
            workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet();
            createCostCenterSheet(sheet, workbook, type);
            workbook.write(arrayOutputStream);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\"")
                    .body(new ByteArrayResource(arrayOutputStream.toByteArray()));
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            Objects.requireNonNull(workbook).close();
            Objects.requireNonNull(arrayOutputStream).close();
        }
    }

    public void createCostCenterSheet(SXSSFSheet sheet, SXSSFWorkbook workbook, String type) {
        int rowCount = 0;
        String[] header = {"Sl No", "Cost Center Code", "Cost Center Name", "Cost Center Category", "Cost Center SubCategory", "Key Allocation", "Carrier Code", "BKF172 Carrier Name", "Cost Center Status"};
        Row rowIndex = sheet.createRow(rowCount);
        for (int i = 0; i < header.length; i++) {
            excelUtil.createHeaderCell(rowIndex, i, header[i], sheet, workbook);
        }
        int columnCount = 0;
        List<CostCenterDTO> costCenterDTOS = findAllCostCenter(type);
        for (CostCenterDTO costCenterDTO : costCenterDTOS) {
            rowCount = rowCount + 1;
            columnCount = 0;
            Row row = sheet.createRow(rowCount);
            excelUtil.createSlNoCell(row, columnCount, rowCount, sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getCode(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getName(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getCategoryName(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getSubCategoryName(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getKeyAllocation(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getCarrierCode(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.getCarrierName(), sheet);
            excelUtil.createCell(row, ++columnCount, costCenterDTO.isActive() ? "Active" : "In-Active", sheet);
        }
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i < header.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    public List<CostCenterDTO> getActiveCarrierBasedOnType(String type) {
        List<CostCenterDTO> costCenterDTOS = new ArrayList<>();
        List<Tuple> bkfMapping = costCenterRepository.findActiveCarriers(type);
        for (Tuple tuple :
                bkfMapping) {
            CostCenterDTO costCenterDTO = new CostCenterDTO();
            costCenterDTO.setId(Long.valueOf(tuple.get("ccId").toString()));
            costCenterDTO.setCode((tuple.get("code").toString()));
            costCenterDTO.setCarrierCode((tuple.get("carrierCode").toString()));
            costCenterDTO.setCarrierName(tuple.get("carrierName") != null ? tuple.get("carrierName").toString() : "");
            costCenterDTOS.add(costCenterDTO);
        }
        return costCenterDTOS;
    }
}
