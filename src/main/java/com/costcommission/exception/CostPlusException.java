package com.costcommission.exception;

public class CostPlusException extends Exception {
    public CostPlusException(String errorMessage) {
        super(errorMessage);
    }

}
