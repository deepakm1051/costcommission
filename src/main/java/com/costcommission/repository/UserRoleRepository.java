package com.costcommission.repository;

import com.costcommission.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Serializable> {

    @Query("select u from UserRole u where u.userId=:userId and u.deleted=false")
    List<UserRole> findByUserId(@Param("userId") Long userId);

    @Modifying
    @Transactional
    @Query("update UserRole u set u.deleted=true where u.id not in :ids and u.userId=:userId")
    void disableById(@Param("ids") List<Long> ids, Long userId);

    @Modifying
    @Transactional
    @Query("update UserRole u set u.deleted=true where  u.userId=:userId")
    void disableByUserId(@Param("userId") Long userId);

}
