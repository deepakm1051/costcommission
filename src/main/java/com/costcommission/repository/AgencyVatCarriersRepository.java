package com.costcommission.repository;

import com.costcommission.entity.AgencyVatCarriers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface AgencyVatCarriersRepository extends JpaRepository<AgencyVatCarriers, Serializable>, JpaSpecificationExecutor<AgencyVatCarriers> {

        List<AgencyVatCarriers> findByAgencyId(Long agencyId);

        List<AgencyVatCarriers> findByAgencyIdAndDeleted(Long agencyId,boolean deleted);

        @Query(value = "SELECT count(id) FROM AgencyVatCarriers where costCenterId=:ccId and deleted=0")
        Long countCarriersMappedToAgencyVat(@Param("ccId") Long costCenterId);

}
