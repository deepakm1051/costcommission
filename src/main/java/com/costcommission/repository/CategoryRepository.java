package com.costcommission.repository;

import com.costcommission.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Serializable> {


    Category findByName(String name);
}
