package com.costcommission.repository;

import com.costcommission.entity.AdiJournal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface ADIJournalRepository extends JpaRepository<AdiJournal, Serializable> , JpaSpecificationExecutor<AdiJournal> {

    AdiJournal findAdiJournalById(Long id);

    AdiJournal findByCostPlusTemplateId(Long costPlusTemplateId);
}
