package com.costcommission.repository;

import com.costcommission.entity.CostPlusTemplateProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface CostPlusTemplateRepository extends JpaRepository<CostPlusTemplateProcess, Serializable>, JpaSpecificationExecutor<CostPlusTemplateProcess> {

    CostPlusTemplateProcess findCostPlusTemplateById(Long costPlusTemplateId);

    List<CostPlusTemplateProcess> findAllByStatusIn(List<Integer> status);

    List<CostPlusTemplateProcess> findAllByStatusInAndAgencyId(List<Integer> status, Long agencyId);

    @Modifying
    @Query("update CostPlusTemplateProcess c set c.status=:status,c.updatedTimeStamp=now(), c.updatedBy=:userId where c.id=:id")
    void updateStatus(@Param("status") Integer costPlusStatus,@Param("id") Long costPlusTemplateId,@Param("userId") Long userId);

    @Modifying
    @Query(value = "update cost_plus_template_process c set  c.reviewer_id=:reviewerId where c.id=:id" ,nativeQuery = true)
    void updateReviewerId(@Param("reviewerId") Long reviewerId,@Param("id") Long costPlusTemplateId);

    @Modifying
    @Query("update CostPlusTemplateProcess c set c.arInvoiceDate=now() where c.id=:id")
    void updateARInvoiceRequestDate(@Param("id") Long costPlusTemplateId);

    @Modifying
    @Query(value = "update cost_plus_template_process c set c.approved_time_stamp=now() , c.approver_id=:approverId where c.id=:id",nativeQuery = true)
    void updateApprovedIdAndDate(@Param("approverId") Long approverId,@Param("id") Long costPlusTemplateId);

    @Modifying
    @Query("update CostPlusTemplateProcess c set c.fileName=:fileName where c.id=:id")
    void updateCostPlusFileName(@Param("fileName") String fileName, @Param("id") Long costPlusTemplateId);

    CostPlusTemplateProcess findByAgencyIdAndMonthAndYear(Long agencyId, String month, Integer year);

}
