package com.costcommission.repository;

import com.costcommission.entity.Agency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface AgencyRepository extends JpaRepository<Agency, Serializable>, JpaSpecificationExecutor<Agency> {

    @Modifying
    @Query("update Agency a set a.active = :status, a.updatedTimeStamp=now(), a.updatedBy=:userId where a.id = :id")
    int enableOrDisable(@Param("status") boolean status,@Param("userId") Long userId, @Param("id") Long id);

    @Query(value = "SELECT count(id) FROM Agency where lower(code) = :code")
    Long countCode(@Param("code") String code);

    @Query(value = "SELECT count(id) FROM Agency where lower(name)=:name")
    Long countName(@Param("name") String name);

    Agency findAgencyById(Long id);

    @Query("select a from Agency a where a.deleted = false")
    List<Agency> allActive();

    List<Agency> findClusterByActive(boolean active);

    Agency findByNameIgnoreCase(String toLowerCase);

    @Query(value = "SELECT count(id) FROM Agency where cluster_id=:clusterId and active=1")
    Long countActiveCluster(@Param("clusterId") Long clusterId);

    @Query(value = "select managerTempRecipient from Agency a where a.id in :agencyIds")
    List<Tuple> findMangerEmailIds(@Param("agencyIds") List<Long> agencyIds);

    @Query(value = "select arTempRecipient from Agency a where a.id =:agencyId")
    Tuple findARTeamEmailIds(@Param("agencyId") Long agencyId);

    @Query(value = "select agencyTempRecipient from Agency a where a.id =:agencyId")
    Tuple findAgencyEmailIds(@Param("agencyId") Long agencyId);

}
