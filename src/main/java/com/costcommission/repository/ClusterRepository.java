package com.costcommission.repository;

import com.costcommission.entity.Cluster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface ClusterRepository extends JpaRepository<Cluster, Serializable>, JpaSpecificationExecutor<Cluster> {


    @Query(value = "SELECT count(id) FROM Cluster where lower(code) = :code")
    Long countByCode(@Param("code") String code );

    @Query(value = "SELECT count(id) FROM Cluster where lower(name) = :name")
    Long countByName(@Param("name") String name );

    @Modifying
    @Query("update Cluster c set c.active = :status , c.updatedTimeStamp=now() , c.updatedBy=:userId where c.id = :id")
    int enableOrDisable(@Param("status") boolean status, @Param("userId") Long userId, @Param("id") Long id);

    @Query("select c from Cluster c where c.id=:id and c.deleted=false")
    Cluster findById(@Param("id") Long id);

    @Query("select c from Cluster c where c.deleted = false")
    List<Cluster> allActive();

    List<Cluster> findClusterByActive(boolean active);

    Cluster findByNameIgnoreCase(String code);

    @Query(value = "SELECT count(id) FROM Cluster where region_id=:regionId and active=1")
    Long countActiveRegion(@Param("regionId") Long id);

    Cluster findByName(String trim);
}
