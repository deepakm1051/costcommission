package com.costcommission.repository;

import com.costcommission.entity.CostCenter;
import com.costcommission.jpaspecification.CostCenterSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;


@Repository
@Transactional
public interface CostCenterRepository extends JpaRepository<CostCenter, Serializable>, JpaSpecificationExecutor<CostCenter> {

    @Query("select c from CostCenter c where c.deleted=false and c.carrierCode!='' order by c.carrierCode")
    List<CostCenter> findAllCostCenterByCarrierCodeNotEmpty();

    @Query(value = "SELECT count(id) FROM CostCenter where lower(code) = :code")
    Long count(@Param("code") String code);

    @Query(value = "SELECT count(id) FROM CostCenter where lower(code) = :code and type=:type")
    Long countCode(@Param("code") String code,@Param("type") String type);

    @Modifying
    @Query("update CostCenter  c set c.active = :status, c.updatedTimeStamp=now(), c.updatedBy=:userId where c.id = :id")
    int enableOrDisable(@Param("status") boolean status,@Param("userId") Long userId, @Param("id") Long id);

    List<CostCenter> findCostCenterByType(String type);

    @Query("select c.code as code,c.name as name,c.carrierName as carrierName from CostCenter c where c.type=:type")
    List<Tuple> findBkfMapping(@Param("type") String type);

    @Query("select c.code as code,c.carrierCode as carrierCode,c.carrierName as carrierName,c.id as ccId from CostCenter c where c.type=:type and c.active=1 and c.carrierCode!=''")
    List<Tuple> findActiveCarriers(@Param("type") String type);

    @Query("select c.carrierCode as carrierCode from CostCenter c where c.id in :costCentreIds")
    List<Tuple> findCarrierCodeByCostCentreId(@Param("costCentreIds") List<Long> ccIds);

    @Query("select c.code from  CostCenter c")
    HashSet<String> getByCode();

    @Query("select c.id from CostCenter c where c.carrierName=:carrierName and c.type=:oceanType")
    Long getIdByCarrierName(@Param("carrierName") String carrierName, @Param("oceanType") String oceanType);

    @Query("select c.carrierName from CostCenter c where c.id=:id")
    String getCarrierNameById(@Param("id") Long id);

    CostCenter findCostCenterById(Long id);
}
