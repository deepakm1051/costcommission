package com.costcommission.repository;

import com.costcommission.entity.TBVolumeTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface TBVolumeTransactionRepository extends JpaRepository<TBVolumeTransaction, Serializable>, JpaSpecificationExecutor<TBVolumeTransaction> {

    @Query("select count(id) from TBVolumeTransaction  where year=:year and month=:month and agency_id=:agencyId and fileType='TRIALBALANCE' and deleted=0")
    Long countTbByCCByYearMonthAgency(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId);

    @Query("select count(id) from TBVolumeTransaction where year=:year and month=:month and agency_id=:agencyId and deleted=0 and fileType='VOLUME'")
    Long countVolumeDetailsByYearMonthAgency(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId);

    TBVolumeTransaction findByAgencyIdAndMonthAndYearAndDeletedAndFileType(Long agencyId,String month, Integer year,boolean deleted,String fileType);

    @Query("select tbv from TBVolumeTransaction tbv  where year=:year and month=:month and agency_id=:agencyId and fileType='VOLUME' and deleted=0")
    List<TBVolumeTransaction> findVolumeReportDetails(Long agencyId, String month, Integer year);

    @Modifying
    @Query("update TBVolumeTransaction  set deleted=1 where year=:year and month=:month and agency_id=:agencyId and fileType='TRIALBALANCE'")
    void disableTBByCCAudit(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId);

    @Modifying
    @Query("update TBVolumeTransaction  set deleted=1 where year=:year and month=:month and agency_id=:agencyId and fileType='VOLUME' and type=:type")
    void disableVolumeAudit(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId,@Param("type") String type);

    @Modifying
    @Query("update TBVolumeTransaction  set deleted=1 where year=:year and month=:month and agency_id=:agencyId and fileType='VOLUME' ")
    void disableVolumeAuditByMonthYear(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId);

}
