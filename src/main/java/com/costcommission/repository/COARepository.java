package com.costcommission.repository;

import com.costcommission.entity.COA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

@Repository
@Transactional
public interface COARepository extends JpaRepository<COA, Serializable> , JpaSpecificationExecutor<COA> {

    @Query(value = "SELECT count(id) FROM COA where natureCode = :natureCode")
    Long count(@Param("natureCode")String natureCode);

    COA  findByNatureCode(String code);

    @Modifying
    @Query("update COA  set active = :status, updatedTimeStamp=now(), updatedBy=:userId where id = :id")
    int enableOrDisable(@Param("status")boolean status,@Param("userId") Long userId, @Param("id") Long id);

    List<COA> findAllByActive(boolean active);
}