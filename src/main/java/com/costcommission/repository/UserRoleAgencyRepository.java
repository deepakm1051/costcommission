package com.costcommission.repository;

import com.costcommission.entity.UserRoleAgency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
public interface UserRoleAgencyRepository extends JpaRepository<UserRoleAgency, Serializable> {

    @Query("select u from UserRoleAgency u where u.userRoleId =:userRoleId and u.deleted=false")
    List<UserRoleAgency> findByUserRoleId(@Param("userRoleId") Long userRoleId);

    @Modifying
    @Transactional
    @Query("update UserRoleAgency u set u.deleted=true where u.id not in :roleAgencyIds and u.userRoleId in :roleIds")
    void disableById(@Param("roleAgencyIds") List<Long> activeRoleAgencyIds,@Param("roleIds")List<Long> savedRoleIds);

    @Modifying
    @Transactional
    @Query("update UserRoleAgency u set u.deleted=true where u.userRoleId in :roleIds")
    void disableByRoleId(@Param("roleIds")List<Long> savedRoleIds);

    @Query(value = "SELECT count(id) FROM UserRoleAgency where agencyId=:agencyId and deleted=0")
    Long countActiveAgency(@Param("agencyId") Long agencyId);


}
