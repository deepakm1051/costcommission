package com.costcommission.repository;

import com.costcommission.entity.CostPlusRemuneration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
public interface CostPlusRemunerationRepository extends JpaRepository<CostPlusRemuneration, Serializable>, JpaSpecificationExecutor<CostPlusRemuneration> {

    List<CostPlusRemuneration> findByCostPlusTemplateIdAndDeleted(Long id, boolean deleted);

    @Query(value = "SELECT sum(amount) as ytdAmount,cost_center_id as ccId FROM cost_plus_remuneration  " +
            "where deleted=0 and agency_id=:agencyId and year =:year and month in :months group by cost_center_id ",nativeQuery = true)
    List<Tuple> sumOfYTDCarrierRemuneration(@Param("year") Integer year, @Param("months") List<String> months, @Param("agencyId") Long agencyId);

    @Query(value = "SELECT distinct c.carrier_name,cr.amount,c.id as costCentreId FROM cost_plus_remuneration cr " +
            "join cost_center  c on c.id = cr.cost_center_id  " +
            "where month=:month and year=:year and agency_id=:agencyId and cr.deleted=0 and c.carrier_name!=''",nativeQuery = true)
    List<Tuple> getARInvoiceAmount(@Param("month") String months,@Param("year") Integer year,@Param("agencyId") Long agencyId);

    @Query("select c.agencyId from CostPlusRemuneration c where c.year=:year  and c.month=:month  and c.deleted=0")
    Set<Tuple> findAgencyByMonthYear(@Param("month") String month, @Param("year") Integer year);

    @Query("select c from CostPlusRemuneration c join  c.costPlusTemplate ct where ct.status in :status  and c.agencyId=:agencyId and c.year=:year  and c.month=:month  and c.deleted=0")
    List<CostPlusRemuneration> findAgencyIdMonthYear(@Param("agencyId") Long agencyId, @Param("month") String month, @Param("year") Integer year,@Param("status") List<Integer> status);

    @Modifying
    @Query("update CostPlusRemuneration  set deleted=1, updatedTimeStamp=now(), updatedBy=:userId where cost_plus_id=:costPlusId")
    void disableSavedRemuneration(@Param("costPlusId") Long costPlusId,@Param("userId") Long userId);

}
