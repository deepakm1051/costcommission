package com.costcommission.repository;

import com.costcommission.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Serializable>, JpaSpecificationExecutor<User> {

    User findUserByLoginIdAndPassword(String loginId, String Password);

    @Modifying
    @Query("update User u set u.active = :status, u.updatedTimeStamp=now(), u.updatedBy=:userId where u.id = :id")
    int enableOrDisable(@Param("status") boolean status, @Param("userId") Long userId, @Param("id") Long id);

    User findByEmailIdIgnoreCase(String emailId);

    User findByLoginIdIgnoreCase(String loginId);

    User findUserById(Long id);

    User findUserByLoginIdIgnoreCase(String loginId);

    @Query(value = "select u.full_name as userName,r.name as roleName,u.id as userId from user_role_agency ua " +
            "  join user_role ur on ur.id = ua.user_role_id " +
            "  join user u on u.id =ur.user_id " +
            "  join role r on r.id=ur.role_id " +
            "  where agency_id=:agencyId and ua.deleted=0 and r.name in ('Reviewer','Approver')", nativeQuery = true)
    List<Tuple> findApproversAndReviewers(@Param("agencyId") Long agencyId);

    @Query(value = "select ua.agency_id from user_role_agency ua\n" +
            " join user_role ur on ur.id = ua.user_role_id \n" +
            " join user u on u.id =ur.user_id \n" +
            " join role r on r.id=ur.role_id \n" +
            " where ur.user_id=:userId and ua.deleted=0 ", nativeQuery = true)
    List<Tuple> findAgencyMappedToUser(@Param("userId") Long userId);

    @Query(value = "select u.login_id from  user_role ur " +
            " join user u on u.id =ur.user_id where ur.role_id=1", nativeQuery = true)
    List<Tuple> findAdminMailIds();

    @Query(value ="select u.login_id as loginMail,r.name as roleName,u.full_name as fullName,u.id as userId from user_role_agency ua " +
            "join user_role ur on ur.id = ua.user_role_id " +
            "join user u on u.id =ur.user_id " +
            "join role r on r.id=ur.role_id " +
            "where agency_id=:agencyId and r.name in ('Preparer','Reviewer','Approver') and ua.deleted=0 order by name asc",nativeQuery = true)
    List<Tuple> findWorkflowUsersMailId(@Param("agencyId") Long agencyId);
}
