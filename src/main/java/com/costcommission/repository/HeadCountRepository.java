package com.costcommission.repository;

import com.costcommission.entity.HeadCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface HeadCountRepository extends JpaRepository<HeadCount, Serializable> {

    @Modifying
    @Query("update HeadCount h set h.allCCBEmployees=:allCCBEmployees, h.inWhichContainerShip=:inWhichContainerShip, h.updatedTimeStamp=now(), h.updatedBy=:userId " +
            " where h.agencyId=:id and h.month=:month and h.year=:year")
    void update(@Param("id") Long agencyId,
                @Param("month") String month,
                @Param("year") Integer year,
                @Param("allCCBEmployees") Long allCCBEmployees,
                @Param("inWhichContainerShip") Long inWhichContainerShip,
                @Param("userId") Long userId);

    @Query(value = "SELECT count(id) FROM HeadCount where agencyId =:agencyId and month=:month and year=:year")
    Long countByAgencyIdAndMonthAndYear(@Param("agencyId") Long agencyId, @Param("month") String month, @Param("year") Integer year);

    List<HeadCount> findByAgencyIdAndYear(Long agencyId, Integer year);
}
