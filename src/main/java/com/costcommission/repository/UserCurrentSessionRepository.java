package com.costcommission.repository;

import com.costcommission.entity.UserCurrentSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface UserCurrentSessionRepository extends JpaRepository<UserCurrentSession, Serializable> {

    @Query(value = "SELECT count(id) FROM UserCurrentSession where userId = :id")
    Long countByUserId(@Param("id") Long id);

    UserCurrentSession findByUserId(Long id);
}
