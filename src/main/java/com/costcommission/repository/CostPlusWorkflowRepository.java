package com.costcommission.repository;

import com.costcommission.entity.CostPlusWorkflow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface CostPlusWorkflowRepository extends JpaRepository<CostPlusWorkflow, Serializable> {
    List<CostPlusWorkflow> findByCostPlusTemplateProcessId(Long id);

    @Query(value = "SELECT c FROM CostPlusWorkflow c where cost_plus_id=:id and UPPER(action)=:action order by createdTimeStamp desc")
    List<CostPlusWorkflow> findByCostPlusByIdAction(@Param("id")Long id, @Param("action") String action);
}
