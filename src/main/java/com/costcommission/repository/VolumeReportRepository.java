package com.costcommission.repository;

import com.costcommission.entity.VolumeReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface VolumeReportRepository extends JpaRepository<VolumeReport, Serializable> {

    @Query("select v from VolumeReport v where v.year=:year and v.month=:month and v.agencyId=:agencyId and v.type=:type")
    List<VolumeReport> getVolumeDetailsByYearMonthAgencyType(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId, @Param("type") String type);

    @Query("select v from VolumeReport v where v.year=:year and v.agencyId=:agencyId and deleted=0")
    List<VolumeReport> getVolumeDetailsByYearAgency(@Param("year") Integer year, @Param("agencyId") Long agencyId);

    @Query("select v from VolumeReport v where v.year=:year and month =:month and v.agencyId=:agencyId and deleted=0")
    List<VolumeReport> getVolumeDetailsByYearMonthAgency(@Param("year") Integer year,@Param("month") String month, @Param("agencyId") Long agencyId);

    @Query(value = "SELECT sum(count_ofbolnumber), sum(sum_of_tues), type, cost_center_id FROM volume_report where year =:year and month in :months and deleted=0 and agency_id=:agencyId group by type, cost_center_id", nativeQuery = true)
    List<Tuple> getSumOfBLAndTEUByMonthYear(@Param("months") List<String> months, @Param("year") String year,@Param("agencyId") Long agencyId);

    @Modifying
    @Query("update VolumeReport  set deleted=1 where year=:year and month=:month and agencyId=:agencyId and type=:type")
    void disableSavedVolumeDetails(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId,@Param("type") String type);

    @Modifying
    @Query("update VolumeReport  set deleted=1 where year=:year and month in :months and agencyId=:agencyId and type=:type")
    void disableSavedVolumeDetailsOfMonths(@Param("year") Integer year, @Param("months") List<String> months, @Param("agencyId") Long agencyId, @Param("type") String type);

    @Query(value ="select count(u.id) as countOfUser,ur.role_id,r.name from user_role_agency ua " +
            "join user_role ur on ur.id = ua.user_role_id " +
            "join user u on u.id =ur.user_id " +
            "join role r on r.id=ur.role_id " +
            "where agency_id=:agencyId and r.name in ('Preparer','Reviewer','Approver') and ua.deleted=0 group by ur.role_id order by name asc",nativeQuery = true)
    List<Tuple> getWorkflowUsers(@Param("agencyId") Long agencyId);

    @Modifying
    @Query("update VolumeReport  set deleted=1 where year=:year and month=:month and agencyId=:agencyId ")
    void disableAllSavedVolumeDetailsByMonthYear(@Param("year") Integer year, @Param("month") String month, @Param("agencyId") Long agencyId);


    @Modifying
    @Query("update VolumeReport  set deleted=0 where year=:year and month in :months and agencyId=:agencyId")
    void activateSavedVolumeDetailsOfMonths(@Param("year") Integer year, @Param("months") List<String> months, @Param("agencyId") Long agencyId);

}
