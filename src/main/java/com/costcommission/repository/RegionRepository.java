package com.costcommission.repository;

import com.costcommission.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface RegionRepository extends JpaRepository<Region, Serializable>, JpaSpecificationExecutor<Region> {

    @Query(value = "SELECT count(id) FROM Region where lower(code) = :code")
    Long countByCode(@Param("code") String code);

    @Query(value = "SELECT count(id) FROM Region where lower(name) = :name")
    Long countByName(@Param("name") String name);

    @Query("select r from Region r where r.id=:id and r.deleted=false")
    Region findRegionById(@Param("id") Long id);

    @Modifying
    @Query("update Region r set r.active = :deleted , r.updatedTimeStamp=now() , r.updatedBy=:userId where r.id = :id")
    int enableOrDisable(@Param("deleted") boolean deleted,@Param("userId") Long userId, @Param("id") Long id);

    @Query("select r from Region r where r.deleted = false")
    List<Region> allActive();

    Region findByNameEqualsIgnoreCase(String name);

    List<Region> findRegionByActive(boolean active);

    @Query(value = "select a.name as agency,r.name as region, a.id as agencyId " +
            "from region  r " +
            "join cluster c on r.id = c.region_id " +
            "join agency a on a.cluster_id =c.id where r.active=1 order by r.name ",nativeQuery = true)
    List<Tuple> getAgencyMappedToRegion();

    Region findByName(String name);
}
