package com.costcommission.repository;

import com.costcommission.entity.PeriodControl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
@Transactional
public interface PeriodControlRepository extends JpaRepository<PeriodControl, Serializable> , JpaSpecificationExecutor<PeriodControl> {

    @Query(value = "SELECT count(id) FROM PeriodControl where agency_id = :agencyId and month=:month and year=:year")
    Long countByAgencyMonthYear(Long agencyId, String month, Integer year);

    PeriodControl findByAgencyIdAndMonthAndYear(Long agencyId,String month,Integer year);

    @Modifying
    @Query("update PeriodControl  set status = :status, updatedTimeStamp=now(), updatedBy=:userId where id = :id")
    void updateStatus(@Param("status")boolean status, @Param("id") Long id,@Param("userId") Long userId);

    @Query(value = "SELECT count(id) FROM PeriodControl where agency_id = :agencyId and month in :months and year=:year and status=1")
    Long countPeriodControlOpenStatus(@Param("agencyId") Long agencyId,@Param("months") List<String> months, @Param("year") Integer year);

}
