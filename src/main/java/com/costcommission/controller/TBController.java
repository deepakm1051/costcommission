package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.TBService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/trialBalance")
public class TBController {

    @Autowired
    private TBService tbService;

    @PostMapping("/upload")
    public ResponseEntity<Object> upload(@RequestPart MultipartFile file, @RequestPart String requestData,@RequestPart String preparerConfirm) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            UserCurrentSessionDTO userCurrentSessionDTO = mapper.readValue(requestData, UserCurrentSessionDTO.class);
            Object inValidTB = tbService.upload(file, userCurrentSessionDTO,preparerConfirm);
            if (inValidTB.toString().startsWith("success")) {
                String[] totalRow =inValidTB.toString().split("-");
                responseDTO = new ResponseDTO("Trial balance by cost center uploaded successfully. Total rows => "+totalRow[1], true, null);
                httpStatus = HttpStatus.OK;
            } else {
                responseDTO = new ResponseDTO("Invalid Trial Balance", false, inValidTB);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/filterByPagination")
    public ResponseEntity<Object> getByPaginationAndSpecification
            (@RequestParam(required = false, defaultValue = "updatedTimeStamp") String column,
             @RequestParam(required = false, defaultValue = "0") Integer page,
             @RequestParam(required = false, defaultValue = "10") Integer size,
             @RequestParam(required = false, defaultValue = "false") boolean asc,
             @RequestBody SpecificationDTO specificationDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Audit Trial successfully", true, tbService.getByPaginationAndSpecification(column, page, size, asc, specificationDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/details")
    public ResponseEntity<Object> getTBDetails(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Trial Balance details fetched successfully", true, tbService.getTBDetails(userCurrentSessionDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @GetMapping("/export/{fileName}")
    public ResponseEntity<?> export(@PathVariable("fileName") String fileName){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = tbService.export(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


}
