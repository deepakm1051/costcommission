package com.costcommission.controller;

import com.costcommission.dto.HeadCountDTO;
import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.HeadCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/headcount")
public class HeadCountController {

    @Autowired
    private HeadCountService headCountService;

    @PostMapping
    public ResponseEntity<Object> createOrUpdate(@RequestBody List<HeadCountDTO> headCountDTO,@RequestHeader("userId") String userId){
        HttpStatus httpStatus=HttpStatus.OK;
        ResponseDTO responseDTO;
        try{
            headCountService.create(headCountDTO,userId);
            responseDTO=new ResponseDTO("Head Count Saved Successfully",true,null);
        }catch (Exception e){
            responseDTO=new ResponseDTO("Failed to create Head Count",false,null);
            httpStatus=HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(responseDTO,httpStatus);
    }
    @PostMapping("/details")
    public ResponseEntity<Object> get(@RequestBody UserCurrentSessionDTO currentSessionDTO){
        HttpStatus httpStatus;
        ResponseDTO responseDTO;
        try{
            responseDTO=new ResponseDTO("Head Count Fetch Successfully",true,headCountService.get(currentSessionDTO));
            httpStatus=HttpStatus.OK;
        }catch (Exception e){
            responseDTO=new ResponseDTO("Failed to fetch head Count",false,null);
            httpStatus=HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(responseDTO,httpStatus);
    }

}
