package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.service.PeriodControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/period")
public class PeriodController {

    @Autowired
    private PeriodControlService periodControlService;

    @PostMapping("/filterByPagination")
    public ResponseEntity<Object> getByPaginationAndSpecification
            (@RequestParam(required = false, defaultValue = "updatedTimeStamp") String column,
             @RequestParam(required = false, defaultValue = "0") Integer page,
             @RequestParam(required = false, defaultValue = "10") Integer size,
             @RequestParam(required = false, defaultValue = "false") boolean asc,
             @RequestBody SpecificationDTO specificationDTO)
    {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Period Control fetched successfully", true, periodControlService.getByPaginationAndSpecification(column, page, size, asc, specificationDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @GetMapping("/export")
    public ResponseEntity<?> export(){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = periodControlService.export();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
