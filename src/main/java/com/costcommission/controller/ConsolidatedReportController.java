package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.ConsolidatedReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/consolidatedReport")
public class ConsolidatedReportController {

    @Autowired
    private ConsolidatedReportService consolidatedReportService;

    @GetMapping("/export/{month}/{year}/{userId}")
    public ResponseEntity<ByteArrayResource> export(@PathVariable("month") String month,@PathVariable("year") Integer year,@PathVariable("userId") String userId){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = consolidatedReportService.export(month,year,userId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    @PostMapping("/sendMail")
    ResponseEntity<Object> sentConsolidatedReportMail(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            consolidatedReportService.sendConsolidatedReportMail(userCurrentSessionDTO);
            responseDTO = new ResponseDTO("Consolidated report email notification sent successfully", true,  "sent");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);

    }



}
