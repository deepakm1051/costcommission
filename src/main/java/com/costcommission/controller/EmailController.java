package com.costcommission.controller;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//to be deleted
@RestController
@RequestMapping("email")
public class EmailController {

    @Autowired
    private EnvConfiguration envConfiguration;

    @Autowired
    private MailService mailService;

    @PostMapping("/sent")
    ResponseEntity<Object> sent() {
        String mailBody = "";
        try {
            mailService.sendMail("deepak@1cloudhub.com", "test", mailBody);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity<>("mail sent", HttpStatus.OK);
    }
}
