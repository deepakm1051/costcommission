package com.costcommission.controller;

import com.costcommission.costenum.CostPlusStatus;
import com.costcommission.dto.CostPlusWorkflowDTO;
import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.ActualProcessService;
import com.costcommission.service.CostPlusReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/actualProcess")
public class ActualProcessController {

    @Autowired
    private ActualProcessService actualProcessService;

    @PostMapping
    public ResponseEntity<Object> saveActualProcessConfirmation(@RequestBody CostPlusWorkflowDTO costPlusWorkflowDTO) {
        ResponseDTO responseDTO;
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            actualProcessService.concurrentUserAccess(costPlusWorkflowDTO);
            String action = costPlusWorkflowDTO.getAction();
            actualProcessService.actualProcessConfirmation(costPlusWorkflowDTO);
            String msg = action + " confirmation saved successfully";
            if (action.equalsIgnoreCase(CostPlusStatus.RELEASED.name()))
                msg = "Cost+ remuneration template released successfully";
            else if (action.equalsIgnoreCase(CostPlusStatus.REVIEWED.name()))
                msg = "Cost+ remuneration template reviewed successfully";
            else if (action.equalsIgnoreCase(CostPlusStatus.CLOSED.name()))
                msg = "Cost+ remuneration template approved successfully";
            else if (action.equalsIgnoreCase(CostPlusStatus.REJECTED.name()))
                msg = "Cost+ remuneration template rejected successfully";
            else if (action.equalsIgnoreCase("COSTPLUSCALCULATIONSTART"))
                msg = "Cost+ remuneration template generated successfully";
            else if (action.equalsIgnoreCase("SENDFORAPPROVAL"))
                msg = "Cost+ remuneration template sent for approval successfully";
            else if (action.equalsIgnoreCase("ARINVOICEREQUEST"))
                msg = "AR Invoice Request notification sent successfully";

            responseDTO = new ResponseDTO(msg, true, null);
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, e.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("details")
    public ResponseEntity<Object> getTemplateDetails(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        ResponseDTO responseDTO;
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            responseDTO = new ResponseDTO("Template details fetched successfully", true, actualProcessService.getCostPlusTemplate(userCurrentSessionDTO));
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, e.getMessage());
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

}
