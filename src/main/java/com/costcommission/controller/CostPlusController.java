package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.service.CostPlusTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/costplus")
public class CostPlusController {


    @Autowired
    private CostPlusTemplateService costPlusTemplateService;

    @PostMapping("/excel")
    public ResponseEntity<Object> generate() {
        ResponseDTO responseDTO;
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            costPlusTemplateService.generate();
            responseDTO = new ResponseDTO("successfully created", true, null);
        } catch (Exception e) {
            responseDTO = new ResponseDTO("failure to create", false, null);
            httpStatus = HttpStatus.EXPECTATION_FAILED;
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }


}
