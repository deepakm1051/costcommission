package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.service.BkfMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/bkfMapping")
public class BkfMappingController {

    @Autowired
    private BkfMappingService bkfMappingService;

    @GetMapping("/{type}")
    public ResponseEntity<Object> bkfMapping(@PathVariable("type") String type){
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("BKFMapping fetched successfully", true, bkfMappingService.getBkfMapping(type));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }
    @GetMapping("/export")
    public ResponseEntity<?> export(){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = bkfMappingService.export();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
