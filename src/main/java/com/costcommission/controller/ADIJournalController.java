package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.service.ADIJournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/adiJournal")
public class ADIJournalController {

    @Autowired
    private ADIJournalService adiJournalService;

    @PostMapping("/filterByPagination")
    public ResponseEntity<Object> getByPaginationAndSpecification
            (@RequestParam(required = false, defaultValue = "updatedTimeStamp") String column,
             @RequestParam(required = false, defaultValue = "0") Integer page,
             @RequestParam(required = false, defaultValue = "10") Integer size,
             @RequestParam(required = false, defaultValue = "false") boolean asc,
             @RequestParam String journalType,
             @RequestParam("agencyId") Long agencyId,
             @RequestBody SpecificationDTO specificationDTO)
    {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("ADI Journal R11 fetched successfully", true, adiJournalService.getByPaginationAndSpecification(column, page, size, asc, journalType,agencyId,specificationDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @GetMapping("/export/{journalId}")
    public ResponseEntity<?> export(@PathVariable("journalId") Long journalId){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = adiJournalService.export(journalId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
