package com.costcommission.controller;

import com.costcommission.config.EnvConfiguration;
import com.costcommission.dto.ResponseDTO;
import com.costcommission.service.MailService;
import com.costcommission.service.TestService;
import com.costcommission.util.EmailTemplateUtil;
import com.costcommission.util.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/test")
public class TestController {

    @Autowired
    private EnvConfiguration envConfiguration;

    @Autowired
    private MailService mailService;

    @Autowired
    private TestService testService;

    @Autowired
    private EmailTemplateUtil emailTemplateUtil;

    @PostMapping("/sent")
    ResponseEntity<Object> sendMail() {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            String process= emailTemplateUtil.getEmailTemplateData("hello-template",null);
            mailService.sendMail(new String[]{"suma@1cloudhub.com"},null, "test", process, null, "sample");
            responseDTO = new ResponseDTO("mail sent", true,  "sent");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);

    }

    @PostMapping("/s3")
    ResponseEntity<Object> s3Access() {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            testService.checkS3Access();
            responseDTO = new ResponseDTO("s3 bucket connected successfully", true,  null);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);

    }

    @PostMapping("/upload")
    public ResponseEntity<Object> uploadInitialSetUpRecords(@RequestPart MultipartFile file) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            testService.upload(file);
            responseDTO = new ResponseDTO("Uploaded successfully", true, null);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }
}
