package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.VolumeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/volume")
public class VolumeController {

    @Autowired
    private VolumeService volumeService;

    @PostMapping("/upload")
    public ResponseEntity<Object> upload(@RequestPart MultipartFile file, @RequestPart String requestData,@RequestPart String volumeType,@RequestPart String mtdOrYtd) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            UserCurrentSessionDTO userCurrentSessionDTO = mapper.readValue(requestData, UserCurrentSessionDTO.class);
            volumeService.upload(file,volumeType,mtdOrYtd,userCurrentSessionDTO);
            responseDTO = new ResponseDTO("Volume Calculated successfully", true, null);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/details")
    public ResponseEntity<Object> getVolumeDetails(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Volume Report details fetched successfully", true, volumeService.getVolumeDetails(userCurrentSessionDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/detailsByMonthYear")
    public ResponseEntity<Object> getVolumeDetailsByMonthYear(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Volume Report details fetched successfully", true, volumeService.getVolumeDetailsByMonthYearAgency(userCurrentSessionDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/deactivate")
    public ResponseEntity<Object> deactivateVolume(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            volumeService.deActivateVolumeByMonthYearAgency(userCurrentSessionDTO);
            responseDTO = new ResponseDTO("Volume Report Removed successfully for selected month, year and agency", true, null);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @PostMapping("/deactivateStatus")
    public ResponseEntity<Object> deactivateStatus(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO) {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("", true, volumeService.deactivateStatus(userCurrentSessionDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @GetMapping("/export/{month}/{year}/{agencyId}")
    public ResponseEntity<?> export(@PathVariable("month") String month, @PathVariable("year")Integer year,@PathVariable("agencyId") Long agencyId){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = volumeService.export(month,year,agencyId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
