package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.SpecificationDTO;
import com.costcommission.service.CostPlusReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/costplusreport")
public class CostPlusReportController {

    @Autowired
    private CostPlusReportService costPlusReportService;

    @PostMapping("/filterByPagination")
    public ResponseEntity<Object> getByPaginationAndSpecification
            (@RequestParam(required = false, defaultValue = "updatedTimeStamp") String column,
             @RequestParam(required = false, defaultValue = "0") Integer page,
             @RequestParam(required = false, defaultValue = "10") Integer size,
             @RequestParam(required = false, defaultValue = "false") boolean asc,
             @RequestParam("agencyId") Long agencyId,
             @RequestParam("year") Integer year,
             @RequestBody SpecificationDTO specificationDTO)
    {
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("CostPlus Report fetched successfully", true, costPlusReportService.getByPaginationAndSpecification(column, page, size, asc, specificationDTO,agencyId,year));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

    @GetMapping("/export/{costPlusId}")
    public ResponseEntity<?> export(@PathVariable("costPlusId") Long costPlusId){
        ResponseEntity<ByteArrayResource> response = null;
        try {
            response = costPlusReportService.export(costPlusId);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new ResponseDTO(e.getMessage(), false, null), HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


}
