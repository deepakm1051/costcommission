package com.costcommission.controller;

import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.service.GraphicalDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/dashboardReport")
public class GraphicalDashboardController {

    @Autowired
    private GraphicalDashboardService graphicalDashboardService;

    @PostMapping
    public ResponseEntity<Object> getDashboardDetails(@RequestBody UserCurrentSessionDTO userCurrentSessionDTO){
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        ResponseDTO responseDTO = null;
        try {
            responseDTO = new ResponseDTO("Dashboard details fetched successfully", true, graphicalDashboardService.getDetails(userCurrentSessionDTO));
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }


}
