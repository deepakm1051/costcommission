package com.costcommission.controller;

import com.costcommission.dto.LoginDTO;
import com.costcommission.dto.ResponseDTO;
import com.costcommission.dto.UserCurrentSessionDTO;
import com.costcommission.dto.UserDTO;
import com.costcommission.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<Object> validateUser(@RequestBody LoginDTO loginDTO) {
        ResponseDTO responseDTO;
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        try {
            UserDTO savedUser = loginService.validateUser(loginDTO.getLoginId(), loginDTO.getPassword());
            if (savedUser != null) {
                if(savedUser.isActive())
                responseDTO = new ResponseDTO("Valid User", true, savedUser);
                else
                    responseDTO = new ResponseDTO("Inactive User", false, savedUser);
            }
            else
                responseDTO = new ResponseDTO("Invalid User", false, null);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }


    @PostMapping("/userSelection")
    public ResponseEntity<Object> createOrUpdate(@RequestBody UserCurrentSessionDTO currentSessionDTO) {
        ResponseDTO responseDTO;
        HttpStatus httpStatus = HttpStatus.EXPECTATION_FAILED;
        try {
            UserCurrentSessionDTO userCurrentSessionDTO = loginService.createOrUpdate(currentSessionDTO);
            responseDTO = new ResponseDTO("Selection saved successfully", true, userCurrentSessionDTO);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            responseDTO = new ResponseDTO(e.getMessage(), false, null);
        }
        return new ResponseEntity<>(responseDTO, httpStatus);
    }

}
